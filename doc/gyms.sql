/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.21 : Database - gyms_gdou
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gyms_gdou` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `gyms_gdou`;

/*Table structure for table `authority` */

DROP TABLE IF EXISTS `authority`;

CREATE TABLE `authority` (
  `id` varchar(50) NOT NULL,
  `name` varchar(202) DEFAULT NULL,
  `code` varchar(202) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `method` varchar(200) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `authority` */

insert  into `authority`(`id`,`name`,`code`,`url`,`method`,`controller`,`des`) values ('047b2f11-3a43-11e8-af4b-507b9dc8fad2','校验用户名唯一性，账号','user_isExist','/api/v1/user/isExist','GET','UserController',''),('12495b97-3a43-11e8-af4b-507b9dc8fad2','查单个用户所拥有的角色id','user_getRoleByUser','/api/v1/user/getRoleByUser','GET','UserController',''),('18230089-3a42-11e8-af4b-507b9dc8fad2','新增角色','role_saveRole','/api/v1/role','POST','RoleController',''),('1c68960f-3a41-11e8-af4b-507b9dc8fad2','按controller分组，得到authority列表','authority_getAuthority','/api/v1/authority','GET','AuthorityController',''),('278e9498-3a42-11e8-af4b-507b9dc8fad2','删除角色','role_deleteRole','/api/v1/role/{roleId}','DELETE','RoleController',''),('331a840d-3a42-11e8-af4b-507b9dc8fad2','修改角色','role_updateRole','/api/v1/role/','PUT','RoleController',''),('3aa50cdc-3a41-11e8-af4b-507b9dc8fad2','根据请求方式，按关键字模糊查询','authority_getLikeAuthority','/api/v1/getLikeAuthority','GET','AuthorityController',''),('46d1ae66-3a42-11e8-af4b-507b9dc8fad2','查询角色列表','role_getRoleList','/api/v1/role','GET','RoleController',''),('6bd80bdb-3a42-11e8-af4b-507b9dc8fad2','给角色授权','role_authorize','/api/v1/role/authorize','POST','RoleController',''),('78e52b25-3a41-11e8-af4b-507b9dc8fad2','新增资源','resource_addResource','/api/v1/resource','POST','ResourceController',''),('7eee4505-3a42-11e8-af4b-507b9dc8fad2','校验code唯一性','role_isExist','/api/v1/role/isExist','GET','RoleController',''),('880e79f3-3a41-11e8-af4b-507b9dc8fad2','修改资源','resource_updateResource','/api/v1/resource','PUT','ResourceController',''),('8bc0323d-3a42-11e8-af4b-507b9dc8fad2','得到某角色拥有的资源','role_getRoleResource','/api/v1/role/getRoleResource','GET','RoleController',''),('9e0c17e4-3a41-11e8-af4b-507b9dc8fad2','删除资源','resource_deleteResource','/api/v1/resource/{resourceId}','DELETE','ResourceController',''),('a516aa5f-3a42-11e8-af4b-507b9dc8fad2','新增用户','user_saveUser','/api/v1/user','POST','UserController',''),('b07a1165-3a41-11e8-af4b-507b9dc8fad2','查询资源列表','resource_getAllResource','/api/v1/resource','GET','ResourceController',''),('b75a7c8b-3a42-11e8-af4b-507b9dc8fad2','修改用户','user_updateUser','/api/v1/user','PUT','UserController',''),('c5951c74-3a42-11e8-af4b-507b9dc8fad2','删除用户','user_deleteUser','/api/v1/user/{id}','DELETE','UserController',''),('c877f065-3a41-11e8-af4b-507b9dc8fad2','查单个资源 拥有的操作码','resource_getAuthorityByResource','/api/v1/resource/getAuthorityByResource','GET','ResourceController',''),('d742bcfa-3a42-11e8-af4b-507b9dc8fad2','将角色授予用户','user_delegate','/api/v1/user/delegate','POST','UserController',''),('de481f75-3a41-11e8-af4b-507b9dc8fad2','取消授权,取消一个资源的某个 或一些 权限','resource_deleteResourceAuthority','/api/v1/resource/deleteResourceAuthority','POST','ResourceController',''),('f0853008-3a41-11e8-af4b-507b9dc8fad2','给资源授权,单个操作码给单个资源','resource_authorize','/api/v1/resource/authorize','POST','ResourceController',''),('f927cf1e-3a42-11e8-af4b-507b9dc8fad2','查所有用户','user_findAllUser','/api/v1/user','GET','UserController','');

/*Table structure for table `book_info` */

DROP TABLE IF EXISTS `book_info`;

CREATE TABLE `book_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `book_id` varchar(10) NOT NULL COMMENT '预约编号，业务主键',
  `user_id` varchar(30) NOT NULL COMMENT '预约用户编号，业务主键（8位日期+2位自增数',
  `book_time` varchar(10) NOT NULL COMMENT '预约时间段编号，业务主键（8位日期+2位自增数）',
  `audit_state` varchar(10) NOT NULL DEFAULT '等待' COMMENT '预约审核状态（成功、失败、等待）预约管理员审核',
  `book_state` varchar(10) NOT NULL DEFAULT '审核中' COMMENT '预约状态（预约成功、预约失败、审核中、预约取消）',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `book_info` */

/*Table structure for table `game_info` */

DROP TABLE IF EXISTS `game_info`;

CREATE TABLE `game_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `game_id` varchar(10) NOT NULL COMMENT '赛事编号，业务主键',
  `book_id` varchar(20) NOT NULL COMMENT '预约编号，业务主键',
  `game_name` varchar(30) NOT NULL COMMENT '赛事名称',
  `game_intro` varchar(500) NOT NULL COMMENT '赛事简介',
  `game_sponsor` varchar(30) NOT NULL COMMENT '赛事主办单位',
  `game_time` varchar(10) NOT NULL COMMENT '赛事举办日期',
  `periods` varchar(10) NOT NULL COMMENT '赛事举办场次',
  `game_type` varchar(10) NOT NULL COMMENT '赛事类型编号',
  `game_state` varchar(10) DEFAULT '待举办' COMMENT '赛事状态（已举办、待举办、举办中）',
  `audit_state` varchar(10) DEFAULT '等待' COMMENT '赛事审核（成功、失败、等待）赛事管理员审核',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `game_info` */

insert  into `game_info`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`game_id`,`book_id`,`game_name`,`game_intro`,`game_sponsor`,`game_time`,`periods`,`game_type`,`game_state`,`audit_state`,`remark`) values (7,'2018-06-25 10:04:18','2018-06-25 10:04:18',0,'S01001','B20180624001','海蓝之家206篮球比赛','篮球使我快乐','海蓝之家','20180624','','篮球比赛',NULL,NULL,'我很帅~'),(8,'2018-06-25 10:05:34','2018-06-25 10:05:34',0,'S01002','B20180624002','海蓝之家206篮球比赛','篮球使我快乐','海蓝之家','20180624','','篮球比赛',NULL,NULL,'我很帅~'),(9,'2018-06-25 10:06:26','2018-06-25 10:06:26',0,'S02001','B20180624003','海蓝之家206篮球比赛','篮球使我快乐','海蓝之家','20180624','','足球比赛',NULL,NULL,'我很帅~'),(10,'2018-06-29 09:12:01','2018-06-29 09:12:01',0,'S01003','B20180629001','GG','123','123','321@qq.com','','篮球比赛','待举办','等待','gg'),(11,'2018-06-29 09:13:23','2018-06-29 09:13:23',0,'S01004','B20180629001','GG','123','123','321@qq.com','','篮球比赛','待举办','等待','gg');

/*Table structure for table `game_type` */

DROP TABLE IF EXISTS `game_type`;

CREATE TABLE `game_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键\r\n（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `type_id` varchar(10) NOT NULL COMMENT '赛事类型编号，业务主键\r\n（）',
  `type_name` varchar(30) NOT NULL COMMENT '赛事类型名称',
  `charge_standard` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '收费标准（按场收费）',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `game_type` */

/*Table structure for table `ground_info` */

DROP TABLE IF EXISTS `ground_info`;

CREATE TABLE `ground_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `ground_id` varchar(10) NOT NULL COMMENT '场地编号，业务主键（场地类型编号+自增）',
  `ground_name` varchar(30) NOT NULL COMMENT '场地名称',
  `ground_location` varchar(30) NOT NULL COMMENT '场地位置',
  `ground_type` varchar(10) NOT NULL COMMENT '场地类型编号',
  `ground_size` int(10) unsigned NOT NULL COMMENT '场地大小',
  `ground_capacity` int(10) unsigned NOT NULL COMMENT '场地可容人数',
  `rent` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '租金（按场收费）',
  `ground_state` varchar(10) NOT NULL DEFAULT '空闲' COMMENT '场地状态(空闲、使用、维护)',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `ground_info` */

insert  into `ground_info`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`ground_id`,`ground_name`,`ground_location`,`ground_type`,`ground_size`,`ground_capacity`,`rent`,`ground_state`,`remark`) values (1,'2018-07-02 14:07:27','2018-07-02 14:07:30',0,'G01001','篮球','海心沙','01',1,1,'0.00','比赛',NULL);

/*Table structure for table `ground_rent` */

DROP TABLE IF EXISTS `ground_rent`;

CREATE TABLE `ground_rent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `ground_id` varchar(10) NOT NULL COMMENT '场地编号，业务主键（场地类型编号+自增）',
  `book_id` varchar(20) NOT NULL COMMENT '预约编号，业务主键',
  `rent_time` varchar(10) NOT NULL COMMENT '租借时间段编号，业务主键（8位日期+2位自增数）',
  `periods` varchar(10) NOT NULL COMMENT '上午、下午、晚上',
  `audit_state` varchar(10) NOT NULL DEFAULT '等待' COMMENT '场地审核状态（成功、失败、等待）场地管理员审核',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ground_rent` */

/*Table structure for table `ground_type` */

DROP TABLE IF EXISTS `ground_type`;

CREATE TABLE `ground_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `type_id` varchar(10) NOT NULL COMMENT '场地类型编号，业务主键()',
  `type_name` varchar(30) NOT NULL COMMENT '场地类型名称',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ground_type` */

/*Table structure for table `material_info` */

DROP TABLE IF EXISTS `material_info`;

CREATE TABLE `material_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `material_id` varchar(10) NOT NULL COMMENT '器材编号，业务主键（器材类型编号+自增）',
  `material_name` varchar(30) NOT NULL COMMENT '器材名称',
  `material_type` varchar(10) NOT NULL COMMENT '器材类型编号',
  `material_number` int(10) unsigned NOT NULL COMMENT '器材总数',
  `material_residue` int(10) unsigned NOT NULL COMMENT '器材库存',
  `buy_time` datetime NOT NULL COMMENT '购置日期',
  `rent` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '租金(按件收费)',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `material_info` */

/*Table structure for table `material_rent` */

DROP TABLE IF EXISTS `material_rent`;

CREATE TABLE `material_rent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `material_id` varchar(10) NOT NULL COMMENT '器材编号，业务主键（器材类型编号+自增）',
  `book_id` varchar(20) NOT NULL COMMENT '预约编号，业务主键',
  `rent_number` int(10) unsigned NOT NULL COMMENT '器材租借数量',
  `rent_time` varchar(10) NOT NULL COMMENT '租借时间段编号，业务主键（8位日期+2位自增数）',
  `audit_state` varchar(10) NOT NULL DEFAULT '等待' COMMENT '器材租借审核状态（成功、失败、等待）器材管理员审核',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `material_rent` */

/*Table structure for table `material_type` */

DROP TABLE IF EXISTS `material_type`;

CREATE TABLE `material_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `type_id` varchar(10) NOT NULL COMMENT '器材类型编号，业务主键',
  `type_name` varchar(30) NOT NULL COMMENT '器材类型名称',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `material_type` */

/*Table structure for table `order_info` */

DROP TABLE IF EXISTS `order_info`;

CREATE TABLE `order_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `order_id` varchar(10) NOT NULL COMMENT '订单编号，业务主键',
  `book_id` varchar(10) NOT NULL COMMENT '预约编号，业务主键',
  `order_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单总金额(赛事+裁判+场地+器材)',
  `order_state` varchar(10) NOT NULL DEFAULT '待支付' COMMENT '订单状态(已支付、待支付)预约管理员线下审核',
  `payment_time` datetime DEFAULT NULL COMMENT '支付时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order_info` */

/*Table structure for table `referee_info` */

DROP TABLE IF EXISTS `referee_info`;

CREATE TABLE `referee_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modifird` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除\r\n（1已删除，0未删除）',
  `referee_id` varchar(10) NOT NULL COMMENT '裁判编号，业务主键',
  `referee_name` varchar(30) NOT NULL COMMENT '裁判名字',
  `referee_sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '裁判性别（ 1男，0女）',
  `referee_age` tinyint(3) unsigned NOT NULL COMMENT '裁判年龄',
  `referee_item` varchar(10) NOT NULL COMMENT '裁判项目编号',
  `charge_standard` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '收费标准（按场收费）',
  `referee_state` varchar(10) NOT NULL DEFAULT '空闲' COMMENT '裁判状态(空闲、比赛、请假)',
  `phone` varchar(20) NOT NULL COMMENT '手机号码',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `referee_info` */

/*Table structure for table `referee_item` */

DROP TABLE IF EXISTS `referee_item`;

CREATE TABLE `referee_item` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `item_id` varchar(10) NOT NULL COMMENT '裁判项目编号，业务主键（）',
  `item_name` varchar(30) NOT NULL COMMENT '裁判项目名称',
  `charge_standard` decimal(10,0) NOT NULL COMMENT '收费标准（按场收费）',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `referee_item` */

/*Table structure for table `referee_rent` */

DROP TABLE IF EXISTS `referee_rent`;

CREATE TABLE `referee_rent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `referee_id` varchar(10) NOT NULL COMMENT '裁判编号，业务主键（裁判类型编号+自增）',
  `referee_item` varchar(10) NOT NULL COMMENT '裁判体育项目编号',
  `book_id` varchar(20) NOT NULL COMMENT '预约编号，业务主键',
  `rent_time` varchar(10) NOT NULL COMMENT '租借时间段编号，业务主键（8位日期+2位自增数）',
  `periods` varchar(10) NOT NULL COMMENT '上午、下午、晚上',
  `audit_state` varchar(10) NOT NULL DEFAULT '等待' COMMENT '裁判审核状态\r\n（成功、失败、等待）\r\n裁判管理员审核',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `referee_rent` */

/*Table structure for table `res_auth` */

DROP TABLE IF EXISTS `res_auth`;

CREATE TABLE `res_auth` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime DEFAULT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `res_id` varchar(50) NOT NULL COMMENT '资源编号',
  `auth_id` varchar(50) NOT NULL COMMENT '权限编号',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `res_auth` */

insert  into `res_auth`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`res_id`,`auth_id`,`remark`) values (1,NULL,NULL,0,'0451e108-da3b-4b87-ae62-f2ee1beb7c9b','12495b97-3a43-11e8-af4b-507b9dc8fad2','0f7b28c9-38a3-4121-a0aa-eb4e7a5151f9'),(2,NULL,NULL,0,'0451e108-da3b-4b87-ae62-f2ee1beb7c9b','f927cf1e-3a42-11e8-af4b-507b9dc8fad2','133057d7-de6b-49ad-9a6e-16158c5a01c0'),(3,NULL,NULL,0,'2fc56a7f-b29c-42f7-8ade-d26ea3eaa0dc','18230089-3a42-11e8-af4b-507b9dc8fad2','13f71763-01e6-4e4e-927a-c0154f3d224a'),(4,NULL,NULL,0,'0d2adeca-08c7-4c3c-a332-6ec7262fb562','b07a1165-3a41-11e8-af4b-507b9dc8fad2','170b5874-edf3-4d50-92b1-bed9df58e968'),(5,NULL,NULL,0,'1efea1bc-aecd-4787-823d-55d63fe54abf','331a840d-3a42-11e8-af4b-507b9dc8fad2','175005d6-4d57-4eb7-97e3-68c18e971524'),(6,NULL,NULL,0,'47e1a143-0fee-4a1e-8bdc-8d659497aec4','278e9498-3a42-11e8-af4b-507b9dc8fad2','1dde580a-0712-4dd4-80b5-866d3977701c'),(7,NULL,NULL,0,'ee83c7b6-d22d-4013-8172-b397f0f31b51','f0853008-3a41-11e8-af4b-507b9dc8fad2','1f7569d6-1054-4e9f-a1f3-f84e6413fd25'),(8,NULL,NULL,0,'376ccf3e-a713-4738-95fd-41e071b3e891','b75a7c8b-3a42-11e8-af4b-507b9dc8fad2','2b6ed9cf-2fc1-4fc9-88f5-ef48d1ad7fb0'),(9,NULL,NULL,0,'0d2adeca-08c7-4c3c-a332-6ec7262fb562','8bc0323d-3a42-11e8-af4b-507b9dc8fad2','458e0f1d-5ba5-4be1-81af-e13b9ae7d2c7'),(10,NULL,NULL,0,'5a9a6b50-789c-4b6c-83ae-7701b6259c51','1c68960f-3a41-11e8-af4b-507b9dc8fad2','56f13076-7ad1-4c8e-9fee-55bbcd8e26cc'),(11,NULL,NULL,0,'c07f19e5-df3e-4893-b9dd-42ee09705b43','c5951c74-3a42-11e8-af4b-507b9dc8fad2','5e4e8229-deed-47ec-8d7e-28ff58644f36'),(12,NULL,NULL,0,'03f1c9d6-b952-4c77-92d6-4ada566567c5','46d1ae66-3a42-11e8-af4b-507b9dc8fad2','6aad5d47-f126-4727-9e29-0bee16a0ee3d'),(13,NULL,NULL,0,'0d2adeca-08c7-4c3c-a332-6ec7262fb562','6bd80bdb-3a42-11e8-af4b-507b9dc8fad2','6bd89b1f-5112-4cbd-a9bf-b703493f6d7f'),(14,NULL,NULL,0,'376ccf3e-a713-4738-95fd-41e071b3e891','f927cf1e-3a42-11e8-af4b-507b9dc8fad2','80dc6290-9817-423a-90e0-d571a7925df6'),(15,NULL,NULL,0,'ee83c7b6-d22d-4013-8172-b397f0f31b51','c877f065-3a41-11e8-af4b-507b9dc8fad2','87879b76-1303-4b30-a3b5-2fef65a2aeee'),(16,NULL,NULL,0,'ee83c7b6-d22d-4013-8172-b397f0f31b51','de481f75-3a41-11e8-af4b-507b9dc8fad2','891b5331-576f-4919-8f42-702d4cad96ab'),(17,NULL,NULL,0,'6d8cb0bc-c43e-4bf3-a14b-db0141174071','880e79f3-3a41-11e8-af4b-507b9dc8fad2','be5cc5b6-0658-4d79-9203-54462bdda829'),(18,NULL,NULL,0,'c07f19e5-df3e-4893-b9dd-42ee09705b43','f927cf1e-3a42-11e8-af4b-507b9dc8fad2','c1317e64-4c4c-4c8b-a255-6d38f91597c9'),(19,NULL,NULL,0,'0451e108-da3b-4b87-ae62-f2ee1beb7c9b','d742bcfa-3a42-11e8-af4b-507b9dc8fad2','cddeffa9-c790-4bee-afc3-b572b5348f07'),(20,NULL,NULL,0,'7a6fdedc-80c8-486a-b545-f7aab65be9c2','9e0c17e4-3a41-11e8-af4b-507b9dc8fad2','d3edb56b-a90f-4c44-85e9-28825d3be57b'),(21,NULL,NULL,0,'ee83c7b6-d22d-4013-8172-b397f0f31b51','78e52b25-3a41-11e8-af4b-507b9dc8fad2','e6303f91-41c4-48e7-ba04-1ec5109642dc'),(22,NULL,NULL,0,'e7307f87-2405-11e8-8806-3ef40d61c1a6','b07a1165-3a41-11e8-af4b-507b9dc8fad2','e863ab45-7c6e-49ad-a324-7f2616a083d6');

/*Table structure for table `resouce_authority` */

DROP TABLE IF EXISTS `resouce_authority`;

CREATE TABLE `resouce_authority` (
  `id` varchar(50) NOT NULL,
  `resource_id` varchar(50) DEFAULT NULL,
  `authority_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `resouce_authority` */

insert  into `resouce_authority`(`id`,`resource_id`,`authority_id`) values ('0f7b28c9-38a3-4121-a0aa-eb4e7a5151f9','0451e108-da3b-4b87-ae62-f2ee1beb7c9b','12495b97-3a43-11e8-af4b-507b9dc8fad2'),('133057d7-de6b-49ad-9a6e-16158c5a01c0','0451e108-da3b-4b87-ae62-f2ee1beb7c9b','f927cf1e-3a42-11e8-af4b-507b9dc8fad2'),('13f71763-01e6-4e4e-927a-c0154f3d224a','2fc56a7f-b29c-42f7-8ade-d26ea3eaa0dc','18230089-3a42-11e8-af4b-507b9dc8fad2'),('170b5874-edf3-4d50-92b1-bed9df58e968','0d2adeca-08c7-4c3c-a332-6ec7262fb562','b07a1165-3a41-11e8-af4b-507b9dc8fad2'),('175005d6-4d57-4eb7-97e3-68c18e971524','1efea1bc-aecd-4787-823d-55d63fe54abf','331a840d-3a42-11e8-af4b-507b9dc8fad2'),('1dde580a-0712-4dd4-80b5-866d3977701c','47e1a143-0fee-4a1e-8bdc-8d659497aec4','278e9498-3a42-11e8-af4b-507b9dc8fad2'),('1f7569d6-1054-4e9f-a1f3-f84e6413fd25','ee83c7b6-d22d-4013-8172-b397f0f31b51','f0853008-3a41-11e8-af4b-507b9dc8fad2'),('2b6ed9cf-2fc1-4fc9-88f5-ef48d1ad7fb0','376ccf3e-a713-4738-95fd-41e071b3e891','b75a7c8b-3a42-11e8-af4b-507b9dc8fad2'),('458e0f1d-5ba5-4be1-81af-e13b9ae7d2c7','0d2adeca-08c7-4c3c-a332-6ec7262fb562','8bc0323d-3a42-11e8-af4b-507b9dc8fad2'),('56f13076-7ad1-4c8e-9fee-55bbcd8e26cc','5a9a6b50-789c-4b6c-83ae-7701b6259c51','1c68960f-3a41-11e8-af4b-507b9dc8fad2'),('5e4e8229-deed-47ec-8d7e-28ff58644f36','c07f19e5-df3e-4893-b9dd-42ee09705b43','c5951c74-3a42-11e8-af4b-507b9dc8fad2'),('6aad5d47-f126-4727-9e29-0bee16a0ee3d','03f1c9d6-b952-4c77-92d6-4ada566567c5','46d1ae66-3a42-11e8-af4b-507b9dc8fad2'),('6bd89b1f-5112-4cbd-a9bf-b703493f6d7f','0d2adeca-08c7-4c3c-a332-6ec7262fb562','6bd80bdb-3a42-11e8-af4b-507b9dc8fad2'),('80dc6290-9817-423a-90e0-d571a7925df6','376ccf3e-a713-4738-95fd-41e071b3e891','f927cf1e-3a42-11e8-af4b-507b9dc8fad2'),('87879b76-1303-4b30-a3b5-2fef65a2aeee','ee83c7b6-d22d-4013-8172-b397f0f31b51','c877f065-3a41-11e8-af4b-507b9dc8fad2'),('891b5331-576f-4919-8f42-702d4cad96ab','ee83c7b6-d22d-4013-8172-b397f0f31b51','de481f75-3a41-11e8-af4b-507b9dc8fad2'),('be5cc5b6-0658-4d79-9203-54462bdda829','6d8cb0bc-c43e-4bf3-a14b-db0141174071','880e79f3-3a41-11e8-af4b-507b9dc8fad2'),('c1317e64-4c4c-4c8b-a255-6d38f91597c9','c07f19e5-df3e-4893-b9dd-42ee09705b43','f927cf1e-3a42-11e8-af4b-507b9dc8fad2'),('cddeffa9-c790-4bee-afc3-b572b5348f07','0451e108-da3b-4b87-ae62-f2ee1beb7c9b','d742bcfa-3a42-11e8-af4b-507b9dc8fad2'),('d3edb56b-a90f-4c44-85e9-28825d3be57b','7a6fdedc-80c8-486a-b545-f7aab65be9c2','9e0c17e4-3a41-11e8-af4b-507b9dc8fad2'),('e6303f91-41c4-48e7-ba04-1ec5109642dc','ee83c7b6-d22d-4013-8172-b397f0f31b51','78e52b25-3a41-11e8-af4b-507b9dc8fad2'),('e863ab45-7c6e-49ad-a324-7f2616a083d6','e7307f87-2405-11e8-8806-3ef40d61c1a6','b07a1165-3a41-11e8-af4b-507b9dc8fad2');

/*Table structure for table `resource` */

DROP TABLE IF EXISTS `resource`;

CREATE TABLE `resource` (
  `id` varchar(50) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `code` varchar(200) DEFAULT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `menu_route` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `resource` */

insert  into `resource`(`id`,`name`,`type`,`code`,`pid`,`des`,`sort`,`path`,`menu_route`) values ('03f1c9d6-b952-4c77-92d6-4ada566567c5','角色管理',2,'category_role','a4afe6f7-2405-11e8-8806-3ef40d61c1a6','角色管理，对角色增删改、授权',1,'/category_authorization/category_role','role_manage'),('0451e108-da3b-4b87-ae62-f2ee1beb7c9b','用户授权',3,'btn_user_authorize','6bb173da-2405-11e8-8806-3ef40d61c1a6','用户授权',2,'/category_usermanage/btn_user_authorize',''),('0d2adeca-08c7-4c3c-a332-6ec7262fb562','角色授权',3,'btn_role_authorize','03f1c9d6-b952-4c77-92d6-4ada566567c5','角色授权按钮',4,'/category_authorization/category_role/btn_role_authorize',''),('1efea1bc-aecd-4787-823d-55d63fe54abf','修改角色',3,'btn_role_update','03f1c9d6-b952-4c77-92d6-4ada566567c5','修改角色按钮',2,'/category_authorization/category_role/btn_role_update',NULL),('200b5315-2406-11e8-8806-3ef40d61c1a6','首页',2,'category_index','pid','系统首页',3,'/category_index','index'),('2fc56a7f-b29c-42f7-8ade-d26ea3eaa0dc','新增角色',3,'btn_roleadd','03f1c9d6-b952-4c77-92d6-4ada566567c5','新增角色',1,'/category_authorization/category_role/btn_roeladd',NULL),('376ccf3e-a713-4738-95fd-41e071b3e891','修改用户',3,'btn_user_update','6bb173da-2405-11e8-8806-3ef40d61c1a6','修改用户',4,'/category_usermanage/btn_user_update',''),('3be78ae4-2406-11e8-8806-3ef40d61c1a6','体育馆简介',2,'category_introductio','pid','广东海洋大学体育馆简介',4,'/category_introduction','introduction'),('47e1a143-0fee-4a1e-8bdc-8d659497aec4','删除角色',3,'btn_role_delete','03f1c9d6-b952-4c77-92d6-4ada566567c5','删除角色，按钮',3,'/category_authorization/category_role/btn_role_delete',NULL),('5a9a6b50-789c-4b6c-83ae-7701b6259c51','权限码管理',2,'category_authority','a4afe6f7-2405-11e8-8806-3ef40d61c1a6','权限码管理',3,NULL,'authority_manage'),('6bb173da-2405-11e8-8806-3ef40d61c1a6','用户管理',2,'category_usermanage','pid','用户管理栏目，可以对用户增删改',1,'/category_usermanage','user_manage'),('6d8cb0bc-c43e-4bf3-a14b-db0141174071','修改资源',3,'btn_resource_update','e7307f87-2405-11e8-8806-3ef40d61c1a6','修改资源，按钮',3,'/category_authorization/category_resource/btn_resource_update',NULL),('7a6fdedc-80c8-486a-b545-f7aab65be9c2','删除资源',3,'btn_resource_delete','e7307f87-2405-11e8-8806-3ef40d61c1a6','删除资源',2,'/category_authorization/category_resource/btn_resource_delete',NULL),('a4afe6f7-2405-11e8-8806-3ef40d61c1a6','系统管理',2,'category_authorization','pid','系统管理栏目，可以操作角色、资源、权限码',2,'/category_authorization',NULL),('b70aeb1e-6bd6-4236-94fe-d14d5435679e','赛事管理',2,'category','pid','赛事管理，对赛事增删改查',1,'/category_game','game_manage'),('c07f19e5-df3e-4893-b9dd-42ee09705b43','删除用户',3,'btn_user_delete','6bb173da-2405-11e8-8806-3ef40d61c1a6','删除用户',3,'/category_usermanage/btn_user_delete',''),('e7307f87-2405-11e8-8806-3ef40d61c1a6','资源管理',2,'category_resource','a4afe6f7-2405-11e8-8806-3ef40d61c1a6','资源管理栏目，可以操作资源增删改、授权',2,'/category_authorization/category_resource','resource_manage'),('e7fdcb7c-43d9-423b-9c00-4847c21cfa35','新增资源',3,'btn_resource_add','e7307f87-2405-11e8-8806-3ef40d61c1a6','新增资源按钮',1,'/category_authorization/category_resource/btn_resource_add',NULL),('ee83c7b6-d22d-4013-8172-b397f0f31b51','资源授权',3,'btn_resource_authorize','e7307f87-2405-11e8-8806-3ef40d61c1a6','资源授权',4,'/category_authorization/category_resource/btn_resource_authorize',''),('f1e4ec99-2800-4d09-a622-632a05e28f52','用户新增',3,'btn_user_add','6bb173da-2405-11e8-8806-3ef40d61c1a6','',1,'/category_usermanage/btn_user_add','');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` varchar(50) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`id`,`name`,`code`,`sort`,`type`,`des`) values ('1ed77c6f-93a5-46c0-a865-29ec8901287c','权限码管理员','MANAGER_AUTHORITY',2,2,'权限码'),('9bd9b381-dbcc-4a48-bf0a-6951f569d686','资源管理员','MANAGER_RESOURCE',4,2,'资源管理员'),('a8bfc2b4-ff73-4c35-9a63-ae2c049d3b01','普通用户','NORMAL',5,1,'普通用户'),('eec145c6-59b4-488f-b597-3e3978566492','角色管理员','MANAGER_ROLE',3,2,'角色管理员IOOO'),('sdfdsfsdfadsfsa','用户管理员','MANAGER_USER',1,1,'用户管理员ok');

/*Table structure for table `role_business` */

DROP TABLE IF EXISTS `role_business`;

CREATE TABLE `role_business` (
  `id` varchar(50) NOT NULL,
  `role_id` varchar(50) DEFAULT NULL,
  `class_pk` varchar(50) DEFAULT NULL,
  `class_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role_business` */

/*Table structure for table `role_res` */

DROP TABLE IF EXISTS `role_res`;

CREATE TABLE `role_res` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime DEFAULT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `role_id` varchar(50) NOT NULL COMMENT '角色编号',
  `resource_id` varchar(50) NOT NULL COMMENT '资源编号',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `role_res` */

insert  into `role_res`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`role_id`,`resource_id`,`remark`) values (1,NULL,NULL,0,'1ed77c6f-93a5-46c0-a865-29ec8901287c','5a9a6b50-789c-4b6c-83ae-7701b6259c51','1d248c91-59c9-4802-a163-b48b3c541804'),(2,NULL,NULL,0,'9bd9b381-dbcc-4a48-bf0a-6951f569d686','e7fdcb7c-43d9-423b-9c00-4847c21cfa35','3dcf7693-c063-4864-9b42-8504a1f4593d'),(3,NULL,NULL,0,'9bd9b381-dbcc-4a48-bf0a-6951f569d686','7a6fdedc-80c8-486a-b545-f7aab65be9c2','46873177-8b52-4e8e-85b8-f192d06d2e3f'),(4,NULL,NULL,0,'9bd9b381-dbcc-4a48-bf0a-6951f569d686','6d8cb0bc-c43e-4bf3-a14b-db0141174071','4ba1c852-cd54-48f8-8b3b-bb4d7e940e50'),(5,NULL,NULL,0,'a8bfc2b4-ff73-4c35-9a63-ae2c049d3b01','3be78ae4-2406-11e8-8806-3ef40d61c1a6','5caa5cfb-af72-4148-a935-757e722a621e'),(6,NULL,NULL,0,'07','2fc56a7f-b29c-42f7-8ade-d26ea3eaa0dc','609c0f87-3a2b-4101-8fd3-2e3eb2d71629'),(7,NULL,NULL,0,'07','03f1c9d6-b952-4c77-92d6-4ada566567c5','7a23aae5-02d5-4283-8266-bc663d72ad87'),(8,NULL,NULL,0,'9bd9b381-dbcc-4a48-bf0a-6951f569d686','e7307f87-2405-11e8-8806-3ef40d61c1a6','7e6203d9-9c37-41d1-a3c1-218fed052dea'),(9,NULL,NULL,0,'eec145c6-59b4-488f-b597-3e3978566492','1efea1bc-aecd-4787-823d-55d63fe54abf','81835f28-031e-4c43-ba6e-b87ace85b427'),(10,NULL,NULL,0,'a8bfc2b4-ff73-4c35-9a63-ae2c049d3b01','200b5315-2406-11e8-8806-3ef40d61c1a6','83292177-46b8-4fce-a4af-ac8152a1b298'),(11,NULL,NULL,0,'07','47e1a143-0fee-4a1e-8bdc-8d659497aec4','a1ac4f7d-87ea-4ccd-a483-b417e4cf52cd'),(12,NULL,NULL,0,'sdfdsfsdfadsfsa','6bb173da-2405-11e8-8806-3ef40d61c1a6','eace25e1-99a2-42b7-abfc-5956d1e7a013'),(13,NULL,NULL,0,'07','0d2adeca-08c7-4c3c-a332-6ec7262fb562','f7296a9d-fa67-4f7a-9647-fad03e0aae3a');

/*Table structure for table `role_resource_r` */

DROP TABLE IF EXISTS `role_resource_r`;

CREATE TABLE `role_resource_r` (
  `id` varchar(50) NOT NULL,
  `role_id` varchar(50) DEFAULT NULL,
  `resource_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role_resource_r` */

/*Table structure for table `sys_authority` */

DROP TABLE IF EXISTS `sys_authority`;

CREATE TABLE `sys_authority` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime DEFAULT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `auth_id` varchar(50) NOT NULL COMMENT '授权编号',
  `auth_name` varchar(50) NOT NULL COMMENT '授权名称',
  `auth_code` varchar(50) NOT NULL,
  `auth_url` varchar(200) NOT NULL,
  `auth_method` varchar(200) NOT NULL,
  `auth_controller` varchar(255) NOT NULL,
  `auth_desc` varchar(200) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `sys_authority` */

insert  into `sys_authority`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`auth_id`,`auth_name`,`auth_code`,`auth_url`,`auth_method`,`auth_controller`,`auth_desc`,`remark`) values (1,NULL,NULL,0,'047b2f11-3a43-11e8-af4b-507b9dc8fad2','校验用户名唯一性，账号','user_isExist','/api/v1/user/isExist','GET','UserController','',NULL),(2,NULL,NULL,0,'12495b97-3a43-11e8-af4b-507b9dc8fad2','查单个用户所拥有的角色id','user_getRoleByUser','/api/v1/user/getRoleByUser','GET','UserController','',NULL),(3,NULL,NULL,0,'18230089-3a42-11e8-af4b-507b9dc8fad2','新增角色','role_addRole','/api/v1/role','POST','RoleController','',NULL),(4,NULL,NULL,0,'1c68960f-3a41-11e8-af4b-507b9dc8fad2','按controller分组，得到authority列表','authority_listByController','/api/v1/authority','GET','AuthorityController','',NULL),(5,NULL,NULL,0,'278e9498-3a42-11e8-af4b-507b9dc8fad2','删除角色','role_deleteRole','/api/v1/role/{roleId}','DELETE','RoleController','',NULL),(6,NULL,NULL,0,'331a840d-3a42-11e8-af4b-507b9dc8fad2','修改角色','role_updateRole','/api/v1/role','PUT','RoleController','',NULL),(7,NULL,NULL,0,'3aa50cdc-3a41-11e8-af4b-507b9dc8fad2','根据请求方式，按关键字模糊查询','authority_getLikeAuthority','/api/v1/listLikeAuthority','GET','AuthorityController','',NULL),(8,NULL,NULL,0,'46d1ae66-3a42-11e8-af4b-507b9dc8fad2','查询角色列表','role_listAllRole','/api/v1/role','GET','RoleController','',NULL),(9,NULL,NULL,0,'6bd80bdb-3a42-11e8-af4b-507b9dc8fad2','给角色授权','role_authorize','/api/v1/role/authorize','POST','RoleController','',NULL),(10,NULL,NULL,0,'78e52b25-3a41-11e8-af4b-507b9dc8fad2','新增资源','resource_addResource','/api/v1/resource','POST','ResourceController','',NULL),(11,NULL,NULL,0,'7eee4505-3a42-11e8-af4b-507b9dc8fad2','校验code唯一性','role_isExist','/api/v1/role/isExist','GET','RoleController','',NULL),(12,NULL,NULL,0,'880e79f3-3a41-11e8-af4b-507b9dc8fad2','修改资源','resource_updateResource','/api/v1/resource','PUT','ResourceController','',NULL),(13,NULL,NULL,0,'8bc0323d-3a42-11e8-af4b-507b9dc8fad2','得到某角色拥有的资源','role_listRoleResource','/api/v1/role/getRoleResource','GET','RoleController','',NULL),(14,NULL,NULL,0,'9e0c17e4-3a41-11e8-af4b-507b9dc8fad2','删除资源','resource_deleteResource','/api/v1/resource/{resourceId}','DELETE','ResourceController','',NULL),(15,NULL,NULL,0,'a516aa5f-3a42-11e8-af4b-507b9dc8fad2','新增用户','user_addUser','/api/v1/user','POST','UserController','',NULL),(16,NULL,NULL,0,'b07a1165-3a41-11e8-af4b-507b9dc8fad2','查询资源列表','resource_listAllResource','/api/v1/resource','GET','ResourceController','',NULL),(17,NULL,NULL,0,'b75a7c8b-3a42-11e8-af4b-507b9dc8fad2','修改用户','user_updateUser','/api/v1/user','PUT','UserController','',NULL),(18,NULL,NULL,0,'c5951c74-3a42-11e8-af4b-507b9dc8fad2','删除用户','user_deleteUser','/api/v1/user/{userId}','DELETE','UserController','',NULL),(19,NULL,NULL,0,'c877f065-3a41-11e8-af4b-507b9dc8fad2','查单个资源 拥有的操作码','resource_listAuthorityByResource','/api/v1/resource/listAuthorityByResource','GET','ResourceController','',NULL),(20,NULL,NULL,0,'d742bcfa-3a42-11e8-af4b-507b9dc8fad2','将角色分配用户','user_assign','/api/v1/user/assign','POST','UserController','',NULL),(21,NULL,NULL,0,'de481f75-3a41-11e8-af4b-507b9dc8fad2','取消授权,取消一个资源的某个 或一些 权限','resource_deleteResourceAuthority','/api/v1/resource/deleteResourceAuthority','POST','ResourceController','',NULL),(22,NULL,NULL,0,'f0853008-3a41-11e8-af4b-507b9dc8fad2','给资源授权,单个操作码给单个资源','resource_authorize','/api/v1/resource/authorize','POST','ResourceController','',NULL),(23,NULL,NULL,0,'f927cf1e-3a42-11e8-af4b-507b9dc8fad2','查所有用户','user_listAllUser','/api/v1/user','GET','UserController','',NULL);

/*Table structure for table `sys_resource` */

DROP TABLE IF EXISTS `sys_resource`;

CREATE TABLE `sys_resource` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime DEFAULT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `res_id` varchar(50) NOT NULL COMMENT '资源编号',
  `res_name` varchar(50) NOT NULL COMMENT '资源名称',
  `res_type` tinyint(4) NOT NULL,
  `res_code` varchar(50) NOT NULL,
  `res_pid` varchar(50) NOT NULL,
  `res_desc` varchar(200) NOT NULL,
  `res_sort` tinyint(4) NOT NULL,
  `res_path` varchar(200) DEFAULT NULL,
  `menu_route` varchar(255) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `sys_resource` */

insert  into `sys_resource`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`res_id`,`res_name`,`res_type`,`res_code`,`res_pid`,`res_desc`,`res_sort`,`res_path`,`menu_route`,`remark`) values (1,'2018-07-03 15:37:36','2018-07-03 15:37:38',0,'03f1c9d6-b952-4c77-92d6-4ada566567c5','角色管理',2,'category_role','a4afe6f7-2405-11e8-8806-3ef40d61c1a6','角色管理，对角色增删改、授权',1,'/category_authorization/category_role','role_manage',NULL),(2,'2018-07-03 15:37:39','2018-07-03 15:37:41',0,'0451e108-da3b-4b87-ae62-f2ee1beb7c9b','用户授权',3,'btn_user_authorize','6bb173da-2405-11e8-8806-3ef40d61c1a6','用户授权',2,'/category_usermanage/btn_user_authorize','',NULL),(3,'2018-07-03 15:37:42','2018-07-03 15:37:44',0,'0d2adeca-08c7-4c3c-a332-6ec7262fb562','角色授权',3,'btn_role_authorize','03f1c9d6-b952-4c77-92d6-4ada566567c5','角色授权按钮',4,'/category_authorization/category_role/btn_role_authorize','',NULL),(4,'2018-07-03 15:37:45','2018-07-03 15:37:47',0,'1efea1bc-aecd-4787-823d-55d63fe54abf','修改角色',3,'btn_role_update','03f1c9d6-b952-4c77-92d6-4ada566567c5','修改角色按钮',2,'/category_authorization/category_role/btn_role_update',NULL,NULL),(5,'2018-07-03 15:37:49','2018-07-03 15:37:51',0,'200b5315-2406-11e8-8806-3ef40d61c1a6','首页',2,'category_index','pid','首页',3,'/category_index','index',NULL),(6,'2018-07-03 15:37:52','2018-07-03 15:37:54',0,'2fc56a7f-b29c-42f7-8ade-d26ea3eaa0dc','新增角色',3,'btn_roeladd','03f1c9d6-b952-4c77-92d6-4ada566567c5','新增角色',1,'/category_authorization/category_role/btn_roeladd',NULL,NULL),(7,'2018-07-03 15:37:55','2018-07-03 15:37:57',0,'376ccf3e-a713-4738-95fd-41e071b3e891','修改用户',3,'btn_user_update','6bb173da-2405-11e8-8806-3ef40d61c1a6','修改用户',4,'/category_usermanage/btn_user_update','',NULL),(8,'2018-07-03 15:37:58','2018-07-03 15:38:00',0,'3be78ae4-2406-11e8-8806-3ef40d61c1a6','体育馆简介',2,'category_introduction','pid','广东海洋大学体育馆介绍',4,'/category_introduction','introduction',NULL),(9,'2018-07-03 15:38:01','2018-07-03 15:38:04',0,'47e1a143-0fee-4a1e-8bdc-8d659497aec4','删除角色',3,'btn_role_delete','03f1c9d6-b952-4c77-92d6-4ada566567c5','删除角色，按钮',3,'/category_authorization/category_role/btn_role_delete',NULL,NULL),(10,'2018-07-03 15:38:05','2018-07-03 15:38:07',0,'5a9a6b50-789c-4b6c-83ae-7701b6259c51','权限码管理',2,'category_authority','a4afe6f7-2405-11e8-8806-3ef40d61c1a6','权限码管理',3,NULL,'authority_manage',NULL),(11,'2018-07-03 15:38:24','2018-07-03 15:38:25',0,'6bb173da-2405-11e8-8806-3ef40d61c1a6','用户管理',2,'category_usermanage','pid','用户管理栏目，可以对用户增删改',1,'/category_usermanage','user_manage',NULL),(12,'2018-07-03 15:38:28','2018-07-03 15:38:29',0,'6d8cb0bc-c43e-4bf3-a14b-db0141174071','修改资源',3,'btn_resource_update','e7307f87-2405-11e8-8806-3ef40d61c1a6','修改资源，按钮',3,'/category_authorization/category_resource/btn_resource_update',NULL,NULL),(13,'2018-07-03 15:38:31','2018-07-03 15:38:33',0,'7a6fdedc-80c8-486a-b545-f7aab65be9c2','删除资源',3,'btn_resource_delete','e7307f87-2405-11e8-8806-3ef40d61c1a6','删除资源',2,'/category_authorization/category_resource/btn_resource_delete',NULL,NULL),(14,'2018-07-03 15:38:35','2018-07-03 15:38:37',0,'a4afe6f7-2405-11e8-8806-3ef40d61c1a6','系统管理',2,'category_system','pid','系统管理，可以操作角色、资源、权限码',2,'/category_authorization',NULL,NULL),(15,'2018-07-03 15:38:39','2018-07-03 15:38:48',0,'c07f19e5-df3e-4893-b9dd-42ee09705b43','删除用户',3,'btn_user_delete','6bb173da-2405-11e8-8806-3ef40d61c1a6','删除用户',3,'/category_usermanage/btn_user_delete','',NULL),(16,'2018-07-03 15:38:47','2018-07-03 15:38:50',0,'e7307f87-2405-11e8-8806-3ef40d61c1a6','资源管理',2,'category_resource','a4afe6f7-2405-11e8-8806-3ef40d61c1a6','资源管理栏目，可以操作资源增删改、授权',2,'/category_authorization/category_resource','resource_manage',NULL),(17,'2018-07-03 15:38:40','2018-07-03 15:38:53',0,'e7fdcb7c-43d9-423b-9c00-4847c21cfa35','新增资源',3,'btn_resource_add','e7307f87-2405-11e8-8806-3ef40d61c1a6','新增资源按钮',1,'/category_authorization/category_resource/btn_resource_add',NULL,NULL),(18,'2018-07-03 15:38:41','2018-07-03 15:38:52',0,'ee83c7b6-d22d-4013-8172-b397f0f31b51','资源授权',3,'btn_resource_authorize','e7307f87-2405-11e8-8806-3ef40d61c1a6','资源授权',4,'/category_authorization/category_resource/btn_resource_authorize','',NULL),(19,'2018-07-03 15:38:44','2018-07-03 15:38:55',0,'f1e4ec99-2800-4d09-a622-632a05e28f52','用户新增',3,'btn_user_add','6bb173da-2405-11e8-8806-3ef40d61c1a6','',1,'/category_usermanage/btn_user_add','',NULL);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime DEFAULT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `role_id` varchar(50) NOT NULL COMMENT '角色编号，业务主键（8位日期+2位自增数）',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称（超级管理员、模块管理员、普通用户）',
  `role_code` varchar(50) DEFAULT NULL,
  `role_sort` tinyint(4) DEFAULT NULL,
  `role_type` tinyint(2) DEFAULT NULL,
  `role_desc` varchar(200) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`role_id`,`role_name`,`role_code`,`role_sort`,`role_type`,`role_desc`,`remark`) values (1,'2018-06-28 16:06:59','2018-06-28 16:06:59',0,'01','超级管理员','MANAGER_SUPER',9,1,'超级管理员',''),(2,NULL,NULL,0,'07','系统管理员','MANAGER_AUTHORITY',2,2,'权限管理员',NULL),(3,NULL,NULL,0,'07','系统管理员','MANAGER_RESOURCE',4,2,'资源管理员',NULL),(4,NULL,NULL,0,'09','普通用户','NORMAL',5,1,'普通用户',NULL),(5,NULL,NULL,0,'07','系统管理员','MANAGER_ROLE',3,2,'角色管理员',NULL),(6,NULL,NULL,0,'08','用户管理员','MANAGER_USER',1,1,'用户管理员',NULL);

/*Table structure for table `sys_time` */

DROP TABLE IF EXISTS `sys_time`;

CREATE TABLE `sys_time` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `time_id` varchar(10) NOT NULL COMMENT '时间段编号，业务主键（8位日期+2位自增数）',
  `periods_start` varchar(10) NOT NULL COMMENT '时间段开始时间',
  `periods_end` varchar(10) NOT NULL COMMENT '时间段结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_time` */

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号，业务主键（16位）',
  `user_name` varchar(50) NOT NULL COMMENT '用户名称',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `phone` varchar(20) NOT NULL COMMENT '手机号码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `super_admin` bit(1) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`user_id`,`user_name`,`password`,`phone`,`email`,`super_admin`,`remark`) values (1,'2018-06-28 16:01:33','2018-06-28 16:01:33',0,'1012244599101538305','AA','4737ea7762a4155c14c9b609a59d5967','123','123@qq.com','','aa'),(3,'2018-07-03 13:03:45','2018-07-03 13:03:46',0,'1014011798476570625','BB','171a3f8f5131536d447d21cd148b6d78','12345678901','123@qq.com','\0','bb'),(4,'2018-07-03 13:11:24','2018-07-03 13:11:24',0,'1014013717341315073','CC','2a3801079fe6732bdc6157d87fb84578','12345678901','123@qq.com','\0','cc'),(6,'2018-07-03 14:21:56','2018-07-03 14:21:56',0,'1014031467338039298','EE','3010c04927f3d51a82c44ef2088df56f','12345678901','123@qq.com','\0','ee');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(50) NOT NULL,
  `login_name` varchar(20) DEFAULT NULL,
  `real_name` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `superman` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`login_name`,`real_name`,`password`,`superman`) values ('05068cb0-131b-11e8-8806-3ef40d61c1a6','melo','melo','12345678',''),('76fbd9f0-cc74-4314-b007-e66bed436b33','james','勒布朗','12345678','\0'),('8127a2e6-69d9-4622-98c2-b81d1942080c','curry','库里','12345678','\0'),('cd3d796e-1ddc-473f-9004-76bc65e19fd3','kobe','Bryenttttttt','12345678','\0');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号，表主键（自增1）',
  `gmt_create` datetime DEFAULT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '记录更新时间',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除（1已删除，0未删除）',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `role_id` varchar(50) NOT NULL COMMENT '角色编号',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`id`,`gmt_create`,`gmt_modified`,`is_deleted`,`user_id`,`role_id`,`remark`) values (1,'2018-07-03 16:46:10','2018-07-03 16:46:11',0,'1012244599101538305','01','50b403e7-72b2-42f5-a0a7-8271e9b8342f'),(2,'2018-07-03 16:47:06','2018-07-03 16:47:08',0,'171a3f8f5131536d447d21cd148b6d78','09','5bfad5e6-43fe-4b48-83e4-8b7ffc9dde83'),(3,'2018-07-03 19:56:56','2018-07-03 19:56:58',0,'1014011798476570625','07','7b666d15-9af5-49a7-9a9a-fb8fe3de0855');

/*Table structure for table `user_role_r` */

DROP TABLE IF EXISTS `user_role_r`;

CREATE TABLE `user_role_r` (
  `id` varchar(50) NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `role_id` varchar(50) DEFAULT NULL,
  `yw_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_role_r` */

insert  into `user_role_r`(`id`,`user_id`,`role_id`,`yw_id`) values ('50b403e7-72b2-42f5-a0a7-8271e9b8342f','cd3d796e-1ddc-473f-9004-76bc65e19fd3','a8bfc2b4-ff73-4c35-9a63-ae2c049d3b01',NULL),('ae0b234b-43f6-4cd5-9733-d7b8d1e19434','8127a2e6-69d9-4622-98c2-b81d1942080c','eec145c6-59b4-488f-b597-3e3978566492',NULL),('eaf561d9-8fa8-4a14-9c83-9c1b35eac5b2','8127a2e6-69d9-4622-98c2-b81d1942080c','a8bfc2b4-ff73-4c35-9a63-ae2c049d3b01',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
