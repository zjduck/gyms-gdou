package com.hlhome.cllz.gyms.module.ground.mapper;

import com.hlhome.cllz.gyms.module.ground.domain.GroundTypeDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 场地类型DAO类
 *
 * Created by luckyTsai on 2018/6/23
 */
@Mapper
public interface GroundTypeMapper {

    /**
     * 通过场地类型id删除场地类型
     */
    int deleteByTypeId(String typeId);

    /**
     * 新增场地类型
     */
    int insert(GroundTypeDO groundTypeDO);

    /**
     * 通过场地类型id获取场地类型
     */
    GroundTypeDO getByTypeId(String typeId);

    /**
     * 通过场地类型名称获取场地类型
     */
    GroundTypeDO getByTypeName(String typeName);

    /**
     * 统计所有场地类型
     */
    List<GroundTypeDO> listAll();

    /**
     * 更新场地类型
     */
    int update(GroundTypeDO groundTypeDO);

    /**
     * 通过场地类型，获取最新的场地类型
     */
    GroundTypeDO getLatestByGroundType(String groundType);
}
