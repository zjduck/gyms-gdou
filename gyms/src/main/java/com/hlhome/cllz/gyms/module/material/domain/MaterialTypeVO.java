package com.hlhome.cllz.gyms.module.material.domain;

/**
 * 器材类型VO类
 *
 *Created by zmz on 2018/6/23
 */

public class MaterialTypeVO {

    private String typeId;
    private String typeName;
    private String remark;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString(){
        return "MaterialTypeVO{"+
                ",typeId"+typeId+'\'' +
                ",typeName"+typeName+'\'' +
                ",remark"+remark+'\'' +
                "}";
    }
}
