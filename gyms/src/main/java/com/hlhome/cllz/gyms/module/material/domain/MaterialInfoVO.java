package com.hlhome.cllz.gyms.module.material.domain;

import java.math.BigDecimal;

/**
 * 器材信息VO类
 *
 *Created by zmz on 2018/6/23
 */

public class MaterialInfoVO {

    private String materialId;
    private String materialName;
    private String materialType;
    private Integer materialNumber;
    private Integer materialResidue;
    private String buyTime;
    private BigDecimal rent;
    private String remark;

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public Integer getMaterialNumber() {
        return materialNumber;
    }

    public void setMaterialNumber(Integer materialNumber) {
        this.materialNumber = materialNumber;
    }

    public Integer getMaterialResidue() {
        return materialResidue;
    }

    public void setMaterialResidue(Integer materialResidue) {
        this.materialResidue = materialResidue;
    }

    public String getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(String buyTime) {
        this.buyTime = buyTime;
    }

    public BigDecimal getRent() {
        return rent;
    }

    public void setRent(BigDecimal rent) {
        this.rent = rent;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString(){
        return "MaterialInfoVO{"+
                ",materialId"+materialId+'\'' +
                ",materialName"+materialName+'\'' +
                ",materialType"+materialType+'\'' +
                ",materialNumber"+materialNumber+'\'' +
                ",materialResidue"+materialResidue+'\'' +
                ",buyTime"+buyTime+'\'' +
                ",rent"+rent+'\'' +
                ",remark"+remark+'\'' +
                "}";
    }
}
