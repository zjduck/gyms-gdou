package com.hlhome.cllz.gyms.module.material.service;

import com.hlhome.cllz.gyms.module.material.domain.MaterialTypeDO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialTypeVO;
import com.hlhome.cllz.gyms.module.material.mapper.MaterialTypeMapper;
import com.hlhome.cllz.gyms.util.Asserts;
import com.hlhome.cllz.gyms.util.POJOUtils;
import com.hlhome.cllz.gyms.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 材类型业务层
 *
 * created by zmz on 2018/6/23
 */
@Service
@Transactional
public class MaterialTypeServiceImpl implements  MaterialTypeService {

    @Autowired
    private MaterialTypeMapper materialTypeMapper;

    @Override
    public List<MaterialTypeVO> listAll() {
        List<MaterialTypeVO> materialTypeVOList = new ArrayList<>();
        List<MaterialTypeDO> materialTypeDOList = materialTypeMapper.listAll();
        for(MaterialTypeDO materialTypeDO : materialTypeDOList){
            MaterialTypeVO materialTypeVO = POJOUtils.DO2VO(materialTypeDO,MaterialTypeVO.class);
            materialTypeVOList.add(materialTypeVO);
        }
        return materialTypeVOList;
    }

    @Override
    public boolean saveMaterialType(MaterialTypeVO materialTypeVO) {
        boolean flag =false;

        Asserts.validate(materialTypeVO,"materialTypeVO");

        MaterialTypeDO materialTypeDO = new MaterialTypeDO();
        materialTypeDO = POJOUtils.VO2DO(materialTypeVO,MaterialTypeDO.class);
        materialTypeDO.setGmtCreate(LocalDateTime.now().withNano(0));
        materialTypeDO.setGmtModified(LocalDateTime.now().withNano(0));


        String materialNum = SystemUtils.materialTypeBind(materialTypeVO.getTypeName());
        materialTypeDO.setTypeId(materialNum);

        int result = materialTypeMapper.insert(materialTypeDO);
        flag = result == 0 ? false : true ;
        return flag;
    }

    @Override
    public boolean removeByTypeId(String typeId) {
        boolean flag = false;
        int result = materialTypeMapper.deleteByTypeId(typeId);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public boolean updateMaterialType(MaterialTypeVO materialTypeVO) {
        boolean flag = false;
        MaterialTypeDO materialTypeDO = POJOUtils.VO2DO(materialTypeVO, MaterialTypeDO.class);
        materialTypeDO.setGmtModified(LocalDateTime.now().withNano(0));
        String materialNum = SystemUtils.materialTypeBind(materialTypeVO.getTypeName());
        materialTypeDO.setTypeId(materialNum);

        int result = materialTypeMapper.update(materialTypeDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public MaterialTypeVO getByTypeId(String typeId) {
        MaterialTypeDO materialTypeDO = materialTypeMapper.getByTypeId(typeId);
        return POJOUtils.DO2VO(materialTypeDO, MaterialTypeVO.class);
    }

    @Override
    public MaterialTypeVO getByTypeName(String typeName) {
        MaterialTypeDO materialTypeDO = materialTypeMapper.getByTypeName(typeName);
        return POJOUtils.DO2VO(materialTypeDO, MaterialTypeVO.class);
    }
}
