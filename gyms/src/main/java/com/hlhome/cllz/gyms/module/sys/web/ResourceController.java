package com.hlhome.cllz.gyms.module.sys.web;

import com.hlhome.cllz.gyms.module.sys.domain.Authority;
import com.hlhome.cllz.gyms.module.sys.domain.ResourceSaveVM;
import com.hlhome.cllz.gyms.module.sys.domain.ResourceUpdateVM;
import com.hlhome.cllz.gyms.module.sys.domain.ResourceVM;
import com.hlhome.cllz.gyms.module.sys.service.impl.ResourceAuthorityService;
import com.hlhome.cllz.gyms.module.sys.service.impl.ResourceService;
import com.hlhome.cllz.gyms.util.Constants;
import com.hlhome.cllz.gyms.util.Message;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/api/v1")
public class ResourceController {
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	ResourceAuthorityService resourceAuthorityService;


	@RequiresPermissions(value={"resource_addResource","administrator"},logical= Logical.OR)
	@RequestMapping(value="/resource",method=RequestMethod.POST)
	public Message<String> addResource(ResourceSaveVM resourceSaveVM){
		resourceService.addResource(resourceSaveVM);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"resource_updateResource","administrator"},logical= Logical.OR)
	@RequestMapping(value="/resource",method=RequestMethod.PUT)
	public Message<String> updateResource( ResourceUpdateVM resourceUpdateVM){
		resourceService.updateResource(resourceUpdateVM);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"resource_deleteResource","administrator"},logical= Logical.OR)
	@RequestMapping(value="/resource/{resourceId}",method=RequestMethod.DELETE)
	public Message<String> deleteResource(@PathVariable(value="resourceId")String resourceId){
		resourceService.deleteResource(resourceId);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"resource_getAllResource","administrator"},logical= Logical.OR)
	@RequestMapping(value="/resource",method=RequestMethod.GET)
	public Message<List<ResourceVM>>getAllResource(){
		List<ResourceVM> resouceList=resourceService.getAllResource();
		return Message.ok(resouceList);
	}
	

//	@RequiresPermissions(value={"resource_getCurrentUserResourceTree","administrator"},logical=Logical.OR)
	@RequestMapping(value="/resource/getCurrentUserResourceTree",method=RequestMethod.GET)
	public Message<List<ResourceVM>>getCurrentUserResourceTree(){
		List<ResourceVM> list=resourceService.getCurrentUserResourceTree();
		return Message.ok(list);
	}
	

	@RequestMapping(value="/resource/getAuthorityByResource",method=RequestMethod.GET)
	public Message<List<Authority>>getAuthorityByResource(@RequestParam(value="ResourceId")String ResourceId){
		List<Authority> list=resourceAuthorityService.getAuthorityByResource(ResourceId);
		return Message.ok(list);
	}
	

	@RequiresPermissions(value={"resource_deleteResourceAuthority","administrator"},logical= Logical.OR)
	@RequestMapping(value="/resource/deleteResourceAuthority",method=RequestMethod.POST)
	public Message<String>deleteResourceAuthority(@RequestParam(value="resourceId")String resourceId,@RequestParam(value="authorityId")String authorityId){
		resourceAuthorityService.deleteResourceAuthority(resourceId,authorityId);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"resource_authorize","administrator"},logical= Logical.OR)
	@RequestMapping(value="/resource/authorize",method=RequestMethod.POST)
	public Message<String> authorize(@RequestParam(value="resourceId")String resourceId,@RequestParam(value="authorityId")String authorityId){
		resourceAuthorityService.authorize(resourceId,authorityId);
		return Message.ok(Constants.SUCCESS);
	}
}
