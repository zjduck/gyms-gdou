package com.hlhome.cllz.gyms.module.material.domain;

import java.time.LocalDateTime;

/**
 * 器材租借DO类
 *
 *Created by zmz on 2018/6/23
 */

public class MaterialRentDO {

    private Integer id;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModified;
    private Boolean isDeleted;

    private String materialId;
    private String bookId;
    private Integer rentNumber;
    private String rentTime;
    private String auditState;
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isdeleted) {
        isDeleted = isdeleted;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public Integer getRentNumber() {
        return rentNumber;
    }

    public void setRentNumber(Integer rentNumber) {
        this.rentNumber = rentNumber;
    }

    public String getRentTime() {
        return rentTime;
    }

    public void setRentTime(String rentTime) {
        this.rentTime = rentTime;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString(){
        return "MaterialRentDO{"+
                "id="+id+
                ",gmtCreate"+gmtCreate+
                ",gmtModified"+gmtModified+
                ",isDeleted"+isDeleted+
                ",materialId"+materialId+'\'' +
                ",bookId"+bookId+'\'' +
                ",rentNumber"+rentNumber+'\'' +
                ",rentTime"+rentTime+'\'' +
                ",auditState"+auditState+'\'' +
                ",remark"+remark+'\'' +
                "}";
    }
}
