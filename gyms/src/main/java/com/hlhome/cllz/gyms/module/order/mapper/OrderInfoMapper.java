package com.hlhome.cllz.gyms.module.order.mapper;

import com.hlhome.cllz.gyms.module.order.domain.OrderInfoDO;

import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
public interface OrderInfoMapper {

    int deleteByOrderId(String orderId);

    int insert(OrderInfoDO orderInfoDO);

    OrderInfoDO getByOrderId(String orderId);

    OrderInfoDO getByBookId(String bookId);

    List<OrderInfoDO> listAll();

    int update(OrderInfoDO orderInfoDO);

    /**
     * 通过Id排序 获取最新的订单记录
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.module.book.domain.OrderInfoDO
     */
    OrderInfoDO getLatestOrderById();
}
