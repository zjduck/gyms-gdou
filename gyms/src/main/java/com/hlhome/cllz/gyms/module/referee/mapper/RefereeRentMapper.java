package com.hlhome.cllz.gyms.module.referee.mapper;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
@Mapper
public interface RefereeRentMapper {

    int insert(RefereeRentDO refereeRentDO);

    int deleteByRefereeId(String refereeId);

    int update(RefereeRentDO refereeRentDO);

    RefereeRentDO getByRefereeId(String refereeId);

    List<RefereeRentDO> listByBookId(String bookId);

    List<RefereeRentDO> listAll();

    RefereeRentDO getLatestByRefereeItem(String refereeItem);

    List<RefereeRentDO> listByRefereeIdAndDate(String refereeId, String date);
}
