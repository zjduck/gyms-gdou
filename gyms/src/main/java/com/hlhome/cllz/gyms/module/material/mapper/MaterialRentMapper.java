package com.hlhome.cllz.gyms.module.material.mapper;

import com.hlhome.cllz.gyms.module.material.domain.MaterialRentDO;

import java.util.List;

/**
 * 器材租借Mapper(DAO)类
 *
 * Created by zmz on 2018/6/23
 */

public interface MaterialRentMapper {

    /**
     *新增器材租借信息
     */
    int insert (MaterialRentDO materialRentDO);

    /**
     *通过预约id，删除器材租借信息
     */
    int deleteByBookIdAndMaterialId (MaterialRentDO materialRentDO);

    /**
     *更新器材租借信息
     */
    int update (MaterialRentDO materialRentDO);

    /**
     *遍历器材租借信息
     */
    List<MaterialRentDO> listAll();

    /**
     *通过预约id，查询器材租借信息
     */
    List<MaterialRentDO> listByBookId(String bookId);

    /**
     *通过器材id，查询器材租借信息
     */
    List<MaterialRentDO> listByMaterialId(String materialId);

    /**
     *通过器材id和预约id，查询器材租借信息
     */
    MaterialRentDO getByMaterialIdAndBookId(String materialId, String bookId);


}
