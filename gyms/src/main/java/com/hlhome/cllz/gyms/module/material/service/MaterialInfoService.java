package com.hlhome.cllz.gyms.module.material.service;

import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoVO;

import java.util.List;

/**
 * 器材信息服务接口
 *
 * created by zmz on 2018/6/23
 */

public interface MaterialInfoService {

    /**
     * 统计所有器材信息
     */
    List<MaterialInfoVO> listAll();

    /**
     * 新增器材信息
     */
    boolean saveMaterialInfo(MaterialInfoVO materialInfoVO);

    /**
     * 通过器材id，删除器材信息
     */
    boolean removeByMaterialId (String materialId);

    /**
     * 更改器材信息
     */
    boolean updateMaterialInfo(MaterialInfoVO materialInfoVO);

    /**
     * 通过器材id，获取器材信息
     */
    MaterialInfoVO getByMaterialId (String materialId);

    /**
     * 通过器材名称，获取器材信息
     */
    MaterialInfoVO getByMaterialName (String materialName);

}
