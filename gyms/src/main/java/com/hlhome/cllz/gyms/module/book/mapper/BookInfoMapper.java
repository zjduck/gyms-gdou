package com.hlhome.cllz.gyms.module.book.mapper;

import com.hlhome.cllz.gyms.module.book.domain.BookInfoDO;

import java.util.List;

/**
 * Created by lingb on 2018/6/23
 */
public interface BookInfoMapper {

    int deleteByBookId(String bookId);

    int insert(BookInfoDO bookInfoDO);

    BookInfoDO getByBookId(String bookId);

    BookInfoDO getByBookName(String bookName);

    List<BookInfoDO> listAll();

    int update(BookInfoDO bookInfoDO);

    /**
     * 通过Id排序 获取最新的预约记录
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.module.book.domain.BookInfoDO
     */
    BookInfoDO getLatestOrderById();

}
