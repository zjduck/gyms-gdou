package com.hlhome.cllz.gyms.module.referee.domain;

import java.time.LocalDateTime;

/**
 * 裁判租借DO类
 *
 * @author LoongMH
 * @date 2018/06/23
 */
public class RefereeRentDO {

    private Integer id;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModified;
    private Integer isDeleted;

    private String refereeId;
    private String refereeItem;
    private String bookId;
    private String rentTime;
    private String periods;
    private String auditState;
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(String refereeId) {
        this.refereeId = refereeId;
    }

    public String getRefereeItem() {
        return refereeItem;
    }

    public void setRefereeItem(String refereeItem) {
        this.refereeItem = refereeItem;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getRentTime() {
        return rentTime;
    }

    public void setRentTime(String rentTime) {
        this.rentTime = rentTime;
    }

    public String getPeriods() { return periods; }

    public void  setPeriods(String periods) { this.periods = periods; }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "RefereeRentDO{" +
                "id=" + id +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", gmtModified='" + gmtModified + '\'' +
                ", isDeleted=" + isDeleted +
                ", rentId='" + refereeId + '\'' +
                ", sportItem='" + refereeItem + '\'' +
                ", bookId='" + bookId + '\'' +
                ", rentTime='" + rentTime + '\'' +
                ", periods='" + periods + '\'' +
                ", auditState='" + auditState + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}