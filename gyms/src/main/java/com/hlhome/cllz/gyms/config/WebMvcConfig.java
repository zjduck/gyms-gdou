package com.hlhome.cllz.gyms.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * WebMvc 全局配置类
 *
 * Created by lingb on 2018/5/26
 */
@Configuration
//public class WebMvcConfig implements WebMvcConfigurer {  // SpringBoot2.0+
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    /**
     * 格式化，字符串 转成 日期
     * @param   [registry]
     * @return  void
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new DateFormatter("yyyy-MM-dd HH:mm:ss"));
    }

/*

    */
/**
     *
     * 实现自定义拦截器只需要3步：
     * 1、创建我们自己的拦截器类并实现 HandlerInterceptor 接口。
     * 2、创建一个Java类实现WebMvcConfigurer，并重写 addInterceptors 方法。
     * 3、实例化我们自定义的拦截器，然后将对像手动添加到拦截器链中（在addInterceptors方法中添加）。
     *
     *//*


    */
/**
     * 此方法把该拦截器实例化成一个bean,否则在拦截器里无法注入其它bean
     * @return
     *//*

    @Bean
    SessionInterceptor sessionInterceptor() {
        return new SessionInterceptor();
    }

    */
/**
     * 配置拦截器
     * @param registry
     *//*

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(sessionInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/login","/permission/userInsert",
                        "/error","/tUser/insert","/gif/getGifCode");
    }
*/

    /**
     * 解决静态资源访问问题
     *
     * @param   [registry]
     * @return  void
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");

    }

    /**
     * 解决跨域访问 Access-Control-Allow-Origin 问题
     *
     * Access-Control-Allow-Origin：允许访问的客户端域名，例如：http://web.xxx.com，若为*，则表示从任意域都能访问，即不做任何限制。
     * Access-Control-Allow-Methods：允许访问的方法名，多个方法名用逗号分割，例如：GET,POST,PUT,DELETE,OPTIONS。
     * Access-Control-Allow-Credentials：是否允许请求带有验证信息，若要获取客户端域下的cookie时，需要将其设置为true。
     * Access-Control-Allow-Headers：允许服务端访问的客户端请求头，多个请求头用逗号分割，例如：Content-Type。
     * Access-Control-Expose-Headers：允许客户端访问的服务端响应头，多个响应头用逗号分割。
     *
     * @param   [registry]
     * @return  void
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/v1/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "POST", "PUT", "DELETE", "PATCH")
                .allowedHeaders("*")
                .exposedHeaders("access-control-allow-headers",
                        "access-control-allow-methods",
                        "access-control-allow-origin",
                        "access-control-max-age",
                        "X-Frame-Options")
                .allowCredentials(true).maxAge(3600);
    }
}