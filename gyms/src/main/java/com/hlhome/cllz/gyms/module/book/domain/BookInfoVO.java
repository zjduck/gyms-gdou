package com.hlhome.cllz.gyms.module.book.domain;

import com.hlhome.cllz.gyms.module.game.domain.GameInfoVO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoVO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundRentVO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoVO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialRentVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentVO;

import java.util.List;

/**
 * Created by lingb on 2018/6/23
 */
public class BookInfoVO {

    private String bookId;
    private String userId;
    private String bookTime;
    private String auditState;
    private String bookState;
    private String remark;

    /**
     * 页面预约信息打包到后台
     */
    private GameInfoVO gameInfoVO;
    private RefereeInfoVO refereeInfoVO;
    private GroundInfoVO groundInfoVO;
    private MaterialInfoVO materialInfoVO;

    /**
     * 数据库预约信息回显到页面
     * 赛事特别
     */
    private List<RefereeRentVO> refereeRentVOList;
    private List<GroundRentVO> groundRentVOList;
    private List<MaterialRentVO> materialRentVOList;


    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBookTime() {
        return bookTime;
    }

    public void setBookTime(String bookTime) {
        this.bookTime = bookTime;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getBookState() {
        return bookState;
    }

    public void setBookState(String bookState) {
        this.bookState = bookState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public GameInfoVO getGameInfoVO() {
        return gameInfoVO;
    }

    public void setGameInfoVO(GameInfoVO gameInfoVO) {
        this.gameInfoVO = gameInfoVO;
    }

    public RefereeInfoVO getRefereeInfoVO() {
        return refereeInfoVO;
    }

    public void setRefereeInfoVO(RefereeInfoVO refereeInfoVO) {
        this.refereeInfoVO = refereeInfoVO;
    }

    public GroundInfoVO getGroundInfoVO() {
        return groundInfoVO;
    }

    public void setGroundInfoVO(GroundInfoVO groundInfoVO) {
        this.groundInfoVO = groundInfoVO;
    }

    public MaterialInfoVO getMaterialInfoVO() {
        return materialInfoVO;
    }

    public void setMaterialInfoVO(MaterialInfoVO materialInfoVO) {
        this.materialInfoVO = materialInfoVO;
    }

    public List<RefereeRentVO> getRefereeRentVOList() {
        return refereeRentVOList;
    }

    public void setRefereeRentVOList(List<RefereeRentVO> refereeRentVOList) {
        this.refereeRentVOList = refereeRentVOList;
    }

    public List<GroundRentVO> getGroundRentVOList() {
        return groundRentVOList;
    }

    public void setGroundRentVOList(List<GroundRentVO> groundRentVOList) {
        this.groundRentVOList = groundRentVOList;
    }

    public List<MaterialRentVO> getMaterialRentVOList() {
        return materialRentVOList;
    }

    public void setMaterialRentVOList(List<MaterialRentVO> materialRentVOList) {
        this.materialRentVOList = materialRentVOList;
    }

    @Override
    public String toString() {
        return "BookInfoVO{" +
                "bookId='" + bookId + '\'' +
                ", userId='" + userId + '\'' +
                ", bookTime='" + bookTime + '\'' +
                ", auditState='" + auditState + '\'' +
                ", bookState='" + bookState + '\'' +
                ", remark='" + remark + '\'' +
                ", gameInfoVO=" + gameInfoVO +
                ", refereeInfoVO=" + refereeInfoVO +
                ", groundInfoVO=" + groundInfoVO +
                ", materialInfoVO=" + materialInfoVO +
                ", refereeRentVOList=" + refereeRentVOList +
                ", groundRentVOList=" + groundRentVOList +
                ", materialRentVOList=" + materialRentVOList +
                '}';
    }
}
