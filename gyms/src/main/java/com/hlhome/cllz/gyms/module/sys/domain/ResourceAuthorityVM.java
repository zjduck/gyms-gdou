package com.hlhome.cllz.gyms.module.sys.domain;

import java.util.List;


public class ResourceAuthorityVM {

	List<ResourceAuthority>resourceAuthorityList;

	public List<ResourceAuthority> getResourceAuthorityList() {
		return resourceAuthorityList;
	}

	public void setResourceAuthorityList(
			List<ResourceAuthority> resourceAuthorityList) {
		this.resourceAuthorityList = resourceAuthorityList;
	}
	
	
}
