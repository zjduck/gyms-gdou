package com.hlhome.cllz.gyms.module.game.mapper;

import com.hlhome.cllz.gyms.module.game.domain.GameTypeDO;

import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
public interface GameTypeMapper {

    int deleteByTypeId(String typeId);

    int insert(GameTypeDO gameTypeDO);

    GameTypeDO getByTypeId(String typeId);

    GameTypeDO getByTypeName(String typeName);

    List<GameTypeDO> listAll();

    int update(GameTypeDO gameInfoDO);

}
