package com.hlhome.cllz.gyms.module.ground.mapper;

import com.hlhome.cllz.gyms.module.ground.domain.GroundRentDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 场地租借DAO类
 *
 * Created by luckyTsai on 2018/6/23
 */
@Mapper
public interface GroundRentMapper {

    /**
     * 通过预约id删除场地租借信息
     */
    int deleteByBookId(String bookId);

    /**
     * 新增场地租借信息
     */
    int insert(GroundRentDO groundRentDO);

    /**
     * 通过场地id获取场地租借信息
     */
    GroundRentDO getByGroundId(String groundId);

    /**
     * 通过预约id获取场地租借信息
     */
    List<GroundRentDO> listByBookId(String bookId);

    /**
     * 统计所有场地租借信息
     */
    List<GroundRentDO> listAll();

    /**
     * 更新场地租借信息
     */
    int update(GroundRentDO groundRentDO);

    /**
     * 通过场地id，获取最新的场地租借
     */
    GroundRentDO getLatestByGroundId(String groundId);

    /**
     * 通过场地编号和日期，获取场地租借列表
     */
    List<GroundRentDO> listByGoundIdAndDate(String groundId, String date);

}
