package com.hlhome.cllz.gyms.module.order.domain;

import java.time.LocalDateTime;

/**
 * Created by lingb on 2018/6/25
 */
public class OrderInfoVO {

    private String orderId;
    private String bookId;
    private String orderAmount;
    private String orderState;
    private LocalDateTime paymentTime;
    private String remark;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public LocalDateTime getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(LocalDateTime paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "OrderInfoVO{" +
                "orderId='" + orderId + '\'' +
                ", bookId='" + bookId + '\'' +
                ", orderAmount='" + orderAmount + '\'' +
                ", orderState='" + orderState + '\'' +
                ", paymentTime=" + paymentTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}
