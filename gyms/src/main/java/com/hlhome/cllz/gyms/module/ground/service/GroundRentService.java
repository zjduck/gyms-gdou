package com.hlhome.cllz.gyms.module.ground.service;

import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoRentTypeVO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundRentDO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundRentVO;

import java.util.List;

/**
 * Created by luckyTsai on 2018/6/23
 */
public interface GroundRentService {

    /**
     * 统计所有场地租借信息
     */
    List<GroundRentVO> listAll();

    /**
     * 通过场地id获取场地租借信息
     */
    GroundRentVO getByGroundId(String groundId);

    /**
     * 通过预约id获取场地租借信息
     */
    List<GroundRentVO> listByBookId(String bookId);

    /**
     * 审核场地租借信息
     */
    boolean auditGroundRent(GroundRentVO groundRentVO);

    /**
     * 添加场地租借信息
     */
    boolean saveGroundRent(GroundRentVO groundRentVO);

    /**
     * 更新场地租借信息
     */
    boolean updateGroundRent(GroundRentVO groundRentVO);

    /**
     * 通过预约编号删除场地租借信息
     */
    boolean removeByBookId(String bookId);

    /**
     * 通过场地id，获取最新的场地租借
     */
    GroundRentDO getLatestByGroundId(String groundId);

    /**
     * 通过场地编号 和 日期，获取场地租借列表
     */
    List<GroundRentVO> listByGoundIdAndDate(String groundId, String date);
}
