package com.hlhome.cllz.gyms.module.ground.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.ground.domain.GroundRentVO;
import com.hlhome.cllz.gyms.module.ground.service.GroundRentService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by luckyTsai on 2018/6/24
 */
@RequestMapping("/api/v1")
@RestController
public class GroundRentController {
    private static final Logger logger = LoggerFactory.getLogger(GroundRentController.class);

    @Autowired
    private GroundRentService groundRentService;

    /**
     * 新增场地租借
     * 需要 ground_saveGroundRent权限或管理员权限
     *
     * @param   [GroundRentVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_saveGroundRent", "admin"}, logical = Logical.OR)
    @PostMapping("/ground/rent")
    public WebResult saveGroundRent(@RequestBody GroundRentVO groundRentVO) {
        boolean flag = groundRentService.saveGroundRent(groundRentVO);
        if (flag) {
            return WebResult.success(flag,"租借成功！");

        } else {
            return WebResult.serverError("租借失败！");
        }
    }

    /**
     * 审核场地租借
     * 需要 ground_auditGroundRent权限或管理员权限
     *
     * @param   [GroundRentVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_auditGroundRent", "admin"}, logical = Logical.OR)
    @PatchMapping("/ground/rent/audit")
    public WebResult auditGroundRent(@RequestBody GroundRentVO groundRentVO) {
        boolean flag = groundRentService.auditGroundRent(groundRentVO);
        if (flag) {
            return WebResult.success(flag,"审核成功！");

        } else {
            return WebResult.serverError("审核失败！");
        }
    }

    /**
     * 修改场地租借
     * 需要 ground_updateGroundRent权限或管理员权限
     *
     * @param   [GroundRentVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_updateGroundRent", "admin"}, logical = Logical.OR)
    @PatchMapping("/ground/rent/update")
    public WebResult updateGroundRent(@RequestBody GroundRentVO groundRentVO) {
        boolean flag = groundRentService.updateGroundRent(groundRentVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }

    /**
     * 删除场地租借
     * 需要 ground_deleteGroundRent权限或管理员权限
     *
     * @param   [bookId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_deleteGroundRent", "admin"}, logical = Logical.OR)
    @DeleteMapping("/ground/rent/{bookId}")
    public WebResult deleteGroundRent(@PathVariable("bookId") String bookId) {
        if(bookId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = groundRentService.removeByBookId(bookId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }

    /**
     * 获取单个场地租借
     * 需要 ground_getGroundRentDetail权限或管理员权限
     *
     * @param   [groundId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_getGroundRentDetail", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/rent/{groundId}")
    public WebResult getGroundRentDetail(@PathVariable String groundId) {
        GroundRentVO groundRentVO = groundRentService.getByGroundId(groundId);
        if (groundRentVO != null) {
            return WebResult.success(groundRentVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    /**
     * 获取所有场地租借
     * 需要 ground_listAllGroundRent权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_listAllGroundRent", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/rent")
    public WebResult listAllGroundRent() {
        List<GroundRentVO> groundRentVOList = groundRentService.listAll();
        if (groundRentService != null) {
            return WebResult.success(groundRentService, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }

    /**
     *
     * 通过场地编号 和 日期，获取场地租借列表
     * 需要 ground_listGroundRentDate权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_listGroundRentDate", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/rent/date")
    public WebResult listGroundRentDate(@RequestParam("groundId") String groundId,
                                        @RequestParam("date") String date) {
        List<GroundRentVO> groundInfoVOList = groundRentService.listByGoundIdAndDate(groundId, date);
        if (groundRentService != null) {
            return WebResult.success(groundRentService, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }

}
