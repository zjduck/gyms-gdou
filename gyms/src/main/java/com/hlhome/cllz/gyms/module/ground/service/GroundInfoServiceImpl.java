package com.hlhome.cllz.gyms.module.ground.service;

import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoDO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoRentTypeVO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoVO;
import com.hlhome.cllz.gyms.module.ground.mapper.GroundInfoMapper;
import com.hlhome.cllz.gyms.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luckyTsai on 2018/6/24
 */
@Service
@Transactional
public class GroundInfoServiceImpl implements GroundInfoService {

    private static final Logger logger = LoggerFactory.getLogger(GroundInfoServiceImpl.class);

    @Autowired
    private GroundInfoMapper groundInfoMapper;

    /**
     * 统计所有空闲的场地信息
     */
    @Override
    public List<GroundInfoVO> listAll() {
        List<GroundInfoVO> groundInfoVOList = new ArrayList<>();
        List<GroundInfoDO> groundInfoDOList = groundInfoMapper.listAll();
        for(GroundInfoDO groundInfoDO : groundInfoDOList){
            GroundInfoVO groundInfoVO = POJOUtils.DO2VO(groundInfoDO, GroundInfoVO.class);
            groundInfoVOList.add(groundInfoVO);
        }
        return groundInfoVOList;
    }

    /**
     * 统计所有使用中的场地信息
     */
    @Override
    public List<GroundInfoVO> listUse() {
        List<GroundInfoVO> groundInfoVOList = new ArrayList<>();
        List<GroundInfoDO> groundInfoDOList = groundInfoMapper.listUse();
        for(GroundInfoDO groundInfoDO : groundInfoDOList){
            GroundInfoVO groundInfoVO = POJOUtils.DO2VO(groundInfoDO, GroundInfoVO.class);
            groundInfoVOList.add(groundInfoVO);
        }
        return groundInfoVOList;
    }

    /**
     * 通过场地id获取场地信息
     *
     * @param groundId
     */
    @Override
    public GroundInfoVO getByGroundId(String groundId) {
        GroundInfoDO groundInfoDO = groundInfoMapper.getByGroundId(groundId);
        return POJOUtils.DO2VO(groundInfoDO, GroundInfoVO.class);
    }

    /**
     * 通过场地名称获取场地信息
     *
     * @param groundName
     */
    @Override
    public GroundInfoVO getByGroundName(String groundName) {
        GroundInfoDO groundInfoDO = groundInfoMapper.getByGroundId(groundName);
        return POJOUtils.DO2VO(groundInfoDO, GroundInfoVO.class);
    }

    /**
     * 添加场地
     *
     * @param groundInfoVO
     */
    @Override
    public synchronized boolean saveGround(GroundInfoVO groundInfoVO) {
        boolean flag = false;
        Asserts.validate(groundInfoVO,"groundInfoVO");
        GroundInfoDO groundInfoDO = new GroundInfoDO();
        groundInfoDO = POJOUtils.VO2DO(groundInfoVO, GroundInfoDO.class);
        groundInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        groundInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        GroundInfoDO latestGroundInfo = getLatestByGroundType(groundInfoVO.getGroundType());

        // 若为空，该场地类型第一个
        if(latestGroundInfo == null) {
            String groundInfoNum = SystemUtils.groundTypeBind(groundInfoVO.getGroundType());
            groundInfoDO.setGroundId(Constants.GROUND + groundInfoNum + "001");

        }// 否则，该场地信息类型递增
        else {
            String groundTypeId = SystemUtils.groundTypeBind(groundInfoVO.getGroundType());
            String groundId = KeyGenerator.getInstance().generateNum(latestGroundInfo.getGroundId(), groundTypeId);
            groundInfoDO.setGroundId(groundId);
        }

        int result = groundInfoMapper.insert(groundInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 更新场地
     *
     * @param groundInfoVO
     */
    @Override
    public boolean updateGround(GroundInfoVO groundInfoVO) {
        boolean flag = false;
        GroundInfoDO groundInfoDO = POJOUtils.VO2DO(groundInfoVO, GroundInfoDO.class);
        groundInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));

        int result = groundInfoMapper.update(groundInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 删除场地
     *
     * @param groundId
     */
    @Override
    public boolean removeByGroundId(String groundId) {
        boolean flag = false;
        int result = groundInfoMapper.deleteByGroundId(groundId);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 通过场地类型，获取最新的场地信息
     *
     * @param groundType
     */
    @Override
    public GroundInfoDO getLatestByGroundType(String groundType) {
        return null;
    }

    /**
     * 统计所有场地信息（基本信息、类型、租借）
     *
     * @return
     */
    @Override
    public List<GroundInfoRentTypeVO> listAllInfo() {
        return groundInfoMapper.listAllInfo();
    }
}
