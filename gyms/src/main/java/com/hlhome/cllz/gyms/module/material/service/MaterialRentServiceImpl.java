package com.hlhome.cllz.gyms.module.material.service;


import com.hlhome.cllz.gyms.module.book.domain.BookInfoDO;
import com.hlhome.cllz.gyms.module.book.mapper.BookInfoMapper;
import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoDO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialRentDO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialRentVO;
import com.hlhome.cllz.gyms.module.material.mapper.MaterialInfoMapper;
import com.hlhome.cllz.gyms.module.material.mapper.MaterialRentMapper;
import com.hlhome.cllz.gyms.module.user.domain.User;
import com.hlhome.cllz.gyms.util.*;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 器材租借业务层
 *
 * created by zmz on 2018/6/23
 */
@Service
@Transactional
public class MaterialRentServiceImpl  implements MaterialRentService{


    @Autowired
    private MaterialRentMapper materialRentMapper;

    @Autowired
    private MaterialInfoMapper materialInfoMapper;

    @Autowired
    private BookInfoMapper bookInfoMapper;

    @Override
    public synchronized List<MaterialRentVO> listAll() {
        List<MaterialRentVO> materialRentVOList = new ArrayList<>();
        List<MaterialRentDO> materialRentDOList = materialRentMapper.listAll();
        for(MaterialRentDO materialRentDO : materialRentDOList){
            MaterialRentVO materialRentVO = POJOUtils.DO2VO(materialRentDO,MaterialRentVO.class);
            materialRentVOList.add(materialRentVO);
        }
        return materialRentVOList;
    }

    @Override
    public synchronized boolean saveMaterialRent(MaterialRentVO materialRentVO) {
        boolean flag =false;

        Asserts.validate(materialRentVO,"materialRentVO");

        MaterialRentDO materialRentDO = new MaterialRentDO();
        materialRentDO = POJOUtils.VO2DO(materialRentVO,MaterialRentDO.class);
        materialRentDO.setGmtCreate(LocalDateTime.now().withNano(0));
        materialRentDO.setGmtModified(LocalDateTime.now().withNano(0));
        materialRentDO.setAuditState("租借等待");

//        // 1、获取用户id
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        String userId = user.getId();
        // 获取最新一条预约记录
        BookInfoDO latestBook = bookInfoMapper.getLatestOrderById();
        String bookId = "";
        // 若为空，数据库首条预约记录
        if (latestBook == null) {
            bookId = Constants.MATERIAL + DateUtils.getNowDate() + "001";

            // 否则，预约记录递增（自动按日期走）
        } else {
            bookId = KeyGenerator.getInstance().generateNum(latestBook.getBookId());

        }

        BookInfoDO bookInfoDO = new BookInfoDO();
        bookInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        bookInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        bookInfoDO.setBookId(bookId);
//        bookInfoDO.setUserId(userId);
        bookInfoDO.setBookTime(materialRentDO.getRentTime());
        bookInfoMapper.insert(bookInfoDO);

        materialRentDO.setBookId(bookId);


        MaterialInfoDO materialInfoDO = materialInfoMapper.getByMaterialId(materialRentVO.getMaterialId());
        int residue = materialInfoDO.getMaterialResidue();
        int rentNumber = materialRentVO.getRentNumber();

        int result;

        if (rentNumber <= residue){

            int materialResidue = residue - rentNumber ;
            materialInfoDO.setMaterialResidue(materialResidue);
            materialInfoMapper.update( materialInfoDO);

            result = materialRentMapper.insert(materialRentDO);

        }

        else {

            result = 0;

        }



        flag = result == 0 ? false : true ;
        return flag;
    }

    @Override
    public synchronized boolean removeByBookIdAndMaterialId(MaterialRentVO materialRentVO) {
        boolean flag = false;
        MaterialRentDO materialRentDO = POJOUtils.VO2DO(materialRentVO, MaterialRentDO.class);
        MaterialInfoDO materialInfoDO = materialInfoMapper.getByMaterialId(materialRentDO.getMaterialId());

        int rentNumber = materialRentDO.getRentNumber();
        int residue = materialInfoDO.getMaterialResidue();
        int materialResidue = residue + rentNumber ;
        materialInfoDO.setMaterialResidue(materialResidue);
        materialInfoMapper.update(materialInfoDO);

        int result = materialRentMapper.deleteByBookIdAndMaterialId(materialRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public synchronized boolean updateMaterialRent(MaterialRentVO materialRentVO) {
        boolean flag = false;

        MaterialInfoDO materialInfoDO = materialInfoMapper.getByMaterialId(materialRentVO.getMaterialId());
        int num = materialInfoDO.getMaterialNumber();
        int residue = materialInfoDO.getMaterialResidue();
        int rent = materialRentVO.getRentNumber();
        int materialResidue = num - rent ;

        MaterialRentDO materialRentDO = POJOUtils.VO2DO(materialRentVO, MaterialRentDO.class);
        materialRentDO.setGmtModified(LocalDateTime.now().withNano(0));

        String audit = materialRentDO.getAuditState();

        if(audit == "归还审核成功"){
            materialResidue = residue + rent ;
        }

        materialInfoDO.setMaterialResidue(materialResidue);
        materialInfoMapper.update(materialInfoDO);



        int result = materialRentMapper.update(materialRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public List<MaterialRentVO> listByMaterialId(String materialId) {
        List<MaterialRentVO> materialRentVOList = new ArrayList<>();
        List<MaterialRentDO> materialRentDOList = materialRentMapper.listByMaterialId(materialId);
        for(MaterialRentDO materialRentDO : materialRentDOList){
            MaterialRentVO materialRentVO = POJOUtils.DO2VO(materialRentDO,MaterialRentVO.class);
            materialRentVOList.add(materialRentVO);
        }
        return materialRentVOList;
    }

    @Override
    public List<MaterialRentVO> listByBookId(String bookId) {
        List<MaterialRentVO> materialRentVOList = new ArrayList<>();
        List<MaterialRentDO> materialRentDOList = materialRentMapper.listByBookId(bookId);
        for(MaterialRentDO materialRentDO : materialRentDOList){
            MaterialRentVO materialRentVO = POJOUtils.DO2VO(materialRentDO,MaterialRentVO.class);
            materialRentVOList.add(materialRentVO);
        }
        return materialRentVOList;
    }

    @Override
    public MaterialRentVO getByBookIdAndMaterialId ( String bookId , String materialId){
        MaterialRentDO materialRentDO = materialRentMapper.getByMaterialIdAndBookId(materialId,bookId);
        MaterialRentVO materialRentVO = POJOUtils.DO2VO(materialRentDO,MaterialRentVO.class);
        return materialRentVO;
    }

//    @Override
//    public boolean auditMaterialRent (MaterialRentVO materialRentVO){
//        boolean flag = false;
//        MaterialRentDO materialRentDO = POJOUtils.VO2DO(materialRentVO, MaterialRentDO.class);
//        materialRentDO.setGmtModified(LocalDateTime.now().withNano(0));
//
//        int result = materialRentMapper.update(materialRentDO);
//        flag = result == 0 ? false : true;
//        return flag;
//    }
}
