package com.hlhome.cllz.gyms.module.referee.service;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeItemVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeItemDO;
import com.hlhome.cllz.gyms.module.referee.mapper.RefereeItemMapper;
import com.hlhome.cllz.gyms.util.*;
import com.hlhome.cllz.gyms.util.Asserts;
import com.hlhome.cllz.gyms.util.POJOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
@Service
@Transactional
public class RefereeItemServiceImpl implements RefereeItemService{

    private static final Logger logger = LoggerFactory.getLogger(RefereeItemServiceImpl.class);

    @Autowired
    private RefereeItemMapper refereeItemMapper;

    /**
     * 添加裁判项目
     *
     * @param refereeItemVO signification
     * @return boolean
     */
    @Override
    public synchronized boolean addItem(RefereeItemVO refereeItemVO) {
        boolean flag = false;
        Asserts.validate(refereeItemVO,"refereeItemVO");

        RefereeItemDO refereeItemDO = new RefereeItemDO();
        // VO -> DO
        refereeItemDO = POJOUtils.VO2DO(refereeItemVO, RefereeItemDO.class);

        refereeItemDO.setGmtCreate(LocalDateTime.now().withNano(0));
        refereeItemDO.setGmtModified(LocalDateTime.now().withNano(0));
        RefereeItemDO latestItem = getLatestByItemName(refereeItemVO.getItemName());
        // 若为空，该裁判项目Id为第一个
        if (latestItem == null) {
            String refereeItemNum = SystemUtils.refereeTypeBind(refereeItemVO.getItemName());
            refereeItemDO.setItemId(Constants.REFEREE + refereeItemNum + "001");

            // 否则，该裁判项目Id递增
        } else {
            String itemName = SystemUtils.refereeTypeBind(refereeItemVO.getItemName());
            String itemId = KeyGenerator.getInstance().generateNum(latestItem.getItemId(), itemName);
            refereeItemDO.setItemId(itemId);
        }

        int result = refereeItemMapper.insert(refereeItemDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 删除裁判项目
     *
     * @param itemId signification
     * @return boolean
     */
    @Override
    public boolean removeItem(String itemId) {
        boolean flag = false;
        int result = refereeItemMapper.deleteByItemId(itemId);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 更新裁判项目
     *
     * @param refereeItemVO signification
     * @return boolean
     */
    @Override
    public boolean updateItem(RefereeItemVO refereeItemVO) {
        boolean flag = false;
        RefereeItemDO refereeItemDO = POJOUtils.VO2DO(refereeItemVO, RefereeItemDO.class);
        refereeItemDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = refereeItemMapper.update(refereeItemDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 通过项目Id查找裁判项目
     *
     * @param itemId signification
     * @return RefereeItemVO
     */
    @Override
    public RefereeItemVO getByItemId(String itemId) {
        RefereeItemDO refereeItemDO = refereeItemMapper.getByItemId(itemId);
        return POJOUtils.DO2VO(refereeItemDO, RefereeItemVO.class);
    }

    /**
     * 通过项目Name查找项目信息
     *
     * @param itemName signification
     * @return RefereeItemVO
     */
    @Override
    public RefereeItemVO getByItemName(String itemName) {
        RefereeItemDO refereeItemDO = refereeItemMapper.getByItemId(itemName);
        return POJOUtils.DO2VO(refereeItemDO, RefereeItemVO.class);
    }

    /**
     * 统计所有裁判项目
     *
     * @return RefereeItemVO
     */
    @Override
    public List<RefereeItemVO> listAll() {
        List<RefereeItemVO> refereeItemVOList = new ArrayList<>();
        List<RefereeItemDO> refereeItemDOList = refereeItemMapper.listAll();
        for(RefereeItemDO refereeItemDO : refereeItemDOList){
            RefereeItemVO refereeItemVO = POJOUtils.DO2VO(refereeItemDO, RefereeItemVO.class);
            refereeItemVOList.add(refereeItemVO);
        }
        return refereeItemVOList;
    }

    @Override
    public RefereeItemDO getLatestByItemName(String itemName) {
        return refereeItemMapper.getLatestByItemName(itemName);
    }
}