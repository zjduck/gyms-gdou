package com.hlhome.cllz.gyms.module.referee.mapper;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeItemDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
@Mapper
public interface RefereeItemMapper {

    int insert(RefereeItemDO refereeItemDO);

    int deleteByItemId(String itemId);

    int update(RefereeItemDO refereeItemDO);

    RefereeItemDO getByItemId(String itemId);

    RefereeItemDO getByItemName(String itemName);

    List<RefereeItemDO> listAll();

    RefereeItemDO getLatestByItemName(String itemName);
}
