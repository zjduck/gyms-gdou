package com.hlhome.cllz.gyms.module.ground.mapper;

import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoDO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoRentTypeVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 场地信息DAO类
 *
 * Created by luckyTsai on 2018/6/23
 */
@Mapper
public interface GroundInfoMapper {

    /**
     * 通过场地id删除场地信息
     */
    int deleteByGroundId(String groundId);

    /**
     * 新增场地信息
     */
    int insert(GroundInfoDO groundInfoDO);

    /**
     * 通过场地id获取场地信息
     */
    GroundInfoDO getByGroundId(String groundId);

    /**
     * 通过场地名称获取场地信息
     */
    GroundInfoDO getByGroundName(String groundName);

    /**
     * 统计所有空闲的场地信息
     */
    List<GroundInfoDO> listAll();

    /**
     * 统计所有使用中的场地
     */
    List<GroundInfoDO> listUse();

    /**
     * 更新场地信息
     */
    int update(GroundInfoDO groundInfoDO);

    /**
     * 通过场地类型，获取最新的场地记录
     */
    GroundInfoDO getLatestByGroundType(String groundType);

    /**
     * 统计所有场地信息（基本信息、类型、租借）
     * @return
     */
    List<GroundInfoRentTypeVO> listAllInfo();
}
