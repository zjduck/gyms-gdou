package com.hlhome.cllz.gyms.module.ground.service;

import com.hlhome.cllz.gyms.module.ground.domain.GroundRentDO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundRentVO;
import com.hlhome.cllz.gyms.module.ground.mapper.GroundRentMapper;
import com.hlhome.cllz.gyms.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luckyTsai on 2018/6/23
 */
@Service
@Transactional
public class GroundRentServiceImpl implements GroundRentService {

    private static final Logger logger = LoggerFactory.getLogger(GroundRentServiceImpl.class);

    @Autowired
    private GroundRentMapper groundRentMapper;

    /**
     * 统计所有场地租借信息
     */
    @Override
    public List<GroundRentVO> listAll() {
        List<GroundRentVO> groundRentVOList = new ArrayList<>();
        List<GroundRentDO> groundRentDOList = groundRentMapper.listAll();
        for(GroundRentDO groundRentDO : groundRentDOList){
            GroundRentVO groundRentVO = POJOUtils.DO2VO(groundRentDO, GroundRentVO.class);
            groundRentVOList.add(groundRentVO);
        }
        return groundRentVOList;
    }

    /**
     * 通过场地id获取场地租借信息
     *
     * @param groundId
     */
    @Override
    public GroundRentVO getByGroundId(String groundId) {
        GroundRentDO groundRentDO = groundRentMapper.getByGroundId(groundId);
        return POJOUtils.DO2VO(groundRentDO, GroundRentVO.class);
    }

    /**
     * 通过预约id获取场地租借信息
     *
     * @param bookId
     */
    @Override
    public List<GroundRentVO> listByBookId(String bookId) {
        List<GroundRentDO> groundRentDOList = groundRentMapper.listByBookId(bookId);
        return POJOUtils.DO2VOList(groundRentDOList, GroundRentVO.class);
    }

    /**
     * 审核场地租借信息
     */
    @Override
    public boolean auditGroundRent(GroundRentVO groundRentVO) {
        boolean flag = false;
        GroundRentDO groundRentDO = POJOUtils.VO2DO(groundRentVO, GroundRentDO.class);
        groundRentDO.setGmtCreate(LocalDateTime.now().withNano(0));
        groundRentDO.setGmtModified(LocalDateTime.now().withNano(0));

       int result = groundRentMapper.update(groundRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }


    /**
     * 添加场地租借信息
     *
     * @param groundRentVO
     */
    @Override
    public synchronized boolean saveGroundRent(GroundRentVO groundRentVO) {
        boolean flag = false;
        Asserts.validate(groundRentVO,"groundRentVO");
        GroundRentDO groundRentDO = new GroundRentDO();
        groundRentDO = POJOUtils.VO2DO(groundRentVO, GroundRentDO.class);
        groundRentDO.setGmtCreate(LocalDateTime.now().withNano(0));
        groundRentDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = groundRentMapper.insert(groundRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 更新场地租借信息
     *
     * @param groundRentVO
     */
    @Override
    public boolean updateGroundRent(GroundRentVO groundRentVO) {
        boolean flag = false;
        GroundRentDO groundRentDO = POJOUtils.VO2DO(groundRentVO, GroundRentDO.class);
        groundRentDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = groundRentMapper.update(groundRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 通过预约编号删除场地租借信息
     *
     * @param bookId
     */
    @Override
    public boolean removeByBookId(String bookId) {
        boolean flag = false;
        int result = groundRentMapper.deleteByBookId(bookId);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public GroundRentDO getLatestByGroundId(String groundId) {
        return null;
    }

    /**
     * 通过场地编号和日期，获取场地租借列表
     *
     * @param groundId
     * @param date
     */
    @Override
    public List<GroundRentVO> listByGoundIdAndDate(String groundId, String date) {
        List<GroundRentVO> groundRentVOList = new ArrayList<>();
        List<GroundRentDO> groundRentDOList = groundRentMapper.listByGoundIdAndDate(groundId, date);
        for(GroundRentDO groundRentDO : groundRentDOList){
            if ("通过".equals(groundRentDO.getAuditState())) {
                GroundRentVO groundRentVO = POJOUtils.DO2VO(groundRentDO, GroundRentVO.class);
                groundRentVOList.add(groundRentVO);
            }
        }
        return groundRentVOList;
    }
}
