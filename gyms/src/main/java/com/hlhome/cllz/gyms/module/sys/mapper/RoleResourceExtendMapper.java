package com.hlhome.cllz.gyms.module.sys.mapper;

import com.hlhome.cllz.gyms.module.sys.domain.RoleResourcer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleResourceExtendMapper {

	/**
	 * 根据roleid 或resourceid 删除角色资源关系
	 */
	public void deleteRoleResource(
            @Param(value = "roleId") String roleId,
            @Param(value = "resourceId") String resourceId);

	/**
	 * 批量保存 角色资源关系
	 */
	public void batchInsert(@Param(value = "list") List<RoleResourcer> list);

	/**
	 * 通过roleid得到拥有的resourceid
	 */
	public List<String> getResourceByRole(@Param(value = "roleIds") List<String> roleIds);
}
