package com.hlhome.cllz.gyms.module.game.domain;

import java.math.BigDecimal;

/**
 * Created by lingb on 2018/6/25
 */
public class GameTypeVO {

    private String typeId;
    private String typeName;
    private BigDecimal chargeStandard;
    private String remark;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public BigDecimal getChargeStandard() {
        return chargeStandard;
    }

    public void setChargeStandard(BigDecimal chargeStandard) {
        this.chargeStandard = chargeStandard;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "GameTypeVO{" +
                "typeId='" + typeId + '\'' +
                ", typeName='" + typeName + '\'' +
                ", chargeStandard=" + chargeStandard +
                ", remark='" + remark + '\'' +
                '}';
    }
}

