package com.hlhome.cllz.gyms.module.referee.service;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeItemDO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeItemVO;

import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
public interface RefereeItemService {

    /**
     * 添加裁判项目
     *
     * @param refereeItemVO signification
     * @return boolean
     */
    boolean addItem(RefereeItemVO refereeItemVO);

    /**
     * 删除裁判项目
     *
     * @param itemId signification
     * @return boolean
     */
    boolean removeItem(String itemId);

    /**
     * 更新裁判项目
     *
     * @param refereeItemVO signification
     * @return boolean
     */
    boolean updateItem(RefereeItemVO refereeItemVO);

    /**
     * 通过裁判项目Id查找裁判项目
     *
     * @param itemId signification
     * @return RefereeInfoVO
     */
    RefereeItemVO getByItemId(String itemId);

    /**
     * 通过裁判Name查找裁判项目
     *
     * @param itemName signification
     * @return RefereeInfoVO
     */
    RefereeItemVO getByItemName(String itemName);

    /**
     * 统计所有裁判项目
     *
     * @param
     * @return RefereeItemVO
     */
    List<RefereeItemVO> listAll();

    /**
     * 通过项目类型查找最新裁判项目记录
     *
     * @param itemName
     * @return RefereeInfoVO
     */
    RefereeItemDO getLatestByItemName(String itemName);
}
