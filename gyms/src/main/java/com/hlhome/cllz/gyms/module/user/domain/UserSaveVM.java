package com.hlhome.cllz.gyms.module.user.domain;

import org.hibernate.validator.constraints.NotBlank;

public class UserSaveVM {
	
	@NotBlank
    private String loginName;

	@NotBlank
    private String realName;

	@NotBlank
    private String password;

	
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
    
}
