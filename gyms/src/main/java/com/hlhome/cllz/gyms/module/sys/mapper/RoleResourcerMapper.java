package com.hlhome.cllz.gyms.module.sys.mapper;

import com.hlhome.cllz.gyms.module.sys.domain.RoleResourcer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleResourcerMapper {
    int deleteByPrimaryKey(String id);

    int insert(RoleResourcer record);

    RoleResourcer selectByPrimaryKey(String id);

    List<RoleResourcer> selectAll();

    int updateByPrimaryKey(RoleResourcer record);
}