package com.hlhome.cllz.gyms.module.material.mapper;

import com.hlhome.cllz.gyms.module.material.domain.MaterialTypeDO;

import java.util.List;

/**
 * 器材类型Mapper(DAO)类
 *
 * Created by zmz on 2018/6/23
 */

public interface MaterialTypeMapper {

    /**
     *新增器材类型信息
     */
    int insert (MaterialTypeDO materialTypeDO);

    /**
     *通过类型id，删除器材类型信息
     */
    int deleteByTypeId (String typeId);

    /**
     *更新器材类型信息
     */
    int update (MaterialTypeDO materialTypeDO);

    /**
     *遍历器材类型信息
     */
    List<MaterialTypeDO> listAll();

    /**
     *通过类型id，查询器材类型信息
     */
    MaterialTypeDO getByTypeId (String typeId);

    /**
     *通过类型名字，查询器材类型信息
     */
    MaterialTypeDO getByTypeName (String typeName);

    /**
     * 通过器材类型名称 获取最新的器材类型记录
     */
    MaterialTypeDO getLatestByTypeName(String typeName);

}
