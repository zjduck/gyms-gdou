package com.hlhome.cllz.gyms.bean;

import org.springframework.http.HttpStatus;

/**
 * web层的返回结果类
 */
public class WebResult {

    private Boolean success;
    private String message;
    private Object data;
    // HTTP状态码
    private Integer status;


    /**
     * 其他情况
     * @param message
     * @return 结果
     */
    public static WebResult echo(String message, Integer status) {
        WebResult result = new WebResult();
        result.setSuccess(false);
        result.setMessage(message);
        result.setStatus(status);
        return result;
    }

    /**
     * 请求成功
     * @param data
     * @return
     */
    public static WebResult success(Object data, String message) {
        WebResult result = new WebResult();
        result.setSuccess(true);
        result.setMessage(message);
        result.setData(data);
        result.setStatus(HttpStatus.OK.value());
        return result;
    }

    /**
     * 请求成功
     * @param data
     * @return
     */
    public static WebResult success(Object data) {
        WebResult result = new WebResult();
        result.setSuccess(true);
        result.setData(data);
        result.setStatus(HttpStatus.OK.value());
        return result;
    }

    /**
     * 校验错误
     * @param message
     * @return 结果
     */
    public static WebResult validateError(String message) {
        WebResult result = new WebResult();
        result.setSuccess(false);
        result.setMessage(message);
        result.setStatus(HttpStatus.BAD_REQUEST.value());
        return result;
    }

    /**
     * 服务器错误
     * @param message
     * @return 结果
     */
    public static WebResult serverError(String message) {
        WebResult result = new WebResult();
        result.setSuccess(false);
        result.setMessage(message);
        result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return result;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WebResult{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", status=" + status +
                '}';
    }
}
