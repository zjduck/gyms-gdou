package com.hlhome.cllz.gyms.module.user.domain;

import org.hibernate.validator.constraints.NotBlank;

public class UserRoleSaveVMS {

	@NotBlank
	private String userId;
	@NotBlank
	private String roleIds;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
	
	
	
}
