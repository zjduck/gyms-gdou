package com.hlhome.cllz.gyms.module.referee.service;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoDO;
import com.hlhome.cllz.gyms.module.referee.mapper.RefereeInfoMapper;
import com.hlhome.cllz.gyms.util.*;
import com.hlhome.cllz.gyms.util.Asserts;
import com.hlhome.cllz.gyms.util.POJOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
@Service
@Transactional
public class RefereeInfoServiceImpl implements RefereeInfoService {

    private static final Logger logger = LoggerFactory.getLogger(RefereeInfoServiceImpl.class);

    @Autowired
    private RefereeInfoMapper refereeInfoMapper;

    /**
     * 添加裁判信息
     *
     * @param refereeInfoVO signification
     * @return boolean
     */
    @Override
    public synchronized boolean addReferee(RefereeInfoVO refereeInfoVO) {
        boolean flag = false;
        Asserts.validate(refereeInfoVO,"refereeInfoVO");

        RefereeInfoDO refereeInfoDO = new RefereeInfoDO();
        // VO -> DO
        refereeInfoDO = POJOUtils.VO2DO(refereeInfoVO, RefereeInfoDO.class);

        refereeInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        refereeInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        RefereeInfoDO latestInfo = getLatestByRefereeItem(refereeInfoDO.getRefereeItem());
        // 若为空，该裁判信息Id为第一个
        if (latestInfo == null) {
            String refereeInfoNum = SystemUtils.refereeTypeBind(refereeInfoVO.getRefereeItem());
            refereeInfoDO.setRefereeId(Constants.REFEREE + refereeInfoNum + "001");

            // 否则，该裁判信息Id递增
        } else {
            String refereeItem = SystemUtils.refereeTypeBind(refereeInfoVO.getRefereeItem());
            String refereeId = KeyGenerator.getInstance().generateNum(latestInfo.getRefereeId(), refereeItem);
            refereeInfoDO.setRefereeId(refereeId);
        }

        int result = refereeInfoMapper.insert(refereeInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 删除裁判信息
     *
     * @param refereeId signification
     * @return boolean
     */
    @Override
    public boolean removeReferee(String refereeId) {
        boolean flag = false;
        int result = refereeInfoMapper.deleteByRefereeId(refereeId);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 更新裁判信息
     *
     * @param refereeInfoVO signification
     * @return boolean
     */
    @Override
    public boolean updateReferee(RefereeInfoVO refereeInfoVO) {
        boolean flag = false;
        RefereeInfoDO refereeInfoDO = POJOUtils.VO2DO(refereeInfoVO, RefereeInfoDO.class);
        refereeInfoDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = refereeInfoMapper.update(refereeInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 通过裁判Id查找裁判信息
     *
     * @param refereeId signification
     * @return RefereeInfoVO
     */
    @Override
    public RefereeInfoVO getByRefereeId(String refereeId) {
        RefereeInfoDO refereeInfoDO = refereeInfoMapper.getByRefereeId(refereeId);
        return POJOUtils.DO2VO(refereeInfoDO, RefereeInfoVO.class);
    }

    /**
     * 通过裁判Name查找裁判信息
     *
     * @param refereeName signification
     * @return RefereeInfoVO
     */
    @Override
    public RefereeInfoVO getByRefereeName(String refereeName) {
        RefereeInfoDO refereeInfoDO = refereeInfoMapper.getByRefereeId(refereeName);
        return POJOUtils.DO2VO(refereeInfoDO, RefereeInfoVO.class);
    }

    /**
     * 统计所有裁判信息
     *
     * @return RefereeInfoVO
     */
    @Override
    public List<RefereeInfoVO> listAll() {
        List<RefereeInfoVO> refereeInfoVOList = new ArrayList<>();
        List<RefereeInfoDO> refereeInfoDOList = refereeInfoMapper.listAll();
        for(RefereeInfoDO refereeInfoDO : refereeInfoDOList){
            RefereeInfoVO refereeInfoVO = POJOUtils.DO2VO(refereeInfoDO, RefereeInfoVO.class);
            refereeInfoVO.setRefereeSex(SystemUtils.sexTypeBind(refereeInfoVO.getRefereeSex()));
            refereeInfoVOList.add(refereeInfoVO);
        }
        return refereeInfoVOList;
    }

    @Override
    public RefereeInfoDO getLatestByRefereeItem(String refereeItem) {
        return refereeInfoMapper.getLatestByRefereeItem(refereeItem);
    }

    /**
     * 统计所有空闲的裁判信息
     *
     * @return RefereeInfoVO
     */
    @Override
    public List<RefereeInfoVO> listAllFreeReferee() {
        List<RefereeInfoVO> refereeInfoVOList = new ArrayList<>();
        List<RefereeInfoDO> refereeInfoDOList = refereeInfoMapper.listAllFreeReferee();
        for(RefereeInfoDO refereeInfoDO : refereeInfoDOList){
            RefereeInfoVO refereeInfoVO = POJOUtils.DO2VO(refereeInfoDO, RefereeInfoVO.class);
            refereeInfoVO.setRefereeSex(SystemUtils.sexTypeBind(refereeInfoVO.getRefereeSex()));
            refereeInfoVOList.add(refereeInfoVO);
        }
        return refereeInfoVOList;
    }

    /**
     * 统计所有比赛的裁判信息
     *
     * @return RefereeInfoVO
     */
    @Override
    public List<RefereeInfoVO> listAllBusyReferee() {
        List<RefereeInfoVO> refereeInfoVOList = new ArrayList<>();
        List<RefereeInfoDO> refereeInfoDOList = refereeInfoMapper.listAllBusyReferee();
        for(RefereeInfoDO refereeInfoDO : refereeInfoDOList){
            RefereeInfoVO refereeInfoVO = POJOUtils.DO2VO(refereeInfoDO, RefereeInfoVO.class);
            refereeInfoVO.setRefereeSex(SystemUtils.sexTypeBind(refereeInfoVO.getRefereeSex()));
            refereeInfoVOList.add(refereeInfoVO);
        }
        return refereeInfoVOList;
    }
}
