package com.hlhome.cllz.gyms.module.material.mapper;

import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 器材信息Mapper(DAO)类
 *
 * Created by zmz on 2018/6/23
 */
@Mapper
public interface MaterialInfoMapper {

    /**
     * 新增器材信息
     */
    int insert (MaterialInfoDO materialInfoDO);

    /**
     *通过器材id，删除器材信息
     */
    int deleteByMaterialId (String materialId);

    /**
     *更新器材信息
     */
    int update (MaterialInfoDO materialInfoDO);

    /**
     *遍历器材信息
     */
    List<MaterialInfoDO> listAll();

    /**
     *通过器材id，查询器材信息
     */
    MaterialInfoDO getByMaterialId(String materialId);

    /**
     *通过器材名字，查询器材信息
     */
    MaterialInfoDO getByMaterialName(String materialName);

    /**
     * 通过器材类型 获取最新的器材记录
     */
    MaterialInfoDO getLatestByMaterialType(String materialType);

}
