package com.hlhome.cllz.gyms.module.game.mapper;

import com.hlhome.cllz.gyms.module.game.domain.GameInfoDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by lingb on 2018/6/22
 */
@Mapper
public interface GameInfoMapper {

    int deleteByGameId(String gameId);

    int insert(GameInfoDO gameInfoDO);

    GameInfoDO getByGameId(@Param(value = "gameId") String gameId);

    GameInfoDO getByGameName(@Param(value = "gameName") String gameName);

    GameInfoDO getByBookId(@Param(value = "bookId") String bookId);

    List<GameInfoDO> listByUserId(@Param(value = "userId") String userId);

    List<GameInfoDO> listAll();

    int update(GameInfoDO gameInfoDO);

    /**
     * 通过赛事类型 获取最新的赛事记录
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.module.game.domain.GameInfoDO
     */
    GameInfoDO getLatestByGameType(String gameType);

    /**
     * 通过赛事编号和日期，获取赛事列表
     *
     * @param   [gameId, date]
     * @return  java.util.List<com.hlhome.cllz.gyms.module.ground.domain.GroundRentDO>
     */
    List<GameInfoDO> listByGameIdAndDate(String gameId, String date);

}
