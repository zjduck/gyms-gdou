package com.hlhome.cllz.gyms.module.ground.service;

import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoDO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoRentTypeVO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoVO;

import java.util.List;

/**
 * Created by luckyTsai on 2018/6/23
 */
public interface GroundInfoService {

    /**
     * 统计所有空闲的场地信息
     */
    List<GroundInfoVO> listAll();

    /**
     * 统计所有使用中的场地
     */
    List<GroundInfoVO> listUse();

    /**
     * 通过场地id获取场地信息
     */
    GroundInfoVO getByGroundId(String groundId);

    /**
     * 通过场地名称获取场地信息
     */
    GroundInfoVO getByGroundName(String groundName);

    /**
     * 添加场地
     */
    boolean saveGround(GroundInfoVO groundInfoVO);

    /**
     * 更新场地
     */
    boolean updateGround(GroundInfoVO groundInfoVO);

    /**
     * 删除场地
     */
    boolean removeByGroundId(String groundId);

    /**
     * 通过场地类型，获取最新的场地信息
     */
    GroundInfoDO getLatestByGroundType(String groundType);

    /**
     * 统计所有场地信息（基本信息、类型、租借）
     * @return
     */
    List<GroundInfoRentTypeVO> listAllInfo();
}
