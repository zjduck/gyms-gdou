package com.hlhome.cllz.gyms.module.game.service;

import com.hlhome.cllz.gyms.module.game.domain.GameTypeDO;
import com.hlhome.cllz.gyms.module.game.domain.GameTypeVO;
import com.hlhome.cllz.gyms.module.game.mapper.GameTypeMapper;
import com.hlhome.cllz.gyms.util.Asserts;
import com.hlhome.cllz.gyms.util.POJOUtils;
import com.hlhome.cllz.gyms.util.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
@Service
@Transactional
public class GameTypeServiceImpl implements GameTypeService {

    private static final Logger logger = LoggerFactory.getLogger(GameTypeServiceImpl.class);

    @Autowired
    private GameTypeMapper gameTypeMapper;

    @Override
    public List<GameTypeVO> listAll() {
        List<GameTypeDO> gameTypeDOList = gameTypeMapper.listAll();
        return POJOUtils.DO2VOList(gameTypeDOList, GameTypeVO.class);
    }

    @Override
    public GameTypeVO getByTypeId(String typeId) {
        GameTypeDO gameTypeDO = gameTypeMapper.getByTypeId(typeId);
        return POJOUtils.DO2VO(gameTypeDO, GameTypeVO.class);
    }

    @Override
    public GameTypeVO getByTypeName(String typeName) {
        GameTypeDO gameTypeDO = gameTypeMapper.getByTypeName(typeName);
        return POJOUtils.DO2VO(gameTypeDO, GameTypeVO.class);
    }

    @Override
    public boolean saveGameType(GameTypeVO gameTypeVO) {
        boolean flag = false;
        Asserts.validate(gameTypeVO,"gameTypeVO");

        GameTypeDO gameTypeDO = POJOUtils.VO2DO(gameTypeVO, GameTypeDO.class);
        gameTypeDO.setGmtCreate(LocalDateTime.now().withNano(0));
        gameTypeDO.setGmtModified(LocalDateTime.now().withNano(0));
        gameTypeDO.setTypeId(SystemUtils.gameTypeBind(gameTypeVO.getTypeName()));
        int result = gameTypeMapper.insert(gameTypeDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public boolean updateGameType(GameTypeVO gameTypeVO) {
        boolean flag = false;
        Asserts.validate(gameTypeVO,"gameTypeVO");

        GameTypeDO gameTypeDO = POJOUtils.VO2DO(gameTypeVO, GameTypeDO.class);
        gameTypeDO.setGmtModified(LocalDateTime.now().withNano(0));
        int result = gameTypeMapper.update(gameTypeDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public boolean removeByTypeId(String typeId) {
        boolean flag = false;
        int result = gameTypeMapper.deleteByTypeId(typeId);
        flag = result == 0 ? false : true;
        return flag;
    }
}
