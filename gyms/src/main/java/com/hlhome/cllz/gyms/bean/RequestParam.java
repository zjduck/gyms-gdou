package com.hlhome.cllz.gyms.bean;

import java.io.Serializable;

public class RequestParam<T> implements Serializable {
    //其他省略
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
