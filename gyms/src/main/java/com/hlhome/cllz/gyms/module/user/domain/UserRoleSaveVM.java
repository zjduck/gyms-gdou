package com.hlhome.cllz.gyms.module.user.domain;


public class UserRoleSaveVM {
	
    private String userId;

    private String roleId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	//这里不需要有业务，新增业务角色应该单独写接口
	//@ApiModelProperty(value = "业务角色id")
    //private String ywId;
	
	
}
