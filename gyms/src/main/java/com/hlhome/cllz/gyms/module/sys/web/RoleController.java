package com.hlhome.cllz.gyms.module.sys.web;


import com.hlhome.cllz.gyms.module.sys.domain.Role;
import com.hlhome.cllz.gyms.module.sys.domain.RoleResourceVM;
import com.hlhome.cllz.gyms.module.sys.domain.RoleSaveVM;
import com.hlhome.cllz.gyms.module.sys.domain.RoleUpdateVM;
import com.hlhome.cllz.gyms.module.sys.service.impl.RoleResourceService;
import com.hlhome.cllz.gyms.module.sys.service.impl.RoleService;
import com.hlhome.cllz.gyms.util.Constants;
import com.hlhome.cllz.gyms.util.Message;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "/api/v1")
public class RoleController {

	@Autowired
	RoleService roleService;
	
	@Autowired
	RoleResourceService roleResourceService;

	@RequestMapping(value="/role",method=RequestMethod.POST)
	@RequiresPermissions(value={"role_saveRole","administrator"},logical= Logical.OR)
	public Message<String> saveRole(RoleSaveVM role){
		roleService.saveRole(role);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequestMapping(value="/role/{roleId}",method=RequestMethod.DELETE)
	@RequiresPermissions(value={"role_deleteRole","administrator"},logical= Logical.OR)
	public Message<String> deleteRole(@PathVariable("roleId")String roleId){
		roleService.deleteRole(roleId);
		return Message.ok(roleId);
	}
	

	@RequiresPermissions(value={"role_updateRole","administrator"},logical= Logical.OR)
	@RequestMapping(value="/role",method=RequestMethod.PUT)
	public Message<String>updateRole(RoleUpdateVM roleUpdateVM){
		roleService.updateRole(roleUpdateVM);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"role_getRoleList","administrator"},logical= Logical.OR)
	@RequestMapping(value="/role",method=RequestMethod.GET)
	public Message<List<Role>>getRoleList(){
		List<Role> roleList=roleService.getRoleList();
		return Message.ok(roleList);
	}
	

	@RequiresPermissions(value={"role_authorize","administrator"},logical= Logical.OR)
	@RequestMapping(value="/role/authorize",method=RequestMethod.POST)
	public  Message<String> authorize(RoleResourceVM roleResource){
		roleService.authorize(roleResource);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"role_isExist","administrator"},logical= Logical.OR)
	@RequestMapping(value="/role/isExist",method=RequestMethod.GET)
	public Message<Boolean> isExist(@RequestParam(value="code")String code){
		Boolean isExist=roleService.isExist(code);
		return Message.ok(isExist);
	}
	

	@RequiresPermissions(value={"role_getRoleResource","administrator"},logical= Logical.OR)
	@RequestMapping(value="/role/getRoleResource",method=RequestMethod.GET)
	public Message<List<String>>getRoleResource(String roleId){
		List<String >roleIds=new ArrayList<>();roleIds.add(roleId);
		List<String>list=roleResourceService.getResource(roleIds);
		return Message.ok(list);
	}
}
