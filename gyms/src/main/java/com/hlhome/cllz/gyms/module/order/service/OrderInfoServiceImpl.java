package com.hlhome.cllz.gyms.module.order.service;

import com.hlhome.cllz.gyms.module.game.domain.GameInfoVO;
import com.hlhome.cllz.gyms.module.game.service.GameInfoService;
import com.hlhome.cllz.gyms.module.ground.service.GroundRentService;
import com.hlhome.cllz.gyms.module.material.service.MaterialRentService;
import com.hlhome.cllz.gyms.module.order.domain.OrderInfoDO;
import com.hlhome.cllz.gyms.module.order.domain.OrderInfoVO;
import com.hlhome.cllz.gyms.module.order.mapper.OrderInfoMapper;
import com.hlhome.cllz.gyms.module.referee.service.RefereeRentService;
import com.hlhome.cllz.gyms.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
@Service
@Transactional
public class OrderInfoServiceImpl implements OrderInfoService {

    private static final Logger logger = LoggerFactory.getLogger(OrderInfoServiceImpl.class);

    @Autowired
    private GameInfoService gameInfoService;

    @Autowired
    private RefereeRentService refereeRentService;

    @Autowired
    private GroundRentService groundRentService;

    @Autowired
    private MaterialRentService materialRentService;

    @Autowired
    private OrderInfoMapper orderInfoMapper;

    /**
     * 新增订单
     * 1、获取订单号
     * 2、计算订单总金额
     *      通过bookId 获取相应模块的收费金额
     * 3、新增操作
     *
     * @param   [orderInfoVO]
     * @return  boolean
     */
    @Override
    public boolean saveOrderInfo(OrderInfoVO orderInfoVO) {
        boolean flag = false;
        Asserts.validate(orderInfoVO,"orderInfoVO");

        // 1、
        // 获取最新一条订单记录
        OrderInfoDO latestOrder = getLatestOrderById();
        String orderId = "";
        // 若为空，数据库首条订单记录
        if (latestOrder == null) {
            orderId = Constants.ORDER + DateUtils.getNowDate() + "001";

            // 否则，预约记录递增（自动按日期走）
        } else {
            orderId = KeyGenerator.getInstance().generateNum(latestOrder.getOrderId());

        }

        // 2、
        String bookId = orderInfoVO.getBookId();
        GameInfoVO gameInfoVO = gameInfoService.getByBookId(bookId);



        OrderInfoDO orderInfoDO = POJOUtils.VO2DO(orderInfoVO, OrderInfoDO.class);
        orderInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        orderInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        orderInfoDO.setOrderId(orderId);
        int result = orderInfoMapper.insert(orderInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public boolean updateOrderInfo(OrderInfoVO orderInfoVO) {
        boolean flag = false;
        OrderInfoDO orderInfoDO = POJOUtils.VO2DO(orderInfoVO, OrderInfoDO.class);
        orderInfoDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = orderInfoMapper.update(orderInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public boolean removeOrderInfo(String orderId) {
        boolean flag = false;
        int result = orderInfoMapper.deleteByOrderId(orderId);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public OrderInfoVO getByBookId(String bookId) {
        OrderInfoDO orderInfoDO = orderInfoMapper.getByBookId(bookId);
        return POJOUtils.DO2VO(orderInfoDO, OrderInfoVO.class);
    }

    @Override
    public List<OrderInfoVO> listAll() {
        List<OrderInfoDO> orderInfoDOList = orderInfoMapper.listAll();
        return POJOUtils.DO2VOList(orderInfoDOList, OrderInfoVO.class);
    }

    @Override
    public OrderInfoDO getLatestOrderById() {
        return orderInfoMapper.getLatestOrderById();
    }
}
