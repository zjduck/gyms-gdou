package com.hlhome.cllz.gyms.module.sys.mapper;

import com.hlhome.cllz.gyms.module.sys.domain.Authority;
import com.hlhome.cllz.gyms.module.sys.domain.AuthorityController;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AuthorityExtendMapper {

	/** 
	 * 根据 权限id 查权限对象集
	 */
	public List<Authority>getAuthorityByIds(@Param(value = "ids") List<String> ids);

	/**
	 * 按controller分组查 操作码列表
	 */
	public List<AuthorityController>getAuthority();

	/**
	 * 根据请求方式，关键字模糊查询
	 */
	public List<Authority>getLikeAuthority(@Param(value = "method") String method, @Param(value = "word") String word);
}
