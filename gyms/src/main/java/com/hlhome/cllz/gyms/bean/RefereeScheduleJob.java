package com.hlhome.cllz.gyms.bean;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoVO;
import com.hlhome.cllz.gyms.module.referee.service.RefereeInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

import static com.hlhome.cllz.gyms.util.Constants.REFEREE_STATE_SPARE;

/**
 * Created by lingb on 2018/7/1
 */
@Configuration
@Component // 该注解必须要加
@EnableScheduling // 该注解必须要加
public class RefereeScheduleJob {

    @Autowired
    private RefereeInfoService refereeInfoService;

    /**
     * "恢复" 裁判状态
     *
     * @param   []
     * @return  boolean
     */
    public void resetReferee() {
        System.err.println("RefereeScheduleJob开始定时执行-------------------------------------------------------" + LocalDateTime.now().withNano(0));
        List<RefereeInfoVO> refereeInfoVOList = refereeInfoService.listAllBusyReferee();
        for (RefereeInfoVO refereeInfoVO : refereeInfoVOList) {
            // 状态 “空闲”
            refereeInfoVO.setRefereeState(REFEREE_STATE_SPARE);
            refereeInfoService.updateReferee(refereeInfoVO);

        }
    }
}
