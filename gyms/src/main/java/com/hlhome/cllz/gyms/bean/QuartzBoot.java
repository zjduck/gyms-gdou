package com.hlhome.cllz.gyms.bean;


import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;


/**
 * Created by lingb on 2018/7/1
 */
public class QuartzBoot {

    private static Logger logger = LoggerFactory.getLogger(QuartzBoot.class);

    public static void main(String[] args) {

        try {
            // 获取调度器
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // 开启调度器
            scheduler.start();

            // 注册一个示例任务和触发器
            registerJobAndTrigger(scheduler);

            String requestedFile = System.getProperty("org.quartz.properties");
            String propFileName = requestedFile != null ? requestedFile : "quartz.properties";
            System.out.println(requestedFile);
            System.out.println(propFileName);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            scheduler.shutdown();

        } catch (SchedulerException se) {
            logger.error("调度器初始化异常", se);
        }
    }

    /**
     * 注册一个任务和触发器
     */
    public static void registerJobAndTrigger(Scheduler scheduler) {
        JobDetail job = JobBuilder.newJob(MySimpleJob.class)
                .withIdentity("mySimpleJob", "simpleGroup")
                .build();

        Trigger trigger = org.quartz.TriggerBuilder.newTrigger()
                .withIdentity("simpleTrigger", "simpleGroup")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(10)
                        .repeatForever())
                .build();

        try {
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            logger.error("初始化（注册）任务和触发器失败", e);
        }
    }

    /**
     * 简单的任务
     */
    public static class MySimpleJob implements Job {
        @Override
        public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
            logger.info("哇真的执行了");
        }
    }

}