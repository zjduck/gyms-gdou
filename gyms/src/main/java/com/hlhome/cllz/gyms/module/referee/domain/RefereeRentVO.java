package com.hlhome.cllz.gyms.module.referee.domain;

/**
 * 裁判租借VO类
 *
 * @author LoongMH
 * @date 2018/06/23
 */
public class RefereeRentVO {

    private String refereeId;
    private String refereeItem;
    private String bookId;
    private String rentTime;
    private String periods;
    private String auditState;
    private String remark;

    public String getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(String refereeId) {
        this.refereeId = refereeId;
    }

    public String getRefereeItem() {
        return refereeItem;
    }

    public void setRefereeItem(String refereeItem) {
        this.refereeItem = refereeItem;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getRentTime() {
        return rentTime;
    }

    public void setRentTime(String rentTime) {
        this.rentTime = rentTime;
    }

    public String getPeriods() { return periods; }

    public void  setPeriods(String periods) { this.periods = periods; }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) { this.remark = remark; }

    @Override
    public String toString() {
        return "RefereeRentVO{" +
                "rentId='" + refereeId + '\'' +
                ", sportItem='" + refereeItem + '\'' +
                ", bookId='" + bookId + '\'' +
                ", rentTime='" + rentTime + '\'' +
                ", periods='" + periods + '\'' +
                ", auditState='" + auditState + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}