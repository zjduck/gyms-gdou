package com.hlhome.cllz.gyms.module.book.service;

import com.hlhome.cllz.gyms.module.book.domain.BookInfoDO;
import com.hlhome.cllz.gyms.module.book.domain.BookInfoVO;

import java.util.List;

/**
 * Created by lingb on 2018/6/24
 */
public interface BookInfoService {
/*
    *//**
     * 添加预约
     *
     * @param   [bookInfoVO]
     * @return  void
     *//*
    void saveBookInfo(BookInfoVO bookInfoVO);*/

    /**
     * 更新预约
     *
     * @param   [bookInfoVO]
     * @return  void
     */
    boolean updateBookInfo(BookInfoVO bookInfoVO);

    /**
     * 删除预约
     *
     * @param   [bookId]
     * @return  void
     */
    boolean removeBookInfo(String bookId);

  /*  *//**
     * 获取单次预约
     *
     * @param   [bookId]
     * @return  com.hlhome.cllz.gyms.module.book.domain.BookInfoVO
     *//*
    BookInfoVO getByBookId(String bookId);*/

    /**
     * 统计所有预约
     *
     * @param   []
     * @return  java.util.List<com.hlhome.cllz.gyms.module.book.domain.BookInfoVO>
     */
    List<BookInfoVO> listAll();

    /**
     * 通过Id排序 获取最新的预约记录
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.module.book.domain.BookInfoDO
     */
    BookInfoDO getLatestOrderById();
}




