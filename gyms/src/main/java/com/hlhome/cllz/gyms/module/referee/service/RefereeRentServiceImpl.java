package com.hlhome.cllz.gyms.module.referee.service;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentDO;
import com.hlhome.cllz.gyms.module.referee.mapper.RefereeRentMapper;
import com.hlhome.cllz.gyms.util.*;
import com.hlhome.cllz.gyms.util.POJOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
@Service
@Transactional
public class RefereeRentServiceImpl implements RefereeRentService{

    private static final Logger logger = LoggerFactory.getLogger(RefereeRentServiceImpl.class);

    @Autowired
    private RefereeRentMapper refereeRentMapper;

    /**
     * 添加裁判租借
     *
     * @param refereeRentVO signification
     * @return boolean
     */
    @Override
    public synchronized boolean addRent(RefereeRentVO refereeRentVO) {
        boolean flag = false;
        Asserts.validate(refereeRentVO,"refereeRentVO");

        RefereeRentDO refereeRentDO = new RefereeRentDO();
        // VO -> DO
        refereeRentDO = POJOUtils.VO2DO(refereeRentVO, RefereeRentDO.class);

        refereeRentDO.setGmtCreate(LocalDateTime.now().withNano(0));
        refereeRentDO.setGmtModified(LocalDateTime.now().withNano(0));
        RefereeRentDO latestRent = getLatestByRefereeItem(refereeRentVO.getRefereeItem());
        // 若为空，该裁判租借Id为第一个
        if (latestRent == null) {
            String refereeRentNum = SystemUtils.refereeTypeBind(refereeRentVO.getRefereeItem());
            refereeRentDO.setRefereeId(Constants.REFEREE + refereeRentNum + "001");

            // 否则，该裁判租借Id递增
        } else {
            String refereeItem = SystemUtils.refereeTypeBind(refereeRentVO.getRefereeItem());
            String refereeId = KeyGenerator.getInstance().generateNum(latestRent.getRefereeItem(), refereeItem);
            refereeRentDO.setRefereeId(refereeId);
        }

        int result = refereeRentMapper.insert(refereeRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 删除裁判租借
     *
     * @param refereeId signification
     * @return boolean
     */
    @Override
    public boolean removeRent(String refereeId) {
        boolean flag = false;
        int result = refereeRentMapper.deleteByRefereeId(refereeId);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 更新裁判租借
     *
     * @param refereeRentVO signification
     * @return boolean
     */
    @Override
    public boolean updateRent(RefereeRentVO refereeRentVO) {
        boolean flag = false;
        RefereeRentDO refereeRentDO = POJOUtils.VO2DO(refereeRentVO, RefereeRentDO.class);
        refereeRentDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = refereeRentMapper.update(refereeRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 通过裁判Id查找裁判租借
     *
     * @param refereeId signification
     * @return RefereeRentVO
     */
    @Override
    public RefereeRentVO getByRefereeId(String refereeId) {
        RefereeRentDO refereeRentDO = refereeRentMapper.getByRefereeId(refereeId);
        return POJOUtils.DO2VO(refereeRentDO, RefereeRentVO.class);
    }

    /**
     * 通过预约Id查找裁判租借
     *
     * @param bookId signification
     * @return RefereeRentVO
     */
    @Override
    public List<RefereeRentVO> listByBookId(String bookId) {
        List<RefereeRentDO> refereeRentDOList = refereeRentMapper.listByBookId(bookId);
        return POJOUtils.DO2VOList(refereeRentDOList, RefereeRentVO.class);
    }

    /**
     * 统计所有裁判租借
     *
     * @return RefereeRentVO
     */
    @Override
    public List<RefereeRentVO> listAll() {
        List<RefereeRentVO> refereeRentVOList = new ArrayList<>();
        List<RefereeRentDO> refereeRentDOList = refereeRentMapper.listAll();
        for(RefereeRentDO refereeRentDO : refereeRentDOList){
            RefereeRentVO refereeRentVO = POJOUtils.DO2VO(refereeRentDO, RefereeRentVO.class);
            refereeRentVOList.add(refereeRentVO);
        }
        return refereeRentVOList;
    }

    /**
     * 通过裁判类型查找最新裁判租借记录
     */
    @Override
    public RefereeRentDO getLatestByRefereeItem(String refereeItem) {
        return refereeRentMapper.getLatestByRefereeItem(refereeItem);
    }

    /**
     * 审核裁判租借
     *
     * @param refereeRentVO signification
     * @return boolean
     */
    @Override
    public boolean checkRent(RefereeRentVO refereeRentVO) {
        boolean flag = false;
        RefereeRentDO refereeRentDO = POJOUtils.VO2DO(refereeRentVO, RefereeRentDO.class);
        refereeRentDO.setGmtModified(LocalDateTime.now().withNano(0));
        refereeRentDO.setAuditState("通过");

        int result = refereeRentMapper.update(refereeRentDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 根据裁判编号和日期查找裁判租借
     *
     * @param refereeId signification
     * @param date signification
     * @return boolean
     */
    @Override
    public List<RefereeRentVO> listByRefereeIdAndDate(String refereeId, String date) {
        List<RefereeRentVO> refereeRentVOList = new ArrayList<>();
        List<RefereeRentDO> refereeRentDOList = refereeRentMapper.listByRefereeIdAndDate(refereeId, date);
        for(RefereeRentDO refereeRentDO : refereeRentDOList){
            if("通过".equals(refereeRentDO.getAuditState())) {
                RefereeRentVO refereeRentVO = POJOUtils.DO2VO(refereeRentDO, RefereeRentVO.class);
                refereeRentVOList.add(refereeRentVO);
            }
        }
        return refereeRentVOList;
    }
}
