package com.hlhome.cllz.gyms.module.ground.web;


import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoRentTypeVO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoVO;
import com.hlhome.cllz.gyms.module.ground.service.GroundInfoService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by luckyTsai on 2018/6/24
 */
@RequestMapping("/api/v1")
@RestController
public class GroundInfoController {
    private static final Logger logger = LoggerFactory.getLogger(GroundInfoController.class);

    @Autowired
    private GroundInfoService groundInfoService;

    /**
     * 新增场地信息
     * 需要 ground_addGroundInfo权限或管理员权限
     *
     * @param   [GroundInfoVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_addGroundInfo", "admin"}, logical = Logical.OR)
    @PostMapping("/ground/info")
    public WebResult addGroundInfo(@RequestBody GroundInfoVO groundInfoVO) {
        boolean flag = groundInfoService.saveGround(groundInfoVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }

    /**
     * 更新场地信息
     * 需要 ground_updateGroundInfo权限或管理员权限
     *
     * @param   [groundInfoVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_updateGroundInfo", "admin"}, logical = Logical.OR)
    @PatchMapping("/ground/info")
    // @RequestMapping(value = "", method = RequestMethod.PUT)
    public WebResult updateGroundInfo(@RequestBody GroundInfoVO groundInfoVO) {
        boolean flag = groundInfoService.updateGround(groundInfoVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }

    /**
     * 删除场地信息
     * 需要 ground_deleteGroundInfo权限或管理员权限
     *
     * @param   [groundId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_deleteDroundInfo", "admin"}, logical = Logical.OR)
    @DeleteMapping("/ground/info/{groundId}")
    public WebResult deleteGroundInfo(@PathVariable("groundId") String groundId) {
        if(groundId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = groundInfoService.removeByGroundId(groundId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }

    /**
     * 获取单个场地信息
     * 需要 ground_getGroundInfoDetail权限或管理员权限
     *
     * @param   [groundId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_getGroundInfoDetail", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/info/{groundId}")
    public WebResult getGroundInfoDetail(@PathVariable String groundId) {
        GroundInfoVO groundInfoVO = groundInfoService.getByGroundId(groundId);
        if (groundInfoVO != null) {
            return WebResult.success(groundInfoVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    /**
     * 获取所有空闲的场地信息
     * 需要 ground_listAllGround权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_listAllGround", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/info1")
    public WebResult listAllGround() {
        List<GroundInfoVO> groundInfoVOList = groundInfoService.listAll();
        if (groundInfoVOList != null) {
            return WebResult.success(groundInfoVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }

    /**
     * 获取所有场地信息、类型、租借
     * 需要 ground_listAllGroundInfo权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_listAllGroundInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/info")
    public WebResult listAllGroundInfo() {
        List<GroundInfoRentTypeVO> groundInfoRentTypeVOList = groundInfoService.listAllInfo();
        if (groundInfoRentTypeVOList != null) {
            return WebResult.success(groundInfoRentTypeVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }

}
