package com.hlhome.cllz.gyms.module.game.service;

import com.hlhome.cllz.gyms.module.game.domain.GameTypeVO;

import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
public interface GameTypeService {


    /**
     * 统计所有赛事类型
     *
     * @param   []
     * @return  java.util.List<com.hlhome.cll.gyms.module.game.domain.GameTypeVO>
     */
    List<GameTypeVO> listAll();

    /**
     * 通过类型Id 获取
     * @param   [typeId]
     * @return  com.hlhome.cll.gyms.module.game.domain.GameTypeVO
     */
    GameTypeVO getByTypeId(String typeId);

    /**
     * 通过类型名 获取
     * @param   [typeName]
     * @return  com.hlhome.cll.gyms.module.game.domain.GameTypeVO
     */
    GameTypeVO getByTypeName(String typeName);

    /**
     * 添加赛事类型
     *
     * @param   [gameTypeVO]
     * @return  int
     */
    boolean saveGameType(GameTypeVO gameTypeVO);

    /**
     * 更新赛事类型
     * @param   [gameTypeVO]
     * @return  java.lang.Boolean
     */
    boolean updateGameType(GameTypeVO gameTypeVO);

    /**
     * 删除赛事类型
     * @param   [typeId]
     * @return  java.lang.Boolean
     */
    boolean removeByTypeId(String typeId);

}
