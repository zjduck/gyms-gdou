package com.hlhome.cllz.gyms.module.sys.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class RoleSaveVM {
	
	@NotBlank
    private String name;

	@NotBlank
    private String code;

	@NotNull
	@Max(100)
	@Min(1)
    private Byte sort;

	@NotNull
	@Max(3)
	@Min(1)
    private Byte type;//

    private String des;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Byte getSort() {
		return sort;
	}

	public void setSort(Byte sort) {
		this.sort = sort;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}
    
    
}
