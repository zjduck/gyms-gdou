package com.hlhome.cllz.gyms.module.material.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoVO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialRentVO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialTypeVO;
import com.hlhome.cllz.gyms.module.material.service.MaterialInfoService;
import com.hlhome.cllz.gyms.module.material.service.MaterialRentService;
import com.hlhome.cllz.gyms.module.material.service.MaterialTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by zmz on 2018/6/23
 */


@RequestMapping("/api/v1")
@RestController
public class MaterialController {

    private static final Logger logger = LoggerFactory.getLogger(MaterialController.class);

    @Autowired
    private MaterialInfoService materialInfoService;
    @Autowired
    private MaterialTypeService materialTypeService;
    @Autowired
    private MaterialRentService materialRentService;

    /**
     *新增器材基本信息（购买器材）
     *
     *需要 material_addMaterialInfo权限或管理员权限
     */

   // @RequiresPermissions(value={"material_addMaterialInfo", "admin"}, logical = Logical.OR)
    @PostMapping("/material/info")
    //新增器材基本信息
    public WebResult addMaterialInfo(@RequestBody MaterialInfoVO materialInfoVO) {
        boolean flag = materialInfoService.saveMaterialInfo(materialInfoVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }

    /**
     *新增器材租借信息（租借器材）
     *
     *需要 material_addMaterialRent权限或管理员权限
     */

//    @RequiresPermissions(value={"material_addMaterialRent", "admin"}, logical = Logical.OR)
    @PostMapping("/material/rent")
    //新增器材租借信息
    public WebResult addMaterialRent(@RequestBody MaterialRentVO materialRentVO) {
        boolean flag = materialRentService.saveMaterialRent(materialRentVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }

    /**
     *新增器材类型信息
     *
     *需要 material_addMaterialType权限或管理员权限
     */

   // @RequiresPermissions(value={"material_addMaterialType", "admin"}, logical = Logical.OR)
    @PostMapping("/material/type")
    //新增器材类型信息
    public WebResult addMaterialType(@RequestBody MaterialTypeVO materialTypeVO) {
        boolean flag = materialTypeService.saveMaterialType(materialTypeVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }


    /**
     *删除器材信息表、器材类型表或器材租借表信息（器材报废）
     *
     *需要 material_deleteMaterialInfo、material_deleteMaterialRent或material_deleteMaterialType权限或管理员权限
     */

    //@RequiresPermissions(value={"material_deleteMaterialInfo", "admin"}, logical = Logical.OR)
    @DeleteMapping("/material/info/{materialId}")
    //删除器材基本信息
    public WebResult deleteMaterialInfo(@PathVariable("materialId") String materialId) {
        if(materialId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = materialInfoService.removeByMaterialId(materialId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }
    //删除器材租借信息
    //@RequiresPermissions(value={"material_deleteMaterialRent", "admin"}, logical = Logical.OR)
    @DeleteMapping("/material/rent")
    public WebResult deleteMaterialRent(@RequestBody MaterialRentVO materialRentVO ) {
        boolean flag = materialRentService.removeByBookIdAndMaterialId(materialRentVO);
        if (flag) {
            return WebResult.success(flag,"删除成功！");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }
    //删除器材类型信息
    //@RequiresPermissions(value={"material_deleteMaterialType", "admin"}, logical = Logical.OR)
    @DeleteMapping("/material/type/{typeId}")
    public WebResult deleteMaterialType(@PathVariable("typeId") String typeId) {
        if(typeId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = materialTypeService.removeByTypeId(typeId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }

    /**
     *修改器材信息表、器材类型表或器材租借表信息（器材租借、器材归还）
     *
     *需要 material_updateMaterialInfo、material_updateMaterialRent或material_updateMaterialType权限或管理员权限
     */

    //@RequiresPermissions(value={"material_updateMaterialInfo", "admin"}, logical = Logical.OR)
    @PatchMapping("/material/info")
    //更改器材基本信息
    public WebResult updateMaterialInfo(@RequestBody MaterialInfoVO materialInfoVO) {
        boolean flag = materialInfoService.updateMaterialInfo(materialInfoVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }
    //@RequiresPermissions(value={"material_updateMaterialRent", "admin"}, logical = Logical.OR)
    @PatchMapping("/material/rent")
    //更改器材租借信息
    public WebResult updateMaterialRent(@RequestBody MaterialRentVO materialRentVO) {
        boolean flag = materialRentService.updateMaterialRent(materialRentVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }
    //@RequiresPermissions(value={"material_updateMaterialType", "admin"}, logical = Logical.OR)
    @PatchMapping("/material/type")
    //更改器材类型信息
    public WebResult updateMaterialType(@RequestBody MaterialTypeVO materialTypeVO) {
        boolean flag = materialTypeService.updateMaterialType(materialTypeVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }

    /**
     *查找所有器材信息
     *
     *需要 material_getAllMaterialInfo、material_getAllMaterialRent或material_getAllMaterialType权限或管理员权限
     */

    //@RequiresPermissions(value={"material_getAllMaterialInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/material/info")
    public WebResult listAllMaterialInfo() {
        List<MaterialInfoVO> materialInfoVOList = materialInfoService.listAll();
        if (materialInfoVOList != null) {
            return WebResult.success(materialInfoVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }
    //@RequiresPermissions(value={"material_getAllMaterialRent", "admin"}, logical = Logical.OR)
    @GetMapping("/material/rent")
    public WebResult listAllMaterialRent() {
        List<MaterialRentVO> materialRentVOList = materialRentService.listAll();
        if (materialRentVOList != null) {
            return WebResult.success(materialRentVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }
    //@RequiresPermissions(value={"material_getAllMaterialType", "admin"}, logical = Logical.OR)
    @GetMapping("/material/type")
    public WebResult listAllMaterialType() {
        List<MaterialTypeVO> materialTypeVOList = materialTypeService.listAll();
        if (materialTypeVOList != null) {
            return WebResult.success(materialTypeVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }


    /**
     *通过器材ID或者器材名称查找器材信息（搜索器材）
     *
     *需要 material_getInfoByMaterialId或material_getInfoByMaterialName权限或管理员权限
     */

    //@RequiresPermissions(value={"material_getInfoByMaterialId", "admin"}, logical = Logical.OR)
    @GetMapping("/material/info/{materialId}")
    public WebResult getInfoByMaterialId(@PathVariable String materialId) {
        MaterialInfoVO materialInfoVO = materialInfoService.getByMaterialId(materialId);
        if (materialInfoVO != null) {
            return WebResult.success(materialInfoVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }
    //@RequiresPermissions(value={"material_getInfoByMaterialName", "admin"}, logical = Logical.OR)
    @GetMapping("/material/info/{materialName}")
    public WebResult getInfoByMaterialName(@PathVariable String materialName) {
        MaterialInfoVO materialInfoVO = materialInfoService.getByMaterialName(materialName);
        if (materialInfoVO != null) {
            return WebResult.success(materialInfoVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    /**
     *通过器材ID或者预约ID查找器材租借信息（搜索器材租借信息）
     *
     *需要 material_getRentByMaterialId或material_getRentByBookId权限或管理员权限
     */

    //@RequiresPermissions(value={"material_getRentByMaterialId", "admin"}, logical = Logical.OR)
    @GetMapping("/material/rent/{materialId}")
    public WebResult listRentByMaterialId(@PathVariable String materialId) {
        List<MaterialRentVO> materialRentVOList  = materialRentService.listByMaterialId(materialId);
        if (materialRentVOList != null) {
            return WebResult.success(materialRentVOList, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

   // @RequiresPermissions(value={"material_getRentByBookId", "admin"}, logical = Logical.OR)
    @GetMapping("/material/rent/{bookId}")
    public WebResult listRentByBookId(@PathVariable String bookId) {
        List<MaterialRentVO> materialRentVOList   = materialRentService.listByBookId(bookId);
        if (materialRentVOList != null) {
            return WebResult.success(materialRentVOList, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    @RequiresPermissions(value={"material_getRentByBookIdAndMaterialId", "admin"}, logical = Logical.OR)
    @GetMapping("/material/rent/{bookId}/{materialId}")
    public WebResult listRentByBookId(@PathVariable String bookId,String materialId) {
        MaterialRentVO materialRentVO   = materialRentService.getByBookIdAndMaterialId(bookId,materialId);
        if (materialRentVO != null) {
            return WebResult.success(materialRentVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }



    /**
     *通过类型ID或者类型名称查找器材类型信息（搜索器材类型信息）
     *
     *需要 material_getTypeByTypeId或material_getTypeByTypeName权限或管理员权限
     */

    //@RequiresPermissions(value={"material_getTypeByTypeId", "admin"}, logical = Logical.OR)
    @GetMapping("/material/type/{typeId}")
    public WebResult getTypeByTypeId(@PathVariable String typeId) {
        MaterialTypeVO materialTypeVO = materialTypeService.getByTypeId(typeId);
        if (materialTypeVO != null) {
            return WebResult.success(materialTypeVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    //@RequiresPermissions(value={"material_getTypeByTypeName", "admin"}, logical = Logical.OR)
    @GetMapping("/material/type/{typeName}")
    public WebResult getTypeByTypeName(@PathVariable String typeName) {
        MaterialTypeVO materialTypeVO = materialTypeService.getByTypeName(typeName);
        if (materialTypeVO != null) {
            return WebResult.success(materialTypeVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }




    /**
     * 具体功能实现
     *
     *器材审核
     *
     *需要 material_auditMaterialRent权限或管理员权限
     */

/*

    @RequiresPermissions(value={"material_auditMaterialRent", "admin"}, logical = Logical.OR)
    @PutMapping("/{result}")
    public WebResult auditMaterialRent(@RequestBody MaterialRentVO materialRentVO,@PathVariable String result) {


    }
*/







}
