package com.hlhome.cllz.gyms.module.ground.domain;

import java.io.Serializable;

/**
 * 场地租借VO类
 *
 * Created by luckyTsai on 2018/6/22
 */

public class GroundRentVO implements Serializable {


    private String groundId;

    private String bookId;

    private String rentTime;

    private String periods;



    private String auditState;

    private String remark;

    public String getGroundId() {
        return groundId;
    }

    public void setGroundId(String groundId) {
        this.groundId = groundId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getRentTime() {
        return rentTime;
    }

    public void setRentTime(String rentTime) {
        this.rentTime = rentTime;
    }

    public String getPeriods() {return periods;}

    public void setPeriods(String periods) {
        this.periods = periods;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "GroundRentVO{" +
                "groundId='" + groundId + '\'' +
                ", bookId='" + bookId + '\'' +
                ", rentTime='" + rentTime + '\'' +
                ", periods='" + periods + '\'' +
                ", auditState='" + auditState + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
