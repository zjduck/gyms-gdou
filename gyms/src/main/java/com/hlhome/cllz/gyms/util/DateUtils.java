package com.hlhome.cllz.gyms.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by lingb on 2018/6/23
 */
public class DateUtils {


    /**
     * 获取当前时间 年月日毫秒字符串（8位）
     *
     * @param   []
     * @return  java.lang.String
     */
    public static String getNowDate() {

        LocalDate localDate = LocalDate.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String dateStr = localDate.format(dateTimeFormatter);
        return dateStr;
    }

    /**
     * 获取当前时间 年月日时分秒字符串（14位）
     *
     * @param   []
     * @return  java.lang.String
     */
    public static String getNowDateTime() {

        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String dateTimeStr = localDateTime.format(dateTimeFormatter);
        return dateTimeStr;
    }

}
