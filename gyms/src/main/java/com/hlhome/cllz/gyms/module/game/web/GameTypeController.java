package com.hlhome.cllz.gyms.module.game.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.game.domain.GameTypeVO;
import com.hlhome.cllz.gyms.module.game.service.GameTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
@RequestMapping("/api/v1")
@RestController
public class GameTypeController {

    private static final Logger logger = LoggerFactory.getLogger(GameTypeController.class);

    @Autowired
    private GameTypeService gameTypeService;

    /**
     * 新增赛事类型
     * 需要 game_addGameType权限或管理员权限
     *
     * @param   [gameVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_addGameType", "admin"}, logical = Logical.OR)
    @PostMapping("/game/type")
    public WebResult addGameType(@RequestBody GameTypeVO gameTypeVO) {
        boolean flag = gameTypeService.saveGameType(gameTypeVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }

    /**
     * 更新赛事类型
     * 需要 game_updateGameType权限或管理员权限
     *
     * @param   [gameVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_updateGameType", "admin"}, logical = Logical.OR)
    @PutMapping("/game/type")
    public WebResult updateGameInfo(@RequestBody GameTypeVO gameTypeVO) {
        boolean flag = gameTypeService.updateGameType(gameTypeVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }

    /**
     * 删除赛事类型
     * 需要 game_deleteGameType权限或管理员权限
     *
     * @param   [gameId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_deleteGameType", "admin"}, logical = Logical.OR)
    @DeleteMapping("/game/type/{typeId}")
    public WebResult deleteGameInfo(@PathVariable("tyepId") String tyepId) {
        if(tyepId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = gameTypeService.removeByTypeId(tyepId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }

    /**
     * 获取单个赛事类型
     * 需要 user_getGameType权限或管理员权限
     *
     * @param   [gameId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_getGameType", "admin"}, logical = Logical.OR)
    @GetMapping("/game/type/{tyepId}")
    public WebResult getGameType(@PathVariable String tyepId) {
        GameTypeVO gameTypeVO = gameTypeService.getByTypeId(tyepId);
        if (gameTypeVO != null) {
            return WebResult.success(gameTypeVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    /**
     * 获取所有赛事类型
     * 需要 game_listAllGameType权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_listAllGameType", "admin"}, logical = Logical.OR)
    @GetMapping("/game/type")
    public WebResult listAllGameType() {
        List<GameTypeVO> gameTypeVOList = gameTypeService.listAll();
        if (gameTypeVOList != null) {
            return WebResult.success(gameTypeVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }
}
