package com.hlhome.cllz.gyms.module.sys.domain;

import javax.validation.constraints.NotNull;

public class RoleResourceVM {

	@NotNull
	String roleIds;
	
	@NotNull
	String resourceIds;

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}

	public String getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	
	
}
