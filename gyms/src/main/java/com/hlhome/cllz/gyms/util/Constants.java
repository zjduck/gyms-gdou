package com.hlhome.cllz.gyms.util;

public class Constants {

	public static final String SUCCESS = "SUCCESS";
	
	public static final String REDIS_RESOURCE = "REDIS_RESOURCE";
	
	public static final String REDIS_RESOURCE_TREE = "REDIS_RESOURCE_TREE";

	// 日期时间格式
	public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	// 模块编号缩写
	// 赛事
	public static final String GAME = "S";

	// 裁判
	public static final String REFEREE = "R";

	// 场地
	public static final String GROUND = "G";

	// 器材
	public static final String  MATERIAL = "M";

	// 预约
	public static final String BOOK = "B";

	// 订单
	public static final String ORDER = "O";

	// 时间段
	public static final String TIME = "T";

	// 模块状态
	// 赛事举办
	public static final String GAME_STATE_FINISHED = "已举办";
	public static final String GAME_STATE_UNFINISHED = "待举办";
	public static final String GAME_STATE_FINISHING = "举办中";
	public static final String GAME_STATE_CANCEL = "取消";
	// 赛事审核
	public static final String GAME_AUDIT_SUCCESS = "成功";
	public static final String GAME_AUDIT_FAILURE = "失败";
	public static final String GAME_AUDIT_WAIT = "等待";

	// 裁判状态
	public static final String REFEREE_STATE_SPARE = "空闲";
	public static final String REFEREE_STATE_GAME = "比赛";
	public static final String REFEREE_STATE_LEAVE = "请假";
	// 裁判审核
	public static final String REFEREE_AUDIT_SUCCESS = "成功";
	public static final String REFEREE_AUDIT_FAILURE = "失败";
	public static final String REFEREE_AUDIT_WAIT = "等待";

	// 场地状态
	public static final String GROUND_STATE_SPARE = "空闲";
	public static final String GROUND_STATE_GAME = "使用";
	public static final String GROUND_STATE_LEAVE = "维护";
	// 场地审核
	public static final String GROUND_AUDIT_SUCCESS = "成功";
	public static final String GROUND_AUDIT_FAILURE = "失败";
	public static final String GROUND_AUDIT_WAIT = "等待";

	// 器材审核
	public static final String MATERIAL_AUDIT_SUCCESS = "成功";
	public static final String MATERIAL_AUDIT_FAILURE = "失败";
	public static final String MATERIAL_AUDIT_WAIT = "等待";

	// 预约状态
	public static final String BOOK_STATE_SPARE = "成功";
	public static final String BOOK_STATE_GAME = "失败";
	public static final String BOOK_STATE_CANCEL = "取消";
	// 预约审核
	public static final String BOOK_AUDIT_SUCCESS = "成功";
	public static final String BOOK_AUDIT_FAILURE = "失败";
	public static final String BOOK_AUDIT_WAIT = "等待";

	// 订单支付状态
	public static final String BOOK_STATE_PAID = "已支付";
	public static final String BOOK_STATE_UNPAID = "待支付";


	public enum RoleType{
		initial("内置角色",1),
		custom("自定义角色",2),
		business("业务角色",3);
		
		private String name;
		private int value;
		
		public String getName() {
			return name;
		}
		public int getValue() {
			return value;
		}

		private RoleType(String name, int value) {
			this.name = name;
			this.value = value;
		}
	}
	
	public enum ResourceType{
		module("模块",1),
		column("栏目",2),
		button("按钮",3);
		
		private String name;
		private int value;
		public String getName() {
			return name;
		}
		public int getValue() {
			return value;
		}
		private ResourceType(String name, int value) {
			this.name = name;
			this.value = value;
		}
		
	}


}
