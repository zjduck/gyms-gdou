package com.hlhome.cllz.gyms.util;

/**
 * Created by lingb on 2018/5/28
 */
public class KeyGenerator {

    // 使用单例模式，不允许直接创建实例
    private KeyGenerator() {}

    // 创建一个空实例对象，类需要用的时候才赋值
    private static KeyGenerator keyGenerator = null;

    // 单例模式--懒汉模式
    public static synchronized KeyGenerator getInstance() {
        if (keyGenerator == null) {
            keyGenerator = new KeyGenerator();
        }
        return keyGenerator;
    }

    /**
     * 赛事、裁判、场地、器材编号
     * 1、通过类型获取数据库最新记录
     * 2、将最新记录编号传参
     * 3、
     *
     * @return  java.lang.String
     */
    public synchronized String generateNum(String id, String typeStr) {


        // 获取id 中module 编号
        String moduleNum = id.substring(0,1);

        // 获取id 中 类型编号
        String typeNum = id.substring(1, 3);

        // 获取 id 中递增数，最多生成多少位(进位的值)
        String total = String.valueOf(id.substring(3,6));

        // 自增数
        int count = Integer.parseInt(total);
        // 匹配类型，则递增数递增
        if (typeNum.equals(typeStr)) {
            count++;

        } else {

            return "匹配类型失败！";
        }

        // 算补位
        int countInteger = total.length() - String.valueOf(count).length();
        System.out.println(total.length());
        System.out.println(String.valueOf(count).length());

        // 补字符串
        String bu = "";
        for (int i = 0; i < countInteger; i++) {
            bu += "0";
        }
        bu += String.valueOf(count);
        System.out.println(bu);

        int total1 = Integer.parseInt(total);
        if (count >= total1) {
            count = 0;
        }
        return moduleNum + typeNum + bu;
    }

    /**
     * 预约、订单编号
     *
     * @param   [id]
     * @return  java.lang.String
     */
    public synchronized String generateNum(String id) {

        // 获取当天日期（8位年月日），比如：20180623
        String now = DateUtils.getNowDate();

        // 获取id 中module 编号
        String moduleNum = id.substring(0,1);

        // 获取id 中日期（年月日) 20180623
        String dateStr = id.substring(1, 9);

        // 获取 id 中递增数，最多生成多少位(进位的值)
        String total = String.valueOf(id.substring(9,12));

        // 自增数
        int count = Integer.parseInt(total);
        // 匹配当天，则编号递增
        if (now.equals(dateStr)) {
            count++;
            now = dateStr;

        // 否则，当天第一个编号
        } else {
            count = 1;
        }
        // 算补位
        int countInteger = total.length() - String.valueOf(count).length();
        System.out.println(total.length());
        System.out.println(String.valueOf(count).length());

        // 补字符串
        String bu = "";
        for (int i = 0; i < countInteger; i++) {
            bu += "0";
        }
        bu += String.valueOf(count);
        System.out.println(bu);

        int total1 = Integer.parseInt(total);
        if (count >= total1) {
            count = 0;
        }
        return moduleNum + dateStr + bu;

    }

    public static void main(String[] args) {

        // 系统初始化都没有数据时，
        for (int i = 0; i < 1; i++) {
            System.out.println(KeyGenerator.getInstance().generateNum("B20180623001"));

        }
    }

    
}
