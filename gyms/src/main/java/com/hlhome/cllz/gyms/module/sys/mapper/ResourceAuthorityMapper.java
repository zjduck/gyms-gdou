package com.hlhome.cllz.gyms.module.sys.mapper;

import com.hlhome.cllz.gyms.module.sys.domain.ResourceAuthority;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ResourceAuthorityMapper {
    int deleteByPrimaryKey(String id);

    int insert(ResourceAuthority record);

    ResourceAuthority selectByPrimaryKey(String id);

    List<ResourceAuthority> selectAll();

    int updateByPrimaryKey(ResourceAuthority record);
}