package com.hlhome.cllz.gyms.module.sys.web;

import com.hlhome.cllz.gyms.module.sys.domain.Authority;
import com.hlhome.cllz.gyms.module.sys.domain.AuthoritySaveVM;
import com.hlhome.cllz.gyms.module.sys.domain.AuthorityUpdateVM;
import com.hlhome.cllz.gyms.module.sys.domain.AuthorityVMS;
import com.hlhome.cllz.gyms.module.sys.service.impl.AuthorityService;
import com.hlhome.cllz.gyms.util.Constants;
import com.hlhome.cllz.gyms.util.Message;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class AuthorityController {
	
	@Autowired
	AuthorityService authorityService;


	@RequiresPermissions(value={"authority_getAuthority","administrator"},logical= Logical.OR)
	@RequestMapping(value="/authority",method=RequestMethod.GET)
	public Message<List<AuthorityVMS>> getAuthority(){
		List<AuthorityVMS> list=authorityService.getAuthority();
		return Message.ok(list);
	}
	

	@RequiresPermissions(value={"authority_getLikeAuthority","administrator"},logical= Logical.OR)
	@RequestMapping(value="/getLikeAuthority",method=RequestMethod.GET)
	public Message<List<Authority>> getLikeAuthority(@RequestParam("method")String method, @RequestParam("word")String word){
		List<Authority> list=authorityService.getLikeAuthority(method,word);
		return Message.ok(list);
	}
	

	@RequiresPermissions(value={"authority_saveAuthority","administrator"},logical= Logical.OR)
	@RequestMapping(value="/authority",method=RequestMethod.POST)
	public Message<String> saveAuthority(@RequestBody AuthoritySaveVM authoritySaveVM){
		authorityService.saveAuthority(authoritySaveVM);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"authority_updateAuhtority","administrator"},logical= Logical.OR)
	@RequestMapping(value="/authority",method=RequestMethod.PUT)
	public Message<String> updateAuhtority(@RequestBody AuthorityUpdateVM authorityUpdateVM){
		authorityService.updateAuhtority(authorityUpdateVM);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"authority_deleteAuthority","administrator"},logical= Logical.OR)
	@RequestMapping(value="/authority/{id}",method=RequestMethod.DELETE)
	public Message<String> deleteAuthority(@PathVariable("id")String id){
		authorityService.deleteAuthority(id);
		return Message.ok(Constants.SUCCESS);
	}
}
