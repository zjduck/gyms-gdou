package com.hlhome.cllz.gyms.module.game.domain;

/**
 * Created by lingb on 2018/6/25
 */
public class GameInfoVO {

    private String gameId;
    private String bookId;
    private String gameName;
    private String gameIntroduction;
    private String gameSponsor;
    private String gameTime;
    private String periods;
    private String gameType;
    private String gameState;
    private String auditState;
    private String remark;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameIntroduction() {
        return gameIntroduction;
    }

    public void setGameIntroduction(String gameIntroduction) {
        this.gameIntroduction = gameIntroduction;
    }

    public String getGameSponsor() {
        return gameSponsor;
    }

    public void setGameSponsor(String gameSponsor) {
        this.gameSponsor = gameSponsor;
    }

    public String getGameTime() {
        return gameTime;
    }

    public void setGameTime(String gameTime) {
        this.gameTime = gameTime;
    }

    public String getPeriods() {
        return periods;
    }

    public void setPeriods(String periods) {
        this.periods = periods;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getGameState() {
        return gameState;
    }

    public void setGameState(String gameState) {
        this.gameState = gameState;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return "GameInfoVO{" +
                "gameId='" + gameId + '\'' +
                ", bookId='" + bookId + '\'' +
                ", gameName='" + gameName + '\'' +
                ", gameIntroduction='" + gameIntroduction + '\'' +
                ", gameSponsor='" + gameSponsor + '\'' +
                ", gameTime='" + gameTime + '\'' +
                ", gameType='" + gameType + '\'' +
                ", gameState='" + gameState + '\'' +
                ", auditState='" + auditState + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
