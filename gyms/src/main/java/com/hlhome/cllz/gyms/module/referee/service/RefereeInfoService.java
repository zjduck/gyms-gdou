package com.hlhome.cllz.gyms.module.referee.service;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoDO;

import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
public interface RefereeInfoService {

    /**
     * 添加裁判信息
     *
     * @param refereeInfoVO signification
     * @return boolean
     */
    boolean addReferee(RefereeInfoVO refereeInfoVO);

    /**
     * 删除裁判信息
     *
     * @param refereeId signification
     * @return boolean
     */
    boolean removeReferee(String refereeId);

    /**
     * 更新裁判信息
     *
     * @param refereeInfoVO signification
     * @return boolean
     */
    boolean updateReferee(RefereeInfoVO refereeInfoVO);

    /**
     * 通过裁判Id查找裁判信息
     *
     * @param refereeId signification
     * @return RefereeInfoVO
     */
    RefereeInfoVO getByRefereeId(String refereeId);

    /**
     * 通过裁判Name查找裁判信息
     *
     * @param refereeName signification
     * @return RefereeInfoVO
     */
    RefereeInfoVO getByRefereeName(String refereeName);

    /**
     * 统计所有裁判信息
     *
     * @param
     * @return RefereeInfoVO
     */
    List<RefereeInfoVO> listAll();

    /**
     * 通过裁判类型查找最新裁判信息记录
     *
     * @param refereeItem
     * @return RefereeInfoVO
     */
    RefereeInfoDO getLatestByRefereeItem(String refereeItem);

    /**
     * 统计所有空闲的裁判信息
     *
     * @param
     * @return RefereeInfoVO
     */
    List<RefereeInfoVO> listAllFreeReferee();

    /**
     * 统计所有比赛的裁判信息
     *
     * @param
     * @return RefereeInfoVO
     */
    List<RefereeInfoVO> listAllBusyReferee();
}
