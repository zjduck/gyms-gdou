package com.hlhome.cllz.gyms.bean;

import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoVO;
import com.hlhome.cllz.gyms.module.ground.service.GroundInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

import static com.hlhome.cllz.gyms.util.Constants.GROUND_STATE_SPARE;

/**
 * 场地、裁判“恢复”任务
 *
 * Created by lingb on 2018/7/1
 */
@Configuration
@Component // 该注解必须要加
@EnableScheduling // 该注解必须要加
public class GroundScheduleJob {

    @Autowired
    private GroundInfoService groundInfoService;

    /**
     * "恢复" 场地状态
     *
     * @param   []
     * @return  boolean
     */
    public void resetGround() {
       System.err.println("GroundScheduleJob开始定时执行-------------------------------------------------------" + LocalDateTime.now().withNano(0));
       List<GroundInfoVO> groundInfoVOList = groundInfoService.listUse();
       for (GroundInfoVO groundInfoVO : groundInfoVOList) {
           // 状态 “空闲”
           groundInfoVO.setGroundState(GROUND_STATE_SPARE);
           groundInfoService.updateGround(groundInfoVO);

       }
    }

    public void scheduleTest() {
        System.out.println(LocalDateTime.now().withNano(0));
        System.err.println("scheduleTest开始定时执行");
    }

}
