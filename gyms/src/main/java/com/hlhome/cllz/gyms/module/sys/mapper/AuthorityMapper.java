package com.hlhome.cllz.gyms.module.sys.mapper;

import com.hlhome.cllz.gyms.module.sys.domain.Authority;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AuthorityMapper {
    int deleteByPrimaryKey(String id);

    int insert(Authority record);

    Authority selectByPrimaryKey(String id);

    List<Authority> selectAll();

    int updateByPrimaryKey(Authority record);
}