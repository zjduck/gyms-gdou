package com.hlhome.cllz.gyms.module.game.service;

import com.hlhome.cllz.gyms.module.game.domain.GameInfoDO;
import com.hlhome.cllz.gyms.module.game.domain.GameInfoVO;

import java.util.List;

/**
 * Created by lingb on 2018/6/22
 */
public interface GameInfoService {

    /**
     * 统计所有赛事
     *
     * @param   []
     * @return  java.util.List<com.hlhome.cll.gyms.module.game.domain.GameInfoVO>
     */
    List<GameInfoVO> listAll();

    /**
     * 通过赛事Id获取赛事
     * @param   [gameId]
     * @return  com.hlhome.cll.gyms.module.game.domain.GameInfoVO
     */
    GameInfoVO getByGameId(String gameId);


    /**
     * 通过赛事名获取赛事
     * @param   [gameName]
     * @return  com.hlhome.cll.gyms.module.game.domain.GameInfoVO
     */
    GameInfoVO getByGameName(String gameName);

    /**
     * 通过预约Id获取赛事
     * @param   [bookId]
     * @return  com.hlhome.cll.gyms.module.game.domain.GameInfoVO
     */
    GameInfoVO getByBookId(String bookId);

    /**
     * 通过用户Id获取赛事
     * @param   [userId]
     * @return  com.hlhome.cll.gyms.module.game.domain.GameInfoVO
     */
    List<GameInfoVO> listByUserId();

    /**
     * 添加赛事
     *
     * @param   [gameVO]
     * @return  int
     */
    boolean saveGameInfo(GameInfoVO gameInfoVO);

    /**
     * 更新赛事
     * @param   [gameVO]
     * @return  java.lang.Boolean
     */
    boolean updateGameInfo(GameInfoVO gameInfoVO);

    /**
     * 删除赛事
     * @param   [gameId]
     * @return  java.lang.Boolean
     */
    boolean removeByGameId(String gameId);

    /**
     * 通过赛事类型 获取最新的赛事记录
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.module.game.domain.GameInfoDO
     */
    GameInfoDO getLatestByGameType(String gameType);
}
