package com.hlhome.cllz.gyms.module.sys.mapper;

import com.hlhome.cllz.gyms.module.sys.domain.RoleBusiness;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleBusinessMapper {
    int deleteByPrimaryKey(String id);

    int insert(RoleBusiness record);

    RoleBusiness selectByPrimaryKey(String id);

    List<RoleBusiness> selectAll();

    int updateByPrimaryKey(RoleBusiness record);
}