package com.hlhome.cllz.gyms.module.ground.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 信息+租借+类型 VO类
 *
 * Created by luckyTsai on 2018/6/27
 */
public class GroundInfoRentTypeVO implements Serializable {

    private String groundId;

    private String groundName;

    private String groundLocation;

    private String groundType;

    private Integer groundSize;

    private Integer groundCapacity;

    private BigDecimal rent;

    private String groundState;

    private String remark;

    private String bookId;

    private String rentTime;

    private String auditState;

    private String typeId;

    private String typeName;

    public String getGroundId() {
        return groundId;
    }

    public void setGroundId(String groundId) {
        this.groundId = groundId;
    }

    public String getGroundName() {
        return groundName;
    }

    public void setGroundName(String groundName) {
        this.groundName = groundName;
    }

    public String getGroundLocation() {
        return groundLocation;
    }

    public void setGroundLocation(String groundLocation) {
        this.groundLocation = groundLocation;
    }

    public String getGroundType() {
        return groundType;
    }

    public void setGroundType(String groundType) {
        this.groundType = groundType;
    }

    public Integer getGroundSize() {
        return groundSize;
    }

    public void setGroundSize(Integer groundSize) {
        this.groundSize = groundSize;
    }

    public Integer getGroundCapacity() {
        return groundCapacity;
    }

    public void setGroundCapacity(Integer groundCapacity) {
        this.groundCapacity = groundCapacity;
    }

    public BigDecimal getRent() {
        return rent;
    }

    public void setRent(BigDecimal rent) {
        this.rent = rent;
    }

    public String getGroundState() {
        return groundState;
    }

    public void setGroundState(String groundState) {
        this.groundState = groundState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getRentTime() {
        return rentTime;
    }

    public void setRentTime(String rentTime) {
        this.rentTime = rentTime;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "GroundInfoRentTypeVO{" +
                "groundId='" + groundId + '\'' +
                ", groundName='" + groundName + '\'' +
                ", groundLocation='" + groundLocation + '\'' +
                ", groundType='" + groundType + '\'' +
                ", groundSize=" + groundSize +
                ", groundCapacity=" + groundCapacity +
                ", rent=" + rent +
                ", groundState='" + groundState + '\'' +
                ", remark='" + remark + '\'' +
                ", bookId='" + bookId + '\'' +
                ", rentTime='" + rentTime + '\'' +
                ", auditState='" + auditState + '\'' +
                ", typeId='" + typeId + '\'' +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
