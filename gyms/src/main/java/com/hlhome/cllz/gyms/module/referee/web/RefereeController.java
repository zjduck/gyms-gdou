package com.hlhome.cllz.gyms.module.referee.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeItemVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentVO;
import com.hlhome.cllz.gyms.module.referee.service.RefereeInfoService;
import com.hlhome.cllz.gyms.module.referee.service.RefereeItemService;
import com.hlhome.cllz.gyms.module.referee.service.RefereeRentService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/24
 */
@RequestMapping("/api/v1")
@RestController
public class RefereeController {

    private static final Logger logger = LoggerFactory.getLogger(RefereeController.class);

    @Autowired
    private RefereeInfoService refereeInfoService;
    @Autowired
    private RefereeItemService refereeItemService;
    @Autowired
    private RefereeRentService refereeRentService;

   /**
     * 添加裁判信息
     * 需要 referee_addRefereeInfo权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */

    //@RequiresPermissions(value={"referee_addRefereeInfo", "admin"}, logical = Logical.OR)
    @PostMapping("/referee/info")
    public WebResult addRefereeInfo(@RequestBody RefereeInfoVO refereeInfoVO) {
        boolean flag = refereeInfoService.addReferee(refereeInfoVO);
        if (flag) {
            return WebResult.success(flag,"添加裁判信息成功！");

        } else {
            return WebResult.serverError("添加裁判信息失败！");
        }
    }

    /**
     * 删除裁判信息
     * 需要 referee_removeRefereeInfo权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_removeRefereeInfo", "admin"}, logical = Logical.OR)
    @DeleteMapping("/referee/info/{refereeId}")
    public WebResult removeRefereeInfo(@PathVariable("refereeId") String refereeId) {
        if(refereeId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = refereeInfoService.removeReferee(refereeId);
        if(flag){
            return WebResult.success(flag,"删除裁判信息成功");

        } else {
            return WebResult.serverError("删除裁判信息失败！");
        }
    }

    /**
     * 更新裁判信息
     * 需要 referee_updateRefereeInfo权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_updateRefereeInfo", "admin"}, logical = Logical.OR)
    @PatchMapping("/referee/info")
    public WebResult updateRefereeInfo(@RequestBody RefereeInfoVO refereeInfoVO) {
        boolean flag = refereeInfoService.updateReferee(refereeInfoVO);
        if (flag) {
            return WebResult.success(flag,"更新裁判信息成功！");

        } else {
            return WebResult.serverError("更新裁判信息失败！");
        }
    }

    /**
     * 通过裁判Id查找裁判信息
     * 需要 referee_getByRefereeId权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_getByRefereeId", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/info/{refereeId}")
    public WebResult getByRefereeId(@PathVariable String refereeId) {
        RefereeInfoVO refereeInfoVO = refereeInfoService.getByRefereeId(refereeId);
        if (refereeInfoVO != null) {
            return WebResult.success(refereeInfoVO, "查找裁判信息成功！");

        } else {
            return WebResult.serverError("查找裁判信息失败！");
        }
    }

    /**
     * 查找所有裁判信息
     * 需要 referee_getAllRefereeInfo权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_getAllRefereeInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/info")
    public WebResult listAllRefereeInfo() {
        List<RefereeInfoVO> refereeInfoVOList = refereeInfoService.listAll();
        if (refereeInfoVOList != null) {
            return WebResult.success(refereeInfoVOList, "查找裁判信息成功！");

        } else {
            return WebResult.validateError("查找裁判信息失败！");
        }
    }

    /**
     * 查找所有空闲的裁判信息
     * 需要 referee_getAllFreeReferee权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_getAllFreeReferee", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/info/freeReferee")
    public WebResult listAllFreeReferee() {
        List<RefereeInfoVO> refereeInfoVOList = refereeInfoService.listAllFreeReferee();
        if (refereeInfoVOList != null) {
            return WebResult.success(refereeInfoVOList, "查找空闲的裁判信息成功！");

        } else {
            return WebResult.validateError("查找空闲的裁判信息失败！");
        }
    }

    /**
     * 查找所有比赛的裁判信息
     * 需要 referee_getAllBusyReferee权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_getAllBusyReferee", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/info/busyReferee")
    public WebResult listAllBusyReferee() {
        List<RefereeInfoVO> refereeInfoVOList = refereeInfoService.listAllBusyReferee();
        if (refereeInfoVOList != null) {
            return WebResult.success(refereeInfoVOList, "查找比赛的裁判信息成功！");

        } else {
            return WebResult.validateError("查找比赛的裁判信息失败！");
        }
    }

    /**
     * 添加裁判项目
     * 需要 referee_addRefereeItem权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */

    //@RequiresPermissions(value={"referee_addRefereeItem", "admin"}, logical = Logical.OR)
    @PostMapping("/referee/item")
    public WebResult addRefereeItem(@RequestBody RefereeItemVO refereeItemVO) {
        boolean flag = refereeItemService.addItem(refereeItemVO);
        if (flag) {
            return WebResult.success(flag,"添加裁判项目成功！");

        } else {
            return WebResult.serverError("添加裁判项目失败！");
        }
    }

    /**
     * 删除裁判项目
     * 需要 referee_removeRefereeItem权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_removeRefereeItem", "admin"}, logical = Logical.OR)
    @DeleteMapping("/referee/item/{itemId}")
    public WebResult removeRefereeItem(@PathVariable("itemId") String itemId) {
        if(itemId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = refereeItemService.removeItem(itemId);
        if(flag){
            return WebResult.success(flag,"删除裁判项目成功");

        } else {
            return WebResult.serverError("删除裁判项目失败！");
        }
    }

    /**
     * 更新裁判项目
     * 需要 referee_updateRefereeItem权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_updateRefereeItem", "admin"}, logical = Logical.OR)
    @PatchMapping("/referee/item")
    public WebResult updateRefereeItem(@RequestBody RefereeItemVO refereeItemVO) {
        boolean flag = refereeItemService.updateItem(refereeItemVO);
        if (flag) {
            return WebResult.success(flag,"更新裁判项目成功！");

        } else {
            return WebResult.serverError("更新裁判项目失败！");
        }
    }

    /**
     * 通过项目Id查找裁判项目
     * 需要 referee_getByItemId权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_getByItemId", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/item/{itemId}")
    public WebResult getByItemId(@PathVariable String itemId) {
        RefereeItemVO refereeItemVO = refereeItemService.getByItemId(itemId);
        if (refereeItemVO != null) {
            return WebResult.success(refereeItemVO, "查找裁判项目成功！");

        } else {
            return WebResult.serverError("查找裁判项目失败！");
        }
    }

    /**
     * 查找所有裁判项目
     * 需要 referee_getAllRefereeItem权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    //@RequiresPermissions(value={"referee_getAllRefereeItem", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/item")
    public WebResult listAllRefereeItem() {
        List<RefereeItemVO> refereeItemVOList = refereeItemService.listAll();
        if (refereeItemVOList != null) {
            return WebResult.success(refereeItemVOList, "查找裁判项目成功！");

        } else {
            return WebResult.validateError("查找裁判项目失败！");
        }
    }

    /**
     * 添加裁判租借
     * 需要 referee_addRefereeRent权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_addRefereeRent", "admin"}, logical = Logical.OR)
    @PostMapping("/referee/rent")
    public WebResult addRefereeRent(@RequestBody RefereeRentVO refereeRentVO) {
        boolean flag = refereeRentService.addRent(refereeRentVO);
        if (flag) {
            return WebResult.success(flag,"添加裁判租借成功！");

        } else {
            return WebResult.serverError("添加裁判租借失败！");
        }
    }

    /**
     * 删除裁判租借
     * 需要 referee_removeRefereeRent权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_removeRefereeRent", "admin"}, logical = Logical.OR)
    @DeleteMapping("/referee/rent/{refereeId}")
    public WebResult removeRefereeRent(@PathVariable("refereeId") String refereeId) {
        if(refereeId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = refereeRentService.removeRent(refereeId);
        if(flag){
            return WebResult.success(flag,"删除裁判租借成功");

        } else {
            return WebResult.serverError("删除裁判租借失败！");
        }
    }

    /**
     * 更新裁判租借
     * 需要 referee_updateRefereeRent权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_updateRefereeRent", "admin"}, logical = Logical.OR)
    @PatchMapping("/referee/rent")
    public WebResult updateRefereeRent(@RequestBody RefereeRentVO refereeRentVO) {
        boolean flag = refereeRentService.updateRent(refereeRentVO);
        if (flag) {
            return WebResult.success(flag,"更新裁判租借成功！");

        } else {
            return WebResult.serverError("更新裁判租借失败！");
        }
    }

    /**
     * 通过预约Id查找裁判租借
     * 需要 referee_getByBookId权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_getByBookId", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/rent/{bookId}")
    public WebResult getByBookId(@PathVariable("bookId") String bookId) {
        List<RefereeRentVO> refereeRentVOList = refereeRentService.listByBookId(bookId);
        if (refereeRentVOList != null) {
            return WebResult.success(refereeRentVOList, "查找裁判租借成功！");

        } else {
            return WebResult.serverError("查找裁判租借失败！");
        }
    }

    /**
     * 查找所有裁判租借
     * 需要 referee_getAllRefereeRent权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_getAllRefereeRent", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/rent")
    public WebResult listAllRefereeRent() {
        List<RefereeRentVO> refereeRentVOList = refereeRentService.listAll();
        if (refereeRentVOList != null) {
            return WebResult.success(refereeRentVOList, "查找裁判租借成功！");

        } else {
            return WebResult.validateError("查找裁判租借失败！");
        }
    }

    /**
     * 审核裁判租借
     * 需要 referee_checkRefereeRent权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_checkRefereeRent", "admin"}, logical = Logical.OR)
    @PutMapping("/referee/rent/check")
    public WebResult checkRefereeRent(@RequestBody RefereeRentVO refereeRentVO) {
        boolean flag = refereeRentService.checkRent(refereeRentVO);
        if (flag) {
            return WebResult.success(flag,"审核裁判租借成功！");

        } else {
            return WebResult.serverError("审核裁判租借失败！");
        }
    }

    /**
     * 通过裁判编号和 日期 获取租借裁判列表
     * 需要 referee_listRefereeRentDate权限或管理员权限
     *
     * @param
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"referee_listRefereeRentDate", "admin"}, logical = Logical.OR)
    @GetMapping("/referee/rent/date")
    public WebResult listRefereeRentDate(@RequestParam("refereeId") String refereeId,
                                         @RequestParam("date") String date) {
        List<RefereeRentVO> refereeRentVOList = refereeRentService.listByRefereeIdAndDate(refereeId, date);
        if (refereeRentVOList != null) {
            return WebResult.success(refereeRentVOList, "查找裁判租借成功！");

        } else {
            return WebResult.validateError("查找裁判租借失败！");
        }
    }
}