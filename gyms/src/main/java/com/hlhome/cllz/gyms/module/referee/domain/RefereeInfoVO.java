package com.hlhome.cllz.gyms.module.referee.domain;

import java.math.BigDecimal;

/**
 * 裁判信息VO类
 *
 * @author LoongMH
 * @date 2018/06/23
 */
public class RefereeInfoVO {

    private String refereeId;
    private String refereeName;
    private String refereeSex;
    private String refereeAge;
    private String refereeItem;
    private BigDecimal chargeStandard;
    private String refereeState;
    private String phone;
    private String remark;

    public String getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(String refereeId) {
        this.refereeId = refereeId;
    }

    public String getRefereeName() {
        return refereeName;
    }

    public void setRefereeName(String refereeName) {
        this.refereeName = refereeName;
    }

    public String getRefereeSex() {
        return refereeSex;
    }

    public void setRefereeSex(String refereeSex) {
        this.refereeSex = refereeSex;
    }

    public String getRefereeAge() {
        return refereeAge;
    }

    public void setRefereeAge(String refereeAge) {
        this.refereeAge = refereeAge;
    }

    public String getRefereeItem() {
        return refereeItem;
    }

    public void setRefereeItem(String refereeItem) {
        this.refereeItem = refereeItem;
    }

    public BigDecimal getChargeStandard() {
        return chargeStandard;
    }

    public void setChargeStandard(BigDecimal chargeStandard) {
        this.chargeStandard = chargeStandard;
    }

    public String getRefereeState() {
        return refereeState;
    }

    public void setRefereeState(String refereeState) {
        this.refereeState = refereeState;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "RefereeInfoVO{" +
                "refereeId='" + refereeId + '\'' +
                ", refereeName='" + refereeName + '\'' +
                ", refereeSex='" + refereeSex + '\'' +
                ", refereeAge='" + refereeAge + '\'' +
                ", refereeItem='" + refereeItem + '\'' +
                ", chargeStandard='" + chargeStandard + '\'' +
                ", refereeState='" + refereeState + '\'' +
                ", phone='" + phone + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}