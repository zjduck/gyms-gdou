package com.hlhome.cllz.gyms.module.user.mapper;

import com.hlhome.cllz.gyms.module.user.domain.UserRoleR;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserRoleRMapper {
    int insert(UserRoleR record);

    List<UserRoleR> selectAll();
}