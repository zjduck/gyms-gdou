package com.hlhome.cllz.gyms.module.book.service;

import com.hlhome.cllz.gyms.module.book.domain.BookInfoDO;
import com.hlhome.cllz.gyms.module.book.domain.BookInfoVO;
import com.hlhome.cllz.gyms.module.book.mapper.BookInfoMapper;
import com.hlhome.cllz.gyms.module.game.service.GameInfoService;
import com.hlhome.cllz.gyms.module.ground.service.GroundInfoService;
import com.hlhome.cllz.gyms.module.ground.service.GroundRentService;
import com.hlhome.cllz.gyms.module.material.service.MaterialInfoService;
import com.hlhome.cllz.gyms.module.material.service.MaterialRentService;
import com.hlhome.cllz.gyms.module.referee.service.RefereeInfoService;
import com.hlhome.cllz.gyms.module.referee.service.RefereeRentService;
import com.hlhome.cllz.gyms.util.POJOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by lingb on 2018/6/24
 */
@Service
@Transactional
public class BookInfoServiceImpl implements BookInfoService {

    private static final Logger logger = LoggerFactory.getLogger(BookInfoServiceImpl.class);

    @Autowired
    private BookInfoMapper bookInfoMapper;

    @Autowired
    private GameInfoService gameInfoService;

    @Autowired
    private RefereeInfoService refereeInfoService;
    @Autowired
    private RefereeRentService refereeRentService;

    @Autowired
    private GroundInfoService groundInfoService;
    @Autowired
    private GroundRentService groundRentService;

    @Autowired
    private MaterialInfoService materialInfoService;
    @Autowired
    private MaterialRentService materialRentService;
/*

   */
/**
     * 新增预约
     * 1、获取用户编号、预约编号（统一）
     * 2、预约VO -> 预约DO，并进行【新增预约】操作
     * 3、拆包操作：将BookVO 中 GameInfoVO、RefereeInfoVO、GroundInfoVO 和 MaterialInfoVO 分开
     * 4、将3、获取到 GameInfoVO、RefereeInfoVO、GroundInfoVO 和 MaterialInfoVO 分别进行【新增租借】操作：
     *      i、赛事新增
     *      ii、裁判更新 裁判状态 -> 比赛
     *      iii、租借新增
     *      其他模块同理即可
     *
     * @param   [bookVO]
     * @return  void
     *//*

    @Override
    public void saveBookInfo(BookInfoVO bookInfoVO) {
        Asserts.validate(bookInfoVO,"bookInfoVO");

        // 1、获取用户id
        UserVO uservo = (UserVO) SecurityUtils.getSubject().getPrincipal();
        String userId = uservo.getUserId();

        // 获取预约id
        // 获取最新一条预约记录
        BookInfoDO latestBook = getLatestOrderById();
        String bookId = "";
        // 若为空，数据库首条预约记录
        if (latestBook == null) {
            // G + 20180703 + 001
            bookId = Constants.GROUND + DateUtils.getNowDate() + "001";

            // 否则，预约记录递增（自动按日期走）
        } else {
            // G + ? + ?
            bookId = KeyGenerator.getInstance().generateNum(latestBook.getBookId());

        }

        GroundRentVO groundRentVO = new GroundRentVO();
        groundRentVO.setBookId(bookId);

        BookInfoDO bookInfoDO = new BookInfoDO();
        bookInfoDO = POJOUtils.VO2DO(bookInfoVO, BookInfoDO.class);
        bookInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        bookInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        bookInfoDO.setBookId(bookId);
        bookInfoMapper.insert(bookInfoDO);

        // 2、拆包
        GameInfoVO> gameVOList = bookInfoVO.getGameInfoVOList();
        List<RefereeInfoVO> refereeInfoVOList = bookInfoVO.getRefereeInfoVOList();
        List<GroundInfoVO> groundInfoVOList = bookInfoVO.getGroundInfoVOList();
        List<MaterialInfoVO> materialInfoVOList = bookInfoVO.getMaterialInfoVOList();

        // 3、分别进行【新增租借】操作
        if (!gameVOList.isEmpty()) {
            for (GameInfoVO gameVO : gameVOList) {
                // 新增赛事
                gameInfoService.saveGameInfo(gameVO);
            }

        }

        if (!refereeInfoVOList.isEmpty()) {
            for (RefereeInfoVO refereeInfoVO : refereeInfoVOList) {
                // 裁判状态 -> 比赛
                refereeInfoVO.setRefereeState(Constants.REFEREE_STATE_GAME);
                refereeInfoService.updateReferee(refereeInfoVO);

                // 新增裁判租借
                RefereeRentVO refereeRentVO = new RefereeRentVO();
                refereeRentVO.setRefereeId(refereeInfoVO.getRefereeId());
                refereeRentVO.setRefereeItem(refereeInfoVO.getRefereeItem());
                // 统一预约编号
                refereeRentVO.setBookId(bookId);
                refereeRentVO.setRentTime(bookInfoVO.getBookTime());
                refereeRentService.addRent(refereeRentVO);

            }
        }

        if (!groundInfoVOList.isEmpty()) {
            for (GroundInfoVO groundInfoVO : groundInfoVOList) {
                // 场地状态 -> 使用
                groundInfoVO.setGroundState(Constants.GROUND_STATE_GAME);
                groundInfoService.updateGround(groundInfoVO);

                // 新增场地租借
                GroundRentVO groundRentVO = new GroundRentVO();
                groundRentVO.setGroundId(groundInfoVO.getGroundId());
                // 统一预约编号
                groundRentVO.setBookId(bookId);
                groundRentVO.setRentTime(bookInfoVO.getBookTime());
                groundRentService.saveGroundRent(groundRentVO);

            }
        }

        if (!materialInfoVOList.isEmpty()) {
            for (MaterialInfoVO materialInfoVO : materialInfoVOList) {
                // 新库存 = 原库存 - 租借数
                int materialResidue = materialInfoVO.getMaterialResidue() - materialInfoVO.getMaterialNumber();
                materialInfoVO.setMaterialResidue(materialResidue);
                materialInfoService.updateMaterialInfo(materialInfoVO);

                // 新增器材租借
                MaterialRentVO materialRentVO = new MaterialRentVO();
                materialRentVO.setMaterialId(materialInfoVO.getMaterialId());
                // 统一预约编号
                materialRentVO.setBookId(bookId);
                materialRentVO.setRentNumber(materialInfoVO.getMaterialNumber());
                materialRentVO.setRentTime(bookInfoVO.getBookTime());
                materialRentService.saveMaterialRent(materialRentVO);

            }
        }

    }
*/

    @Override
    public boolean updateBookInfo(BookInfoVO bookInfoVO) {
        boolean flag = false;
//        Asserts.validate(bookInfoVO,"bookInfoVO");

        BookInfoDO bookInfoDO = POJOUtils.VO2DO(bookInfoVO, BookInfoDO.class);
        bookInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        int result = bookInfoMapper.update(bookInfoDO);
        flag = result == 0 ? false : true;
        return flag;

    }

    @Override
    public boolean removeBookInfo(String bookId) {
        boolean flag = false;
        int result = bookInfoMapper.deleteByBookId(bookId);
        flag = result == 0 ? false : true;
        return flag;
    }


  /*  @Override
    public BookInfoVO getByBookId(String bookId) {
        BookInfoDO bookInfoDO = bookInfoMapper.getByBookId(bookId);
        // 返回页面显示预约信息
        // 赛事
        List<GameInfoVO> gameVOList = gameInfoService.listByBookId(bookId);

        // 裁判
        List<RefereeRentVO> refereeRentVOList = refereeRentService.listByBookId(bookId);

        // 场地
        List<GroundRentVO> groundRentVOList = groundRentService.listByBookId(bookId);

        // 器材
        List<MaterialRentVO> materialRentVOList = materialRentService.listByBookId(bookId);

        // DO -> VO
        BookInfoVO bookInfoVO = POJOUtils.DO2VO(bookInfoDO, BookInfoVO.class);
//        bookInfoVO.setGameInfoVOList(gameVOList);
        bookInfoVO.setRefereeRentVOList(refereeRentVOList);
        bookInfoVO.setGroundRentVOList(groundRentVOList);
        bookInfoVO.setMaterialRentVOList(materialRentVOList);

        return bookInfoVO;
    }*/

    @Override
    public List<BookInfoVO> listAll() {
        List<BookInfoDO> bookInfoDOList = bookInfoMapper.listAll();
        return POJOUtils.DO2VOList(bookInfoDOList, BookInfoVO.class);
    }

    @Override
    public BookInfoDO getLatestOrderById() {
        return bookInfoMapper.getLatestOrderById();
    }


}
