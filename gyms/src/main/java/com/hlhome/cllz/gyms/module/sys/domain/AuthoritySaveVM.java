package com.hlhome.cllz.gyms.module.sys.domain;

import org.hibernate.validator.constraints.NotBlank;

public class AuthoritySaveVM {
	
	@NotBlank
    private String name;

	@NotBlank
    private String code;

	@NotBlank
    private String url;

	@NotBlank
    private String method;

	@NotBlank
    private String controller;

    private String des;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}
	
	
    
    
}
