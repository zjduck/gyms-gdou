package com.hlhome.cllz.gyms.module.referee.service;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentVO;
import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentDO;

import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
public interface RefereeRentService {

    /**
     * 添加裁判租借
     *
     * @param refereeRentVO signification
     * @return boolean
     */
    boolean addRent(RefereeRentVO refereeRentVO);

    /**
     * 删除裁判租借
     *
     * @param refereeId signification
     * @return boolean
     */
    boolean removeRent(String refereeId);

    /**
     * 更新裁判租借
     *
     * @param refereeRentVO signification
     * @return boolean
     */
    boolean updateRent(RefereeRentVO refereeRentVO);

    /**
     * 通过裁判Id查找裁判租借
     *
     * @param refereeId signification
     * @return RefereeRentVO
     */
    RefereeRentVO getByRefereeId(String refereeId);

    /**
     * 通过预约Id查找裁判租借
     *
     * @param bookId signification
     * @return RefereeRentVO
     */
    List<RefereeRentVO> listByBookId(String bookId);

    /**
     * 统计所有裁判租借
     *
     * @param
     * @return RefereeRentVO
     */
    List<RefereeRentVO> listAll();

    /**
     * 通过裁判类型查找最新裁判租借记录
     *
     * @param refereeItem
     * @return RefereeInfoVO
     */
    RefereeRentDO getLatestByRefereeItem(String refereeItem);

    /**
     * 审核裁判租借
     *
     * @param refereeRentVO signification
     * @return boolean
     */
    boolean checkRent(RefereeRentVO refereeRentVO);

    /**
     * 通过裁判编号和 日期 获取租借裁判列表
     * @param refereeId
     * @param date
     * @return
     */
    List<RefereeRentVO> listByRefereeIdAndDate(String refereeId, String date);
}
