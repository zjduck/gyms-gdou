package com.hlhome.cllz.gyms.module.order.service;

import com.hlhome.cllz.gyms.module.order.domain.OrderInfoDO;
import com.hlhome.cllz.gyms.module.order.domain.OrderInfoVO;

import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
public interface OrderInfoService {

    /**
     * 添加订单
     *
     * @param   [orderInfoVO]
     * @return  void
     */
    boolean saveOrderInfo(OrderInfoVO orderInfoVO);

    /**
     * 更新订单
     *
     * @param   [orderInfoVO]
     * @return  void
     */
    boolean updateOrderInfo(OrderInfoVO orderInfoVO);

    /**
     * 删除订单
     *
     * @param   [orderId]
     * @return  void
     */
    boolean removeOrderInfo(String orderId);

    /**
     * 获取单次订单
     *
     * @param   [orderId]
     * @return  com.hlhome.cllz.gyms.module.order.domain.OrderInfoVO
     */
    OrderInfoVO getByBookId(String orderId);

    /**
     * 统计所有订单
     *
     * @param   []
     * @return  java.util.List<com.hlhome.cllz.gyms.module.order.domain.OrderInfoVO>
     */
    List<OrderInfoVO> listAll();

    /**
     * 通过Id排序 获取最新的订单记录
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.module.order.domain.OrderInfoVO
     */
    OrderInfoDO getLatestOrderById();

}
