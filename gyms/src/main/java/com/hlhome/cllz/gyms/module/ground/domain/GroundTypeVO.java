package com.hlhome.cllz.gyms.module.ground.domain;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 场地类型VO类
 *
 * Created by luckyTsai on 2018/6/22
 */

public class GroundTypeVO implements Serializable {

    @NotBlank
    private String typeId;

    @NotBlank
    private String typeName;


    private String remark;





    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "GroundTypeVO{" +
                "typeId='" + typeId + '\'' +
                ", typeName='" + typeName + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
