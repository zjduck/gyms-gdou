package com.hlhome.cllz.gyms.module.game.service;

import com.hlhome.cllz.gyms.module.book.domain.BookInfoDO;
import com.hlhome.cllz.gyms.module.book.mapper.BookInfoMapper;
import com.hlhome.cllz.gyms.module.game.domain.GameInfoDO;
import com.hlhome.cllz.gyms.module.game.domain.GameInfoVO;
import com.hlhome.cllz.gyms.module.game.mapper.GameInfoMapper;
import com.hlhome.cllz.gyms.module.user.domain.User;
import com.hlhome.cllz.gyms.util.*;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by lingb on 2018/6/22
 */
@Service
@Transactional
public class GameInfoServiceImpl implements GameInfoService {

    private static final Logger logger = LoggerFactory.getLogger(GameInfoServiceImpl.class);

    @Autowired
    private BookInfoMapper bookInfoMapper;

    @Autowired
    private GameInfoMapper gameInfoMapper;

    @Override
    public List<GameInfoVO> listAll() {
        List<GameInfoDO> gameInfoDOList = gameInfoMapper.listAll();

        // 2、
        List<GameInfoVO> gameInfoVOList = POJOUtils.DO2VOList(gameInfoDOList, GameInfoVO.class);
/*
//        // 1、
//        for(GameInfoDO gameInfoDO : gameInfoDOList){
//            GameInfoVO gameInfoVO = POJOUtils.DO2VO(gameInfoDO, GameInfoVO.class);
//            gameInfoVOList.add(gameInfoVO);
//        }*/

        return gameInfoVOList;
    }

    @Override
    public GameInfoVO getByGameId(String gameId) {
        GameInfoDO gameInfoDO = gameInfoMapper.getByGameId(gameId);
        return POJOUtils.DO2VO(gameInfoDO, GameInfoVO.class);
    }

    @Override
    public GameInfoVO getByGameName(String gameName) {
        GameInfoDO gameInfoDO = gameInfoMapper.getByGameId(gameName);
        return POJOUtils.DO2VO(gameInfoDO, GameInfoVO.class);
    }

    @Override
    public GameInfoVO getByBookId(String bookId) {
        GameInfoDO gameInfoDO = gameInfoMapper.getByBookId(bookId);
        return POJOUtils.DO2VO(gameInfoDO, GameInfoVO.class);
    }

    @Override
    public List<GameInfoVO> listByUserId() {
        // 获取用户id
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        String userId = user.getId();

        List<GameInfoDO> gameInfoDOList = gameInfoMapper.listByUserId(userId);
        return POJOUtils.DO2VOList(gameInfoDOList, GameInfoVO.class);
    }

    /**
     * 新增（预约）赛事
     * 1、赛事信息表
     * 2、预约信息表
     *
     * @param   [gameInfoVO]
     * @return  boolean
     */
    @Override
    public  boolean saveGameInfo(GameInfoVO gameInfoVO) {
        boolean flag = false;
        Asserts.validate(gameInfoVO,"gameInfoVO");
        System.out.println(gameInfoVO);
        GameInfoDO gameInfoDO = new GameInfoDO();
        gameInfoDO = POJOUtils.VO2DO(gameInfoVO, GameInfoDO.class);
//        System.out.println(gameInfoDO);

        gameInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        gameInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
//        System.out.println(gameInfoVO.getGameType());

        String gameTypeNum = "";
        String gameId = "";
        GameInfoDO latestGame = getLatestByGameType(gameInfoVO.getGameType());
        // 获取赛事编号
        // 若为空，该赛事类型第一个
        if (latestGame == null) {
            gameTypeNum = SystemUtils.gameTypeBind(gameInfoVO.getGameType());
            gameId = Constants.GAME + gameTypeNum + "001";

            // 否则，该赛事类型递增
        } else {

            gameTypeNum = SystemUtils.gameTypeBind(gameInfoVO.getGameType());
            gameId = KeyGenerator.getInstance().generateNum(latestGame.getGameId(), gameTypeNum);

        }
        gameInfoDO.setGameId(gameId);

        // 获取预约编号
        // 获取最新一条预约记录
        BookInfoDO latestBook = bookInfoMapper.getLatestOrderById();
        String bookId = "";
        // 若为空，数据库首条预约记录
        if (latestBook == null) {
            bookId = Constants.BOOK + DateUtils.getNowDate() + "001";

            // 否则，预约记录递增（自动按日期走）
        } else {
            bookId = KeyGenerator.getInstance().generateNum(latestBook.getBookId());

        }
        gameInfoDO.setBookId(bookId);

        // 赛事信息表
        int result = gameInfoMapper.insert(gameInfoDO);
        flag = result == 0 ? false : true;


        // 获取用户id
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        String userId = user.getId();

        BookInfoDO bookInfoDO = new BookInfoDO();
        bookInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        bookInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        bookInfoDO.setBookId(bookId);
        bookInfoDO.setUserId(userId);
        bookInfoDO.setBookTime(gameInfoVO.getGameTime());
        // 预约信息表
        bookInfoMapper.insert(bookInfoDO);

        return flag;
    }

    @Override
    public boolean updateGameInfo(GameInfoVO gameInfoVO) {
        boolean flag = false;
        GameInfoDO gameInfoDO = POJOUtils.VO2DO(gameInfoVO, GameInfoDO.class);
        gameInfoDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = gameInfoMapper.update(gameInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public boolean removeByGameId(String gameId) {
        boolean flag = false;
        int result = gameInfoMapper.deleteByGameId(gameId);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public GameInfoDO getLatestByGameType(String gameType) {
        return gameInfoMapper.getLatestByGameType(gameType);
    }


}
