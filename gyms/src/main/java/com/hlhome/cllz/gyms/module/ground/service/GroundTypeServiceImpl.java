package com.hlhome.cllz.gyms.module.ground.service;

import com.hlhome.cllz.gyms.module.ground.domain.GroundTypeDO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundTypeVO;
import com.hlhome.cllz.gyms.module.ground.mapper.GroundTypeMapper;
import com.hlhome.cllz.gyms.util.Asserts;
import com.hlhome.cllz.gyms.util.POJOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luckyTsai on 2018/6/23
 */
@Service
@Transactional
public class GroundTypeServiceImpl implements GroundTypeService {

    private static final Logger logger = LoggerFactory.getLogger(GroundTypeServiceImpl.class);

    @Autowired
    private GroundTypeMapper groundTypeMapper;

    /**
     * 统计所有场地类型
     */
    @Override
    public List<GroundTypeVO> listAll() {
        List<GroundTypeVO> groundTypeVOList = new ArrayList<>();
        List<GroundTypeDO> groundTypeDOList = groundTypeMapper.listAll();
        for(GroundTypeDO groundTypeDO : groundTypeDOList){
            GroundTypeVO groundTypeVO = POJOUtils.DO2VO(groundTypeDO, GroundTypeVO.class);
            groundTypeVOList.add(groundTypeVO);
        }
        return groundTypeVOList;
    }

    /**
     * 通过场地类型id获取场地类型
     *
     * @param typeId
     */
    @Override
    public GroundTypeVO getByTypeId(String typeId) {
        GroundTypeDO groundTypeDO = groundTypeMapper.getByTypeId(typeId);
        return POJOUtils.DO2VO(groundTypeDO, GroundTypeVO.class);
    }

    /**
     * 通过场地类型名称获取场地类型
     *
     * @param typeName
     */
    @Override
    public GroundTypeVO getByTypeName(String typeName) {
        GroundTypeDO groundTypeDO = groundTypeMapper.getByTypeName(typeName);
        return POJOUtils.DO2VO(groundTypeDO, GroundTypeVO.class);
    }

    /**
     * 添加场地类型
     *
     * @param groundTypeVO
     */
    @Override
    public synchronized boolean saveGroundType(GroundTypeVO groundTypeVO) {
        boolean flag = false;
        Asserts.validate(groundTypeVO,"groundTypeVO");
        GroundTypeDO groundTypeDO = new GroundTypeDO();
        groundTypeDO = POJOUtils.VO2DO(groundTypeVO, GroundTypeDO.class);
        groundTypeDO.setGmtCreate(LocalDateTime.now().withNano(0));
        groundTypeDO.setGmtModified(LocalDateTime.now().withNano(0));


        int result = groundTypeMapper.insert(groundTypeDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 更新场地类型
     *
     * @param groundTypeVO
     */
    @Override
    public boolean updateGroundType(GroundTypeVO groundTypeVO) {
        boolean flag = false;
        GroundTypeDO groundTypeDO = POJOUtils.VO2DO(groundTypeVO, GroundTypeDO.class);
        groundTypeDO.setGmtModified(LocalDateTime.now().withNano(0));

        int result = groundTypeMapper.update(groundTypeDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    /**
     * 通过类型编号删除场地类型
     *
     * @param typeId
     */
    @Override
    public boolean removeByTypeId(String typeId) {
        boolean flag = false;
        int result = groundTypeMapper.deleteByTypeId(typeId);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public GroundTypeDO getLatestByGroundType(String groundType) {
        return null;
    }
}
