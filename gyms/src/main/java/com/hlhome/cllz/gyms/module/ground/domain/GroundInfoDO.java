package com.hlhome.cllz.gyms.module.ground.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 场地信息DO类
 *
 * Created by luckyTsai on 2018/6/22
 */

public class GroundInfoDO implements Serializable {

    private Integer id;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModified;
    private Integer isDeleted;

    private String groundId;
    private String groundName;
    private String groundLocation;
    private String groundType;
    private Integer groundSize;
    private Integer groundCapacity;
    private BigDecimal rent;
    private String groundState;
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getGroundId() {
        return groundId;
    }

    public void setGroundId(String groundId) {
        this.groundId = groundId;
    }

    public String getGroundName() {
        return groundName;
    }

    public void setGroundName(String groundName) {
        this.groundName = groundName;
    }

    public String getGroundLocation() {
        return groundLocation;
    }

    public void setGroundLocation(String groundLocation) {
        this.groundLocation = groundLocation;
    }

    public String getGroundType() {
        return groundType;
    }

    public void setGroundType(String groundType) {
        this.groundType = groundType;
    }

    public Integer getGroundSize() {
        return groundSize;
    }

    public void setGroundSize(Integer groundSize) {
        this.groundSize = groundSize;
    }

    public Integer getGroundCapacity() {
        return groundCapacity;
    }

    public void setGroundCapacity(Integer groundCapacity) {
        this.groundCapacity = groundCapacity;
    }

    public BigDecimal getRent() {
        return rent;
    }

    public void setRent(BigDecimal rent) {
        this.rent = rent;
    }

    public String getGroundState() {
        return groundState;
    }

    public void setGroundState(String groundState) {
        this.groundState = groundState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "GroundInfoDO{" +
                "id=" + id +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", isDeleted=" + isDeleted +
                ", groundId='" + groundId + '\'' +
                ", groundName='" + groundName + '\'' +
                ", groundLocation='" + groundLocation + '\'' +
                ", groundType='" + groundType + '\'' +
                ", groundSize=" + groundSize +
                ", groundCapacity=" + groundCapacity +
                ", rent=" + rent +
                ", groundState='" + groundState + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
