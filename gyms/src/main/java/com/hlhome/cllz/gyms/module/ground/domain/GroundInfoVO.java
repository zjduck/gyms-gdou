package com.hlhome.cllz.gyms.module.ground.domain;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 场地信息VO类
 *
 * Created by luckyTsai on 2018/6/22
 */

public class GroundInfoVO implements Serializable {


    private String groundId;


    private String groundName;


    private String groundLocation;


    private String groundType;


    private Integer groundSize;


    private Integer groundCapacity;


    private BigDecimal rent;


    private String groundState;


    private String remark;

    public String getGroundId() {
        return groundId;
    }

    public void setGroundId(String groundId) {
        this.groundId = groundId;
    }

    public String getGroundName() {
        return groundName;
    }

    public void setGroundName(String groundName) {
        this.groundName = groundName;
    }

    public String getGroundLocation() {
        return groundLocation;
    }

    public void setGroundLocation(String groundLocation) {
        this.groundLocation = groundLocation;
    }

    public String getGroundType() {
        return groundType;
    }

    public void setGroundType(String groundType) {
        this.groundType = groundType;
    }

    public Integer getGroundSize() {
        return groundSize;
    }

    public void setGroundSize(Integer groundSize) {
        this.groundSize = groundSize;
    }

    public Integer getGroundCapacity() {
        return groundCapacity;
    }

    public void setGroundCapacity(Integer groundCapacity) {
        this.groundCapacity = groundCapacity;
    }

    public BigDecimal getRent() {
        return rent;
    }

    public void setRent(BigDecimal rent) {
        this.rent = rent;
    }

    public String getGroundState() {
        return groundState;
    }

    public void setGroundState(String groundState) {
        this.groundState = groundState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "GroundInfoVO{" +
                "groundId='" + groundId + '\'' +
                ", groundName='" + groundName + '\'' +
                ", groundLocation='" + groundLocation + '\'' +
                ", groundType='" + groundType + '\'' +
                ", groundSize=" + groundSize +
                ", groundCapacity=" + groundCapacity +
                ", rent=" + rent +
                ", groundState='" + groundState + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
