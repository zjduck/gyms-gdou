package com.hlhome.cllz.gyms.module.game.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.game.domain.GameInfoVO;
import com.hlhome.cllz.gyms.module.game.service.GameInfoService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lingb on 2018/6/22
 */
@RequestMapping("/api/v1")
@RestController
public class GameInfoController {

    private static final Logger logger = LoggerFactory.getLogger(GameInfoController.class);

    @Autowired
    private GameInfoService gameInfoService;

    /**
     * 新增赛事信息
     * 需要 game_addGameInfo权限或管理员权限
     *
     * @param   [gameVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_addGameInfo", "admin"}, logical = Logical.OR)
    @PostMapping("/game/info")
    public WebResult addGameInfo(@RequestBody GameInfoVO gameInfoVO) {
        boolean flag = gameInfoService.saveGameInfo(gameInfoVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }

    /**
     * 更新赛事信息
     * 需要 game_updateGameInfo权限或管理员权限
     *
     * @param   [gameVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_updateGameInfo", "admin"}, logical = Logical.OR)
    @PutMapping("/game/info")
    public WebResult updateGameInfo(@RequestBody GameInfoVO gameInfoVO) {
        boolean flag = gameInfoService.updateGameInfo(gameInfoVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }

    /**
     * 删除赛事信息
     * 需要 game_deleteGameInfo权限或管理员权限
     *
     * @param   [gameId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_deleteGameInfo", "admin"}, logical = Logical.OR)
    @DeleteMapping("/game/info/{gameId}")
    public WebResult deleteGameInfo(@PathVariable("gameId") String gameId) {
        if(gameId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = gameInfoService.removeByGameId(gameId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }

    /**
     * 获取单个赛事信息
     * 需要 user_getGameInfo权限或管理员权限
     *
     * @param   [gameId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_getGameInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/game/info/{gameId}")
    public WebResult getGameInfo(@PathVariable("gameId") String gameId) {
        GameInfoVO gameInfoVO = gameInfoService.getByGameId(gameId);
        if (gameInfoVO != null) {
            return WebResult.success(gameInfoVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    /**
     * 获取所有赛事信息
     * 需要 game_listAllGameInfo权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_listAllGameInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/game/info")
    public WebResult listAllGameInfo() {
        List<GameInfoVO> gameInfoVOList = gameInfoService.listAll();
        if (gameInfoVOList != null) {
            return WebResult.success(gameInfoVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }

    /**
     * 通过用户Id获取所有赛事信息
     * 需要 game_listGameInfoByUserId权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_listGameInfoByUserId", "admin"}, logical = Logical.OR)
    @GetMapping("/game/info/user")
    public WebResult listGameInfoByUserId() {
        List<GameInfoVO> gameInfoVOList = gameInfoService.listByUserId();
        if (gameInfoVOList != null) {
            return WebResult.success(gameInfoVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }

}
