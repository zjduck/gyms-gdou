package com.hlhome.cllz.gyms.module.referee.domain;

import java.math.BigDecimal;

/**
 * 裁判项目VO类
 *
 * @author LoongMH
 * @date 2018/06/23
 */
public class RefereeItemVO {

    private String itemId;
    private String itemName;
    private BigDecimal chargeStandard;
    private String remark;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getChargeStandard() {
        return chargeStandard;
    }

    public void setChargeStandard(BigDecimal chargeStandard) {
        this.chargeStandard = chargeStandard;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "RefereeItemVO{" +
                "itemId='" + itemId + '\'' +
                ", itemName='" + itemName + '\'' +
                ", chargeStandard='" + chargeStandard + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}