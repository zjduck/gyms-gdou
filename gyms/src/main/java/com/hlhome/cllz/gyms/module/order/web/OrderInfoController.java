package com.hlhome.cllz.gyms.module.order.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.order.domain.OrderInfoVO;
import com.hlhome.cllz.gyms.module.order.service.OrderInfoService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lingb on 2018/6/25
 */
@RequestMapping("/api/v1")
@RestController
public class OrderInfoController {

    private static final Logger logger = LoggerFactory.getLogger(OrderInfoController.class);

    @Autowired
    private OrderInfoService orderInfoService;

    /**
     * 新增订单信息
     * 需要 order_addOrderInfo权限或管理员权限
     *
     * @param   [orderInfoVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"order_addOrderInfo", "admin"}, logical = Logical.OR)
    @PostMapping("/order/info")
    public WebResult addOrderInfo(@RequestBody OrderInfoVO orderInfoVO) {
        boolean flag = orderInfoService.saveOrderInfo(orderInfoVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }

    /**
     * 更新订单信息
     * 需要 order_updateOrderInfo权限或管理员权限
     *
     * @param   [orderInfoVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"order_updateOrderInfo", "admin"}, logical = Logical.OR)
    @PutMapping("/order/info")
    public WebResult updateOrderInfo(@RequestBody OrderInfoVO orderInfoVO) {
        boolean flag = orderInfoService.updateOrderInfo(orderInfoVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }

    /**
     * 删除订单信息
     * 需要 order_deleteOrderInfo权限或管理员权限
     *
     * @param   [gameId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"order_deleteOrderInfo", "admin"}, logical = Logical.OR)
    @DeleteMapping("/order/info{orderId}")
    public WebResult deleteGameInfo(@PathVariable("orderId") String orderId) {
        if(orderId == null){
            return WebResult.validateError("输入值不能为空！");
        }
        boolean flag = orderInfoService.removeOrderInfo(orderId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }

    /**
     * 获取单个订单信息
     * 需要 order_getOrderInfo权限或管理员权限
     *
     * @param   [orderId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"order_getOrderInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/order/info/{orderId}")
    public WebResult getOrderInfo(@PathVariable("orderId") String orderId) {
        OrderInfoVO orderInfoVO = orderInfoService.getByBookId(orderId);
        if (orderInfoVO != null) {
            return WebResult.success(orderInfoVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    /**
     * 获取所有订单信息
     * 需要 order_listAllOrderInfo权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"order_listAllOrderInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/order/info")
    public WebResult listAllOrderInfo() {
        List<OrderInfoVO> orderInfoVOList = orderInfoService.listAll();
        if (orderInfoVOList != null) {
            return WebResult.success(orderInfoVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }
}
