package com.hlhome.cllz.gyms.module.sys.domain;

import org.hibernate.validator.constraints.NotBlank;

public class AuthorityUpdateVM {
	
	@NotBlank
	private String id;
	
	@NotBlank
    private String name;

	@NotBlank
    private String code;

	@NotBlank
    private String url;

	@NotBlank
    private String method;

	@NotBlank
    private String controller;

    private String des;
}
