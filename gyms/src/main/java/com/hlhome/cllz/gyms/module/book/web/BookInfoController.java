package com.hlhome.cllz.gyms.module.book.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.book.domain.BookInfoVO;
import com.hlhome.cllz.gyms.module.book.service.BookInfoService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lingb on 2018/6/24
 */
@RequestMapping("/api/v1/book")
@RestController
public class BookInfoController {

    private static final Logger logger  = LoggerFactory.getLogger(BookInfoController.class);

    @Autowired
    private BookInfoService bookInfoService;
/*

    */
/**
     * 新增预约信息
     * 需要 book_addBookInfo权限或管理员权限
     *
     * @param   [bookInfoVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     *//*

    @RequiresPermissions(value={"book_addBookInfo", "admin"}, logical = Logical.OR)
    @PostMapping
    public WebResult addBookInfo(@RequestBody BookInfoVO bookInfoVO) {
        bookInfoService.saveBookInfo(bookInfoVO);
        return WebResult.success("新增成功！");
    }
*/


    /**
     * 更新预约信息
     * 需要 book_updateBookInfo权限或管理员权限
     *
     * @param   [bookInfoVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"book_updateBookInfo", "admin"}, logical = Logical.OR)
    @PutMapping
    public WebResult updateBookInfo(@RequestBody BookInfoVO bookInfoVO) {
        boolean flag = bookInfoService.updateBookInfo(bookInfoVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }


    /**
     * 删除预约信息
     * 需要 game_deleteBookInfo权限或管理员权限
     *
     * @param   [bookId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"game_deleteBookInfo", "admin"}, logical = Logical.OR)
    @DeleteMapping("/{bookId}")
    public WebResult deleteBookInfo(@PathVariable("bookId") String bookId) {
        if(bookId == null){
            return WebResult.validateError("输入值不能为空！");
        }
        boolean flag = bookInfoService.removeBookInfo(bookId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }
/*

    *//**
     * 获取单个预约信息
     * 需要 book_getBookInfo权限或管理员权限
     *
     * @param   [bookId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     *//*
    @RequiresPermissions(value={"book_getBookInfo", "admin"}, logical = Logical.OR)
    @GetMapping("/{bookId}")
    public WebResult getBookInfo(@PathVariable("bookId") String bookId) {
        BookInfoVO bookInfoVO = bookInfoService.getByBookId(bookId);
        if (bookInfoVO != null) {
            return WebResult.success(bookInfoVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }*/


    /**
     * 获取所有预约信息
     * 需要 book_listAllBookInfo权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"book_listAllBookInfo", "admin"}, logical = Logical.OR)
    @GetMapping
    public WebResult listAllBookInfo() {
        List<BookInfoVO> gameVOList = bookInfoService.listAll();
        if (gameVOList != null) {
            return WebResult.success(gameVOList, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }

}

