package com.hlhome.cllz.gyms.module.referee.mapper;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author LoongMH
 * @date 2018/06/23
 */
@Mapper
public interface RefereeInfoMapper {

    int insert(RefereeInfoDO refereeInfoDO);

    int deleteByRefereeId(String refereeId);

    int update(RefereeInfoDO refereeInfoDO);

    RefereeInfoDO getByRefereeId(String refereeId);

    RefereeInfoDO getByRefereeName(String refereeName);

    List<RefereeInfoDO> listAll();

    RefereeInfoDO getLatestByRefereeItem(String refereeItem);

    List<RefereeInfoDO> listAllFreeReferee();

    List<RefereeInfoDO> listAllBusyReferee();
}
