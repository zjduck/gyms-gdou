package com.hlhome.cllz.gyms.module.material.domain;

import java.time.LocalDateTime;

/**
 * 器材类型DO类
 *
 *Created by zmz on 2018/6/23
 */

public class MaterialTypeDO {

    private Integer id;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModified;
    private Boolean isDeleted;

    private String typeId;
    private String typeName;
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isdeleted) {
        isDeleted = isdeleted;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString(){
        return "MaterialTypeDO{"+
                "id="+id+
                ",gmtCreate"+gmtCreate+
                ",gmtModified"+gmtModified+
                ",isDeleted"+isDeleted+
                ",typeId"+typeId+'\'' +
                ",typeName"+typeName+'\'' +
                ",remark"+remark+'\'' +
                "}";
    }

}
