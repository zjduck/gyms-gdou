package com.hlhome.cllz.gyms.module.material.service;

import com.hlhome.cllz.gyms.module.material.domain.MaterialTypeVO;

import java.util.List;

/**
 * 器材类型服务接口
 *
 * created by zmz on 2018/6/23
 */

public interface MaterialTypeService {

    /**
     * 统计所有器材类型信息
     */
    List<MaterialTypeVO> listAll();

    /**
     * 新增器材类型信息
     */
    boolean saveMaterialType(MaterialTypeVO materialTypeVO);

    /**
     * 通过类型id，删除器材类型信息
     */
    boolean removeByTypeId (String typeId);

    /**
     * 更改器材类型信息
     */
    boolean updateMaterialType(MaterialTypeVO materialTypeVO);

    /**
     * 通过类型id，获取器材类型信息
     */
    MaterialTypeVO getByTypeId (String typeId);

    /**
     * 通过类型名称，获取器材类型信息
     */
    MaterialTypeVO getByTypeName (String typeName);

}
