package com.hlhome.cllz.gyms.module.sys.service.impl;

import com.hlhome.cllz.gyms.module.sys.domain.Authority;
import com.hlhome.cllz.gyms.module.sys.domain.ResourceAuthority;
import com.hlhome.cllz.gyms.module.sys.mapper.ResourceAuthorityExtendMapper;
import com.hlhome.cllz.gyms.module.sys.mapper.ResourceAuthorityMapper;
import com.hlhome.cllz.gyms.util.Asserts;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class ResourceAuthorityService {
	
	@Autowired
	ResourceAuthorityExtendMapper resourceAuthorityExtendMapper;
	
	@Autowired
	AuthorityService authorityService;
	
	@Autowired
	ResourceAuthorityMapper resourceAuthorityMapper;

	/**
	 * 根据参数资源 查出其拥有的 权限
	 */
	public List<String> getAuthorityByResource(List<String> resourceList){
		if(CollectionUtils.isEmpty(resourceList)){return null;}
		return resourceAuthorityExtendMapper.getAuthorityByResource(resourceList);
	}
	
	/**
	 * 查单个资源拥有的权限
	 * @param resourceId
	 * @return
	 */
	public List<Authority> getAuthorityByResource(String resourceId) {
		List<String>list=new ArrayList<>();
		list.add(resourceId);
		List<String> authorityIds = resourceAuthorityExtendMapper.getAuthorityByResource(list);
		
		return authorityService.getAuthorityByIds(authorityIds);
	}

	/**
	 * 取消授权
	 * @param resourceId
	 * @param authorityId
	 */
	public void deleteResourceAuthority(String resourceId, String authorityId) {
		resourceAuthorityExtendMapper.deleteResourceAuthority(resourceId,authorityId);
	}

	/**
	 * 给资源授权
	 * @param resourceId
	 * @param authorityId
	 */
	public void authorize(String resourceId, String authorityId) {
		Asserts.notEmpty(resourceId);
		Asserts.notEmpty(authorityId);
		
		ResourceAuthority resourceAuthority=new ResourceAuthority();
		resourceAuthority.setId(UUID.randomUUID().toString());
		resourceAuthority.setAuthorityId(authorityId);
		resourceAuthority.setResourceId(resourceId);
		
		resourceAuthorityMapper.insert(resourceAuthority);
	}
	
}
