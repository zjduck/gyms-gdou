package com.hlhome.cllz.gyms.module.material.service;

import com.hlhome.cllz.gyms.module.material.domain.MaterialRentDO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialRentVO;

import java.util.List;

/**
 * 器材租借服务接口
 *
 * created by zmz on 2018/6/23
 */

public interface MaterialRentService {

    /**
     * 统计所有器材租借信息
     */
    List<MaterialRentVO> listAll();

    /**
     * 新增器材租借信息
     */
    boolean saveMaterialRent(MaterialRentVO materialRentVO);

    /**
     * 通过预约id，删除器材租借信息
     */
    boolean removeByBookIdAndMaterialId (MaterialRentVO materialRentVO);

    /**
     * 更改器材租借信息
     */
    boolean updateMaterialRent(MaterialRentVO materialRentVO);

    /**
     * 通过器材id，获取器材租借信息
     */
    List<MaterialRentVO> listByMaterialId (String materialId);

    /**
     * 通过预约id，获取器材租借信息
     */
    List<MaterialRentVO> listByBookId (String bookId);

    /**
     *通过预约id和器材id，获取器材租借信息
     */
    MaterialRentVO getByBookIdAndMaterialId (String bookId,String materialId);

//    /**
//     * 通过预约id，审核器材租借信息
//     */
//    boolean auditMaterialRent(MaterialRentVO materialRentVO);


}
