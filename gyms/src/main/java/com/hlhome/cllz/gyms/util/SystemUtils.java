package com.hlhome.cllz.gyms.util;

/**
 * Created by lingb on 2018/5/30
 */
public class SystemUtils {

    public static String roleBind(String roleName) {

        String roleId = "";

        switch (roleName) {
            case "超级管理员":
                roleId = "01";
                break;
            case "预约管理员":
                roleId = "02";
                break;
            case "赛事管理员":
                roleId = "03";
                break;
            case "场地管理员":
                roleId = "04";
                break;
            case "器材管理员":
                roleId = "05";
                break;
            case "裁判管理员":
                roleId = "06";
                break;
            case "系统管理员":
                roleId = "07";
                break;
            case "用户管理员":
                roleId = "08";
                break;
            case "普通用户":
                roleId = "09";
                break;
            default:
                roleId = "10";
                break;
        }

        return roleId;
    }

    public static String materialTypeBind(String typeName){

        String typeId="";

        switch (typeName){
            case "篮球":
                typeId="01";
                break;
            case "足球":
                typeId="02";
                break;
            case "排球":
                typeId="03";
                break;
            case "羽毛球拍":
                typeId="04";
                break;
            case "网球拍":
                typeId="05";
                break;
            case "乒乓球拍":
                typeId="06";
                break;
            case "毽子":
                typeId="07";
                break;
            case "跳绳":
                typeId="08";
                break;
            case "轮滑鞋":
                typeId="09";
                break;
            case "垒球棒":
                typeId="10";
                break;
            default:
                typeId="00";
                break;
        }

        return typeId;
    }

    public static String gameTypeBind(String gameType){

        String typeId="";

        switch (gameType){
            case "篮球比赛":
                typeId="01";
                break;
            case "足球比赛":
                typeId="02";
                break;
            case "排球比赛":
                typeId="03";
                break;
            case "羽毛球比赛":
                typeId="04";
                break;
            case "网球比赛":
                typeId="05";
                break;
            case "乒乓球比赛":
                typeId="06";
                break;
            case "毽子比赛":
                typeId="07";
                break;
            case "跳绳比赛":
                typeId="08";
                break;
            case "轮滑比赛":
                typeId="09";
                break;
            case "垒球比赛":
                typeId="10";
                break;
            default:
                typeId="00";
                break;
        }

        return typeId;

    }

    public static String groundTypeBind(String typeName){

        String typeId="";

        switch (typeName){
            case "篮球场":
                typeId="01";
                break;
            case "足球场":
                typeId="02";
                break;
            case "排球场":
                typeId="03";
                break;
            case "羽毛球场":
                typeId="04";
                break;
            case "网球场":
                typeId="05";
                break;
            case "乒乓球场":
                typeId="06";
                break;
            case "轮滑场":
                typeId="07";
                break;
            case "垒球场":
                typeId="08";
                break;
            case "高尔夫球场":
                typeId="09";
                break;
            default:
                typeId="00";
                break;
        }

        return typeId;
    }

    public static String refereeTypeBind(String typeName){

        String typeId="";

        switch (typeName){
            case "篮球":
                typeId="01";
                break;
            case "足球":
                typeId="02";
                break;
            case "排球":
                typeId="03";
                break;
            case "羽毛球":
                typeId="04";
                break;
            case "网球":
                typeId="05";
                break;
            case "乒乓球":
                typeId="06";
                break;
            case "毽球":
                typeId="07";
                break;
            case "跳绳":
                typeId="08";
                break;
            case "轮滑":
                typeId="09";
                break;
            case "垒球":
                typeId="10";
                break;
            default:
                typeId="00";
                break;
        }

        return typeId;
    }

    public static String sexTypeBind(String typeId){

        String typeName="";

        switch (typeId){
            case "1":
                typeName="男";
                break;
            case "0":
                typeName="女";
                break;
            default:
                typeName="2";
                break;
        }

        return typeName;
    }

    public static void main(String[] args){
        String typeId = materialTypeBind("跳绳");
        System.out.println(typeId);

    }
}