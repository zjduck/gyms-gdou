package com.hlhome.cllz.gyms.module.material.domain;

/**
 * 器材租借VO类
 *
 *Created by zmz on 2018/6/23
 */

public class MaterialRentVO {

    private String materialId;
    private String bookId;
    private Integer rentNumber;
    private String rentTime;
    private String auditState;
    private String remark;

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public Integer getRentNumber() {
        return rentNumber;
    }

    public void setRentNumber(Integer rentNumber) {
        this.rentNumber = rentNumber;
    }

    public String getRentTime() {
        return rentTime;
    }

    public void setRentTime(String rentTime) {
        this.rentTime = rentTime;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString(){
        return "MaterialRentVO{"+
                ",materialId"+materialId+'\'' +
                ",bookId"+bookId+'\'' +
                ",rentNumber"+rentNumber+'\'' +
                ",rentTime"+rentTime+'\'' +
                ",auditState"+auditState+'\'' +
                ",remark"+remark+'\'' +
                "}";
    }
}
