package com.hlhome.cllz.gyms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.hlhome.cllz.gyms")
@SpringBootApplication
public class GymsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GymsApplication.class, args);
	}
}
