package com.hlhome.cllz.gyms.module.user.mapper;

import com.hlhome.cllz.gyms.module.user.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface UserExtendMapper {

	User selectByName(@Param(value = "account") String account);

	/**
	 * 校验用户名唯一,账号
	 */
	Integer isExist(@Param(value = "loginName") String loginName);
}
