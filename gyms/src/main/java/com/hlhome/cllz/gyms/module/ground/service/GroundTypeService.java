package com.hlhome.cllz.gyms.module.ground.service;

import com.hlhome.cllz.gyms.module.ground.domain.GroundTypeDO;
import com.hlhome.cllz.gyms.module.ground.domain.GroundTypeVO;

import java.util.List;

/**
 * Created by luckyTsai on 2018/6/23
 */
public interface GroundTypeService {

    /**
     * 统计所有场地类型
     */
    List<GroundTypeVO> listAll();

    /**
     * 通过场地类型id获取场地类型
     */
    GroundTypeVO getByTypeId(String typeId);

    /**
     * 通过场地类型名称获取场地类型
     */
    GroundTypeVO getByTypeName(String typeName);

    /**
     * 添加场地类型
     */
    boolean saveGroundType(GroundTypeVO groundTypeVO);

    /**
     * 更新场地类型
     */
    boolean updateGroundType(GroundTypeVO groundTypeVO);

    /**
     * 通过类型编号删除场地类型
     */
    boolean removeByTypeId(String typeId);

    /**
     * 通过场地类型，获取最新的场地类型
     */
    GroundTypeDO getLatestByGroundType(String groundType);
}
