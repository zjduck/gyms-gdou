package com.hlhome.cllz.gyms.module.material.service;


import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoDO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoVO;
import com.hlhome.cllz.gyms.module.material.domain.MaterialTypeDO;
import com.hlhome.cllz.gyms.module.material.mapper.MaterialInfoMapper;
import com.hlhome.cllz.gyms.module.material.mapper.MaterialTypeMapper;
import com.hlhome.cllz.gyms.util.Asserts;
import com.hlhome.cllz.gyms.util.Constants;
import com.hlhome.cllz.gyms.util.POJOUtils;
import com.hlhome.cllz.gyms.util.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 材信息业务层
 *
 * created by zmz on 2018/6/23
 */
@Service
@Transactional
public class MaterialInfoServiceImpl implements MaterialInfoService {

    private static final Logger logger = LoggerFactory.getLogger(MaterialInfoServiceImpl.class);

    @Autowired
    private MaterialInfoMapper materialInfoMapper;

    @Autowired
    private MaterialTypeMapper materialTypeMapper;


    @Override
    public List<MaterialInfoVO> listAll() {
        List<MaterialInfoVO> materialInfoVOList = new ArrayList<>();
        List<MaterialInfoDO> materialInfoDOList = materialInfoMapper.listAll();
        for(MaterialInfoDO materialInfoDO : materialInfoDOList){
            MaterialInfoVO materialInfoVO = POJOUtils.DO2VO(materialInfoDO,MaterialInfoVO.class);
            materialInfoVOList.add(materialInfoVO);
        }
        return materialInfoVOList;
    }

    @Override
    public boolean saveMaterialInfo(MaterialInfoVO materialInfoVO) {
        boolean flag =false;

        Asserts.validate(materialInfoVO,"materialInfoVO");

        MaterialInfoDO materialInfoDO = new MaterialInfoDO();
        MaterialTypeDO materialTypeDO = new MaterialTypeDO();
        materialInfoDO = POJOUtils.VO2DO(materialInfoVO,MaterialInfoDO.class);
        materialInfoDO.setGmtCreate(LocalDateTime.now().withNano(0));
        materialInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
       // materialInfoDO = POJOUtils.VO2DO(materialInfoVO,MaterialInfoDO.class);
        MaterialInfoDO latestMaterial = materialInfoMapper.getLatestByMaterialType(materialInfoVO.getMaterialName());

        int result;

        // 若为空，该器材类型是第一个,进行数据库增加操作
        if (latestMaterial == null) {
            String materialNum = SystemUtils.materialTypeBind(materialInfoVO.getMaterialName());
            materialInfoDO.setMaterialId(Constants.MATERIAL + materialNum);
            materialInfoDO.setMaterialType(materialNum);
            materialInfoDO.setMaterialResidue(materialInfoDO.getMaterialNumber());

            materialTypeDO.setTypeName(materialInfoVO.getMaterialName());
            materialTypeDO.setTypeId(materialNum);
            materialTypeDO.setGmtCreate(LocalDateTime.now().withNano(0));
            materialTypeDO.setGmtModified(LocalDateTime.now().withNano(0));
            materialTypeMapper.insert(materialTypeDO);

           result =materialInfoMapper.insert(materialInfoDO);

            // 否则，该器材类型库存和总数量相加，进行数据库更新操作
        } else {
            // 通过器材id 获取数据库该器材类型原有库存和总数量
            int residueDB = latestMaterial.getMaterialResidue();
            int numberDB = latestMaterial.getMaterialNumber();
            // 新增该器材类型数目
            int residueNew = materialInfoDO.getMaterialNumber();
            // 新的库存 = 数据库原有库存 + 新增数目  新的总数也相同
            int materialResidue = residueDB + residueNew;
            int materialNumber = numberDB + residueNew;

            latestMaterial.setMaterialResidue(materialResidue);
            latestMaterial.setMaterialNumber(materialNumber);
            latestMaterial.setGmtModified(LocalDateTime.now().withNano(0));



            result =  materialInfoMapper.update(latestMaterial) ;
        }

        flag = result == 0 ? false : true ;
        return flag;

    }
    @Override
    public boolean removeByMaterialId(String materialId) {
        boolean flag = false;
        int result = materialInfoMapper.deleteByMaterialId(materialId);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public boolean updateMaterialInfo(MaterialInfoVO materialInfoVO) {
        boolean flag = false;
        MaterialInfoDO materialInfoDO = POJOUtils.VO2DO(materialInfoVO, MaterialInfoDO.class);
        materialInfoDO.setGmtModified(LocalDateTime.now().withNano(0));
        String materialNum = SystemUtils.materialTypeBind(materialInfoVO.getMaterialName());
        materialInfoDO.setMaterialId(Constants.MATERIAL + materialNum);
        materialInfoDO.setMaterialType(materialNum);


        int result = materialInfoMapper.update(materialInfoDO);
        flag = result == 0 ? false : true;
        return flag;
    }

    @Override
    public MaterialInfoVO getByMaterialId(String materialId) {
        MaterialInfoDO materialInfoDO = materialInfoMapper.getByMaterialId(materialId);
        return POJOUtils.DO2VO(materialInfoDO, MaterialInfoVO.class);
    }

    @Override
    public MaterialInfoVO getByMaterialName(String materialName) {
        MaterialInfoDO materialInfoDO = materialInfoMapper.getByMaterialName(materialName);
        return POJOUtils.DO2VO(materialInfoDO, MaterialInfoVO.class);
    }
}
