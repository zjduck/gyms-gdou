package com.hlhome.cllz.gyms.module.user.web;

import com.hlhome.cllz.gyms.module.user.domain.User;
import com.hlhome.cllz.gyms.module.user.domain.UserRoleSaveVMS;
import com.hlhome.cllz.gyms.module.user.domain.UserSaveVM;
import com.hlhome.cllz.gyms.module.user.domain.UserUpdateVM;
import com.hlhome.cllz.gyms.module.user.service.UserRoleService;
import com.hlhome.cllz.gyms.module.user.service.UserService;
import com.hlhome.cllz.gyms.util.Constants;
import com.hlhome.cllz.gyms.util.Message;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRoleService userRoleService;


	@RequiresPermissions(value={"user_saveUser","administrator"},logical= Logical.OR)
	@RequestMapping(value="/user",method=RequestMethod.POST)
	public Message<String> saveUser(UserSaveVM user){
		userService.saveUser(user);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"user_updateUser","administrator"},logical= Logical.OR)
	@RequestMapping(value="/user",method=RequestMethod.PUT)
	public Message<String> updateUser(UserUpdateVM user){
		userService.updateUser(user);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"user_deleteUser","administrator"},logical= Logical.OR)
	@RequestMapping(value="/user/{id}",method=RequestMethod.DELETE)
	public Message<String> deleteUser(@PathVariable("id")String id){
		userService.deleteUser(id);
		return Message.ok(Constants.SUCCESS);

	}
	

	@RequiresPermissions(value={"user_delegate","administrator"},logical= Logical.OR)
	@RequestMapping(value="/user/delegate",method=RequestMethod.POST)
	public Message<String> delegate(UserRoleSaveVMS userRoleVMS){
		userRoleService.delegate(userRoleVMS);
		return Message.ok(Constants.SUCCESS);
	}
	

	@RequiresPermissions(value={"user_findAllUser","administrator"},logical= Logical.OR)
	@RequestMapping(value="/user",method=RequestMethod.GET)
	public Message<List<User>> findAllUser(){
		List<User> userList=userService.findAllUser();
		return Message.ok(userList);
	}
	

	@RequiresPermissions(value={"user_isExist","administrator"},logical= Logical.OR)
	@RequestMapping(value="/user/isExist",method=RequestMethod.GET)
	public Message<Boolean> isExist(/*@RequestParam(value="loginName")*/String loginName){
		return Message.ok(userService.isExist(loginName));
	}
	

	@RequiresPermissions(value={"user_getRoleByUser","administrator"},logical= Logical.OR)
	@RequestMapping(value="/user/getRoleByUser",method=RequestMethod.GET)
	public Message<List<String>>getRoleByUser(@RequestParam(value="userId")String userId){
		return Message.ok(userRoleService.getRoleByUser(userId));
	}
}
