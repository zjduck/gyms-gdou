package com.hlhome.cllz.gyms.module.ground.web;

import com.hlhome.cllz.gyms.bean.WebResult;
import com.hlhome.cllz.gyms.module.ground.domain.GroundTypeVO;
import com.hlhome.cllz.gyms.module.ground.service.GroundTypeService;
import com.hlhome.cllz.gyms.module.user.web.UserController;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by luckyTsai on 2018/6/24
 */
@RequestMapping("/api/v1")
@RestController
public class GroundTypeController {
    private static final Logger logger = LoggerFactory.getLogger(GroundTypeController.class);

    @Autowired
    private GroundTypeService groundTypeService;

    /**
     * 新增场地类型
     * 需要 ground_addGroundType权限或管理员权限
     *
     * @param   [GroundTypeVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_addGroundType", "admin"}, logical = Logical.OR)
    @PostMapping("/ground/type")
    public WebResult addGroundType(@RequestBody GroundTypeVO groundTypeVO) {
        boolean flag = groundTypeService.saveGroundType(groundTypeVO);
        if (flag) {
            return WebResult.success(flag,"新增成功！");

        } else {
            return WebResult.serverError("新增失败！");
        }
    }

    /**
     * 更新场地类型
     * 需要 ground_updateGroundType权限或管理员权限
     *
     * @param   [GroundTypeVO]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_updateGroundRent", "admin"}, logical = Logical.OR)
    @PutMapping("/ground/type")
    public WebResult updateGroundType(@RequestBody GroundTypeVO groundTypeVO) {
        boolean flag = groundTypeService.updateGroundType(groundTypeVO);
        if (flag) {
            return WebResult.success(flag,"更新成功！");

        } else {
            return WebResult.serverError("更新失败！");
        }
    }

    /**
     * 删除场地类型
     * 需要 ground_deleteGroundType权限或管理员权限
     *
     * @param   [typeId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_deleteGroundType", "admin"}, logical = Logical.OR)
    @DeleteMapping("/ground/type/{typeId}")
    public WebResult deleteGroundType(@PathVariable("typeId") String typeId) {
        if(typeId == null){
            return WebResult.serverError("输入值不能为空！");
        }
        boolean flag = groundTypeService.removeByTypeId(typeId);
        if(flag){
            return WebResult.success(flag,"删除成功");

        } else {
            return WebResult.serverError("删除失败！");
        }
    }

    /**
     * 获取单个场地类型
     * 需要 ground_getGroundTypeDetail权限或管理员权限
     *
     * @param   [typeId]
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_getGroundTypeDetail", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/type/{typeId}")
    public WebResult getGroundTypeDetail(@PathVariable String typeId) {
        GroundTypeVO groundTypeVO = groundTypeService.getByTypeId(typeId);
        if (groundTypeVO != null) {
            return WebResult.success(groundTypeVO, "获取成功！");

        } else {
            return WebResult.serverError("获取失败！");
        }

    }

    /**
     * 获取所有场地类型
     * 需要 ground_listAllGroundType权限或管理员权限
     *
     * @param   []
     * @return  com.hlhome.cllz.gyms.bean.WebResult
     */
    @RequiresPermissions(value={"ground_listAllGroundType", "admin"}, logical = Logical.OR)
    @GetMapping("/ground/type")
    public WebResult listAllGroundType() {
        List<GroundTypeVO> groundTypeVOList = groundTypeService.listAll();
        if (groundTypeService != null) {
            return WebResult.success(groundTypeService, "获取成功！");

        } else {
            return WebResult.validateError("获取失败！");
        }
    }
}
