package com.hlhome.cllz.gyms.module.user.web;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by lingb on 2018/6/19
 */
@RunWith(SpringRunner.class)
// 需要测试的 Controller
@WebMvcTest(UserController.class)
public class UserControllerTest {


}
