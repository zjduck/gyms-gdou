package com.hlhome.cllz.gyms.module.referee;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeRentVO;
import com.hlhome.cllz.gyms.module.referee.service.RefereeRentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * @author LoongMH
 * @date 2018/06/25
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RefereeRentServiceTest {

    @Autowired
    private RefereeRentService refereeRentService;

    @Test
    public void testAddRent(){

        RefereeRentVO refereeRentVO = new RefereeRentVO();
        refereeRentVO.setRefereeItem("篮球");
        refereeRentVO.setBookId("00002");
        refereeRentVO.setRentTime("2018062503");
        refereeRentVO.setAuditState("成功");
        refereeRentVO.setRemark("我很帅");

        System.out.println(refereeRentVO);
        refereeRentService.addRent(refereeRentVO);
    }

    @Test
    public void testRemoveRent(){

        RefereeRentVO refereeRentVO = new RefereeRentVO();
        refereeRentVO.setRefereeId("R02001");

        System.out.println(refereeRentVO);
        refereeRentService.removeRent(refereeRentVO.getRefereeId());
    }

    @Test
    public void testUpdateRent(){

        RefereeRentVO refereeRentVO = new RefereeRentVO();
        refereeRentVO.setRefereeId("R02001");
        refereeRentVO.setRefereeItem("足球");
        refereeRentVO.setBookId("00023");
        refereeRentVO.setRentTime("2018062502");
        refereeRentVO.setAuditState("失败");
        refereeRentVO.setRemark("我很帅");

        System.out.println(refereeRentVO);
        refereeRentService.updateRent(refereeRentVO);
    }

    @Test
    public void testGetByRefereeId() {

        RefereeRentVO refereeRentVO = new RefereeRentVO();
        refereeRentVO.setRefereeId("R02001");
        System.out.println(refereeRentService.getByRefereeId(refereeRentVO.getRefereeId()));
    }

    @Test
    public void testListAll(){

        System.out.println(refereeRentService.listAll());
    }

    @Test
    public void testCheckRent(){

        RefereeRentVO refereeRentVO = new RefereeRentVO();
        refereeRentVO.setRefereeId("R02001");
        refereeRentVO.setRefereeItem("足球");
        refereeRentVO.setBookId("00001");
        refereeRentVO.setRentTime("2018062502");
        refereeRentVO.setRemark("我很帅");

        System.out.println(refereeRentVO);
        refereeRentService.checkRent(refereeRentVO);
    }
}
