package com.hlhome.cllz.gyms.module.game.service;

import com.hlhome.cllz.gyms.module.game.domain.GameInfoVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by lingb on 2018/6/24
 */
@RunWith(SpringRunner.class)
@SpringBootTest

public class GameInfoServiceTest {


    @Autowired
    private GameInfoService gameInfoService;

    // @MockBean

    @Test
    public void testSaveGame() {
//        assertEquals();

        GameInfoVO gameInfoVO = new GameInfoVO();
        String bookId = "B20180624003";
        gameInfoVO.setBookId(bookId);
        gameInfoVO.setGameName("海蓝之家206篮球比赛");
        gameInfoVO.setGameIntroduction("篮球使我快乐");
        gameInfoVO.setGameSponsor("海蓝之家");
        gameInfoVO.setGameTime("20180624");
        gameInfoVO.setGameType("足球比赛");
        gameInfoVO.setRemark("我很帅~");

        System.out.println(gameInfoVO);
        gameInfoService.saveGameInfo(gameInfoVO);
    }

    @Test
    public void testListAll() {
        List<GameInfoVO> gameInfoVOList = gameInfoService.listAll();
        for(GameInfoVO gameInfoVO : gameInfoVOList){
            System.out.println(gameInfoVO);
        }
    }


}
