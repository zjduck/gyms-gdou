package com.hlhome.cllz.gyms.module.material.service;

import com.hlhome.cllz.gyms.module.material.domain.MaterialInfoVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zmz on 2018/6/25
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class MaterialInfoServiceTest {

    @Autowired
    private MaterialInfoService materialInfoService;

    //增加器材信息ServiceTest
    @Test
    public void saveMaterialInfo(){

        MaterialInfoVO materialInfoVO = new MaterialInfoVO();
        materialInfoVO.setMaterialName("足球");
        materialInfoVO.setMaterialNumber(10);
        materialInfoVO.setBuyTime("2018/6/25");
        materialInfoVO.setRent(new BigDecimal("5"));
        materialInfoVO.setRemark("我爱圳机智");

        System.out.println(materialInfoVO);
        materialInfoService.saveMaterialInfo(materialInfoVO);

    }

    //遍历器材信息ServiceTest
    @Test
    public void listAllMaterialInfo(){

        List<MaterialInfoVO> materialInfoVOList = new ArrayList<>();

        materialInfoVOList = materialInfoService.listAll();

        // 1、
        for (int i  = 0; i <  materialInfoVOList.size(); i++) {
            MaterialInfoVO materialInfoVO = materialInfoVOList.get(i);
            System.out.println("1、 " + materialInfoVO);
        }

        // 2、
        for(MaterialInfoVO materialInfoVO : materialInfoVOList) {
            System.out.println("2、 " + materialInfoVO);
        }

        System.out.println(materialInfoVOList);


    }

    //查找器材信息ServiceTest
    @Test
    public  void  queryMaterial(){

        //按器材ID查找
        MaterialInfoVO materialInfoVO = materialInfoService.getByMaterialId("M01");

        //按器材名称查找
        MaterialInfoVO materialInfoVO1 = materialInfoService.getByMaterialName("足球");

        System.out.println("按ID"+materialInfoVO);
        System.out.println("按Name"+materialInfoVO1);

    }

    //更改器材信息ServiceTest
    @Test
    public void updateMaterial(){

        //按器材ID查找
        MaterialInfoVO materialInfoVO = materialInfoService.getByMaterialId("M01");

        materialInfoVO.setRemark("更改成功");


        materialInfoService.updateMaterialInfo(materialInfoVO);

    }

    //删除器材信息ServiceTest
    @Test
    public void deleteMaterial(){

        materialInfoService.removeByMaterialId("M01");

    }




}
