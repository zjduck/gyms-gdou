package com.hlhome.cllz.gyms.module.material.service;

import com.hlhome.cllz.gyms.module.material.domain.MaterialRentVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zmz on 2018/6/25
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class MaterialRentServiceTest {

    @Autowired
    private MaterialRentService materialRentService;

    //增加器材租借信息ServiceTest
    @Test
    public void saveMaterialRent(){

        MaterialRentVO materialRentVO = new MaterialRentVO();

        materialRentVO.setMaterialId("M01");
        materialRentVO.setBookId("001");
        materialRentVO.setRentNumber(10);
        materialRentVO.setRentTime("2018/6/25");
        materialRentVO.setAuditState("等待");
        materialRentVO.setRemark("圳机智真滴帅");

        materialRentService.saveMaterialRent(materialRentVO);

    }

    //删除器材租借信息ServiceTest
    @Test
    public void deleteMaterialRent(){
        List<MaterialRentVO> materialRentVOList = materialRentService.listByMaterialId("M01");


        for(MaterialRentVO materialRentVO : materialRentVOList) {
            materialRentVO.setMaterialId("M01");
            materialRentVO.setBookId("001");
            materialRentVO.setRentNumber(4);
            materialRentVO.setAuditState("租借审核成功");
            materialRentVO.setRentTime("2018/06/30");
            materialRentVO.setRemark("更新成功");

            materialRentService.removeByBookIdAndMaterialId(materialRentVO);
        }


    }


    //更新器材租借信息ServiceTest
    @Test
    public void updateMaterialRent(){

        List<MaterialRentVO> materialRentVOList = materialRentService.listByMaterialId("M01");


        for(MaterialRentVO materialRentVO : materialRentVOList) {
            materialRentVO.setMaterialId("M01");
            materialRentVO.setBookId("001");
            materialRentVO.setRentNumber(4);
            materialRentVO.setAuditState("租借审核成功");
            materialRentVO.setRentTime("2018/06/30");
            materialRentVO.setRemark("更新成功");
            materialRentService.updateMaterialRent(materialRentVO);
        }


    }

    //遍历器材租借信息ServiceTest
    @Test
    public void listAllMaterialRent(){

        List<MaterialRentVO> materialRentVOList = new ArrayList<>();

        materialRentVOList = materialRentService.listAll();

        System.out.println(materialRentVOList);

    }

    //查找器材租借信息ServiceTest
    @Test
    public void queryMaterialRent(){

        List<MaterialRentVO> materialRentVOList = materialRentService.listByMaterialId("M01");

        List<MaterialRentVO> materialRentVOList1 = materialRentService.listByBookId("001");

        System.out.println("按器材ID"+materialRentVOList);

        System.out.println("按预约ID"+materialRentVOList1);

    }

}
