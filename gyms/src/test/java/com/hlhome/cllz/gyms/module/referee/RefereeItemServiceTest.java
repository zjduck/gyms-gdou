package com.hlhome.cllz.gyms.module.referee;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeItemVO;
import com.hlhome.cllz.gyms.module.referee.service.RefereeItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * @author LoongMH
 * @date 2018/06/25
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RefereeItemServiceTest {

    @Autowired
    private RefereeItemService refereeItemService;

    @Test
    public void testAddItem(){

        BigDecimal num = new BigDecimal("50");
        RefereeItemVO refereeItemVO = new RefereeItemVO();
        refereeItemVO.setItemName("足球");
        refereeItemVO.setChargeStandard(num);
        refereeItemVO.setRemark("我很帅");

        System.out.println(refereeItemVO);
        refereeItemService.addItem(refereeItemVO);
    }

    @Test
    public void testRemoveItem(){

        RefereeItemVO refereeItemVO = new RefereeItemVO();
        refereeItemVO.setItemId("R02002");

        System.out.println(refereeItemVO);
        refereeItemService.removeItem(refereeItemVO.getItemId());
    }

    @Test
    public void testUpdateItem(){

        BigDecimal num = new BigDecimal("30");
        RefereeItemVO refereeItemVO = new RefereeItemVO();
        refereeItemVO.setItemId("R01002");
        refereeItemVO.setItemName("足球");
        refereeItemVO.setChargeStandard(num);
        refereeItemVO.setRemark("我很帅");

        System.out.println(refereeItemVO);
        refereeItemService.updateItem(refereeItemVO);
    }

    @Test
    public void testGetByItemId() {

        RefereeItemVO refereeItemVO = new RefereeItemVO();
        refereeItemVO.setItemId("R01002");
        System.out.println(refereeItemService.getByItemId(refereeItemVO.getItemId()));
    }

    @Test
    public void testListAll(){

        System.out.println(refereeItemService.listAll());
    }
}
