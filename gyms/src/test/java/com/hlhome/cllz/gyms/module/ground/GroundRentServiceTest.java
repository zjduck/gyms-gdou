package com.hlhome.cllz.gyms.module.ground;


import com.hlhome.cllz.gyms.module.ground.domain.GroundRentVO;
import com.hlhome.cllz.gyms.module.ground.service.GroundRentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by luckyTsai on 2018/6/24
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GroundRentServiceTest {

    @Autowired
    private GroundRentService groundRentService;

    @Test
    public void testSaveGroundRent(){

        GroundRentVO groundRentVO = new GroundRentVO();

        groundRentVO.setGroundId("G09001");
        groundRentVO.setBookId("G00");
        groundRentVO.setRentTime("20180625");
        groundRentVO.setAuditState("等待");
        groundRentVO.setRemark(" ");

        System.out.println(groundRentVO);
        groundRentService.saveGroundRent(groundRentVO);

    }
}
