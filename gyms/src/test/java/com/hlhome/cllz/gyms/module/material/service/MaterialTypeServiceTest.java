package com.hlhome.cllz.gyms.module.material.service;

import com.hlhome.cllz.gyms.module.material.domain.MaterialTypeVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zmz on 2018/6/25
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class MaterialTypeServiceTest {

    @Autowired
    private MaterialTypeService materialTypeService;

    //增加器材类型信息ServiceTest
    @Test
    public void saveMaterialType(){

        MaterialTypeVO materialTypeVO = new MaterialTypeVO();

        materialTypeVO.setTypeName("网球拍");

        materialTypeService.saveMaterialType(materialTypeVO);

    }

    //删除器材类型信息ServiceTest
    @Test
    public void deleteMaterialType(){

        materialTypeService.removeByTypeId("00");

    }

    //更新器材类型信息ServiceTest
    @Test
    public void updateMaterialType(){

        MaterialTypeVO materialTypeVO = materialTypeService.getByTypeName("网球拍");

        materialTypeVO.setRemark("更新成功");

        materialTypeService.updateMaterialType(materialTypeVO);

    }

    //遍历器材类型信息ServiceTest
    @Test
    public void listAllMaterialType(){

        List<MaterialTypeVO> materialTypeVOList = new ArrayList<>();

        materialTypeVOList = materialTypeService.listAll();

        System.out.println(materialTypeVOList);

    }

    //查找器材类型信息ServiceTest
    @Test
    public void queryMaterialType(){

        MaterialTypeVO materialTypeVO = materialTypeService.getByTypeName("网球拍");

        MaterialTypeVO materialTypeVO1 = materialTypeService.getByTypeId("01");

        System.out.println("通过类型名称查找"+materialTypeVO);

        System.out.println("通过类型ID查找"+materialTypeVO1);

    }

}
