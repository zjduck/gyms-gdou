package com.hlhome.cllz.gyms.module.ground;

import com.hlhome.cllz.gyms.module.ground.domain.GroundInfoVO;
import com.hlhome.cllz.gyms.module.ground.service.GroundInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * Created by luckyTsai on 2018/6/25
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GroundInfoServiceTest {

    @Autowired
    private GroundInfoService groundInfoService;

    @Test
    public void testSaveGroundInfo(){

        GroundInfoVO groundInfoVO = new GroundInfoVO();

        BigDecimal num = new BigDecimal("4000");
        groundInfoVO.setGroundName("高尔夫");
        groundInfoVO.setGroundLocation("珠江新城");
        groundInfoVO.setGroundType("高尔夫球场");
        groundInfoVO.setGroundSize(1000);
        groundInfoVO.setGroundCapacity(500);
        groundInfoVO.setRent(num);
        groundInfoVO.setRemark("  ");

        System.out.println(groundInfoVO);
        groundInfoService.saveGround(groundInfoVO);

    }
}
