package com.hlhome.cllz.gyms.module.ground;

import com.hlhome.cllz.gyms.module.ground.domain.GroundTypeVO;
import com.hlhome.cllz.gyms.module.ground.service.GroundTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * Created by luckyTsai on 2018/6/25
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GroundTypeServiceTest {

    @Autowired
    private GroundTypeService groundTypeService;

    @Test
    public void testSaveGroundType(){

        GroundTypeVO groundTypeVO = new GroundTypeVO() ;

        groundTypeVO.setTypeId("09");
        groundTypeVO.setTypeName("高尔夫球场");
        groundTypeVO.setRemark("  ");



        System.out.println(groundTypeVO);
        groundTypeService.saveGroundType(groundTypeVO);
    }
}
