package com.hlhome.cllz.gyms.module.referee;

import com.hlhome.cllz.gyms.module.referee.domain.RefereeInfoVO;
import com.hlhome.cllz.gyms.module.referee.service.RefereeInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * @author LoongMH
 * @date 2018/06/25
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RefereeInfoServiceTest {

    @Autowired
    private RefereeInfoService refereeInfoService;

    @Test
    public void testAddReferee() {

        BigDecimal num = new BigDecimal("50");
        RefereeInfoVO refereeInfoVO = new RefereeInfoVO();
        refereeInfoVO.setRefereeName("龙敏华");
        refereeInfoVO.setRefereeSex("1");
        refereeInfoVO.setRefereeAge("21");
        refereeInfoVO.setRefereeItem("篮球");
        refereeInfoVO.setChargeStandard(num);
        refereeInfoVO.setRefereeState("空闲");
        refereeInfoVO.setPhone("13392279606");
        refereeInfoVO.setRemark("我很帅");

        System.out.println(refereeInfoVO);
        refereeInfoService.addReferee(refereeInfoVO);
    }

    @Test
    public void testRemoveReferee() {

        RefereeInfoVO refereeInfoVO = new RefereeInfoVO();
        refereeInfoVO.setRefereeId("R01001");

        System.out.println(refereeInfoVO);
        refereeInfoService.removeReferee(refereeInfoVO.getRefereeId());
    }

    @Test
    public void testUpdateReferee() {

        BigDecimal num = new BigDecimal("40");
        RefereeInfoVO refereeInfoVO = new RefereeInfoVO();
        refereeInfoVO.setRefereeId("R01002");
        refereeInfoVO.setRefereeName("龙敏华");
        refereeInfoVO.setRefereeSex("1");
        refereeInfoVO.setRefereeAge("21");
        refereeInfoVO.setRefereeItem("篮球");
        refereeInfoVO.setChargeStandard(num);
        refereeInfoVO.setRefereeState("空闲");
        refereeInfoVO.setPhone("13392279606");
        refereeInfoVO.setRemark("我很帅");

        System.out.println(refereeInfoVO);
        refereeInfoService.updateReferee(refereeInfoVO);
    }

    @Test
    public void testGetByRefereeId(){

        RefereeInfoVO refereeInfoVO = new RefereeInfoVO();
        refereeInfoVO.setRefereeId("R01002");
        System.out.println(refereeInfoService.getByRefereeId(refereeInfoVO.getRefereeId()));
    }

    @Test
    public void testListAll(){

        System.out.println(refereeInfoService.listAll());
    }
}